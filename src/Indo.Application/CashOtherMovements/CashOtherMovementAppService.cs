﻿using Indo.CashMovements;
using Indo.FlowApi;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.CashOtherMovements
{
    public  class CashOtherMovementAppService : IndoAppService, ICashOtherMovementAppService
    {
        private readonly Endpoints _endpoints;

        public CashOtherMovementAppService()
        {
            _endpoints = new Endpoints();
        }

        public async Task<List<CashOtherMovementsReadDto>> GetListAsync()
        {
            List<CashOtherMovementsReadDto> cashOtherMovements = await _endpoints.FetchCashMovements<CashOtherMovementsReadDto>(CashMovementTypes.Other);
            if (cashOtherMovements != null)
            {
                return cashOtherMovements;
            }
            else
            {
                return null;
            }
        }
    }
}