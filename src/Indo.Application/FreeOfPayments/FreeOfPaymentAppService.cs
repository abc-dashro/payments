﻿using Indo.Currencies;
using Indo.FlowApi;
using Indo.FlowApi.Models;
using Indo.PaymentTypes;
using Indo.SourcePortfolios;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.FreeOfPayments
{
    public class FreeOfPaymentAppService : IndoAppService, IFreeOfPaymentAppService
    {
        private readonly Endpoints _endpoints;
        public FreeOfPaymentAppService()
        {
            _endpoints = new Endpoints();
        }

        public List<TypeReadDto> GetTypeList()
        {
            var dtos = new List<TypeReadDto>
            {
                new TypeReadDto { TypeID = 1, TypeName = "Type-1"},
                new TypeReadDto { TypeID = 2, TypeName = "Type-2"},
            };

            return dtos;
        }

        public List<ISINReadDto> GetISINList()
        {
            var dtos = new List<ISINReadDto>
            {
                new ISINReadDto { ISINID = 1, ISINName = "ISIN-1" },
                new ISINReadDto { ISINID = 2, ISINName = "ISIN-2" },
            };

            return dtos;
        }

        public List<CCYReadDto> GetCCYList()
        {
            var dtos = new List<CCYReadDto>
            {
                new CCYReadDto { CCYID = 1, CCYName = "USD" },
                new CCYReadDto { CCYID = 2, CCYName = "GBP" },
                new CCYReadDto { CCYID = 3, CCYName = "EUR" }
            };

            return dtos;
        }

        public List<FOPRequestReadDto> GetFOPRequestList()
        {
            var dtos = new List<FOPRequestReadDto>
            {

            };

            return dtos;
        }

        public async Task<string> InsertFOPPayment(FOP fop)
        {
            var freeOfPaymentData = await _endpoints.InsertFOPPayment(fop);
            if (freeOfPaymentData != null)
            {
                return freeOfPaymentData.Message;
            }

            return "Error";
        }

        public async Task<List<PortfoliosReadDto>> FetchSourcePortfolios(string? SpId)
        {
            var portfolioTypes = await _endpoints.FetchSourcePortfolios(SpId);
            return portfolioTypes;
        }
        public async Task<List<PaymentTypesReadDto>> FetchPaymentTypes(string? ctyId)
        {
            var paymentTypes = await _endpoints.FetchPaymentTypes(ctyId);
            return paymentTypes;
        }
    }
}
