﻿using Indo.Benficiaries;
using Indo.Currencies;
using Indo.FlowApi;
using Indo.PaymentCountries;
using Indo.PaymentTypes;
using Indo.SourceClients;
using Indo.SourcePortfolios;
using Indo.SwiftCodes;
using Indo.Templates;
using Indo.TransferFees;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.Payments
{
    public class PaymentAppService : IndoAppService, IPaymentAppService
    {
        private readonly Endpoints _endpoints;
        public PaymentAppService()
        {
            _endpoints = new Endpoints();
        }


        public async Task<string> InsertPayment(Payment payment)
        {
            var paymentData = await _endpoints.InsertPayment(payment);
            if (paymentData != null)
            {
                return paymentData.Message;
            }

            return "Error";
        }
        public async Task<List<SwiftCodeReadDto>> FetchSwiftCodes(string? scId, string? swiftCode)
        {
            var swiftCodes = await _endpoints.FetchSwiftCodes(scId, swiftCode);
            return swiftCodes;
        }
        public async Task<List<PaymentCountriesReadDto>> FetchPaymentCountries(string? ctyId)
        {
            var paymentCountries = await _endpoints.FetchPaymentCountries(ctyId);
            return paymentCountries;
        }

        public async Task<List<PaymentTypesReadDto>> FetchPaymentTypes(string? ctyId)
        {
            var paymentTypes = await _endpoints.FetchPaymentTypes(ctyId);
            return paymentTypes;
        }

        public async Task<List<PortfoliosReadDto>> FetchSourcePortfolios(string? SpId)
        {
            var portfolioTypes = await _endpoints.FetchSourcePortfolios(SpId);
            return portfolioTypes;
        }
        public async Task<List<FlowCurrency>> GetCurrencyListAsync()
        {
            var currency = await _endpoints.FetchCurrencies("");

            return currency;
        }

        public async Task<List<TransferFeesReadDto>> FetchTransferFees(string? scId)
        {
            var transferFee = await _endpoints.FetchTransferFees(scId);
            return transferFee;
        }

        public async Task<List<ClientRelationships>> FetchSourceClientRelationships()
        {
            var clientRelationship = await _endpoints.FetchSourceClientRelationships();
            return clientRelationship;
        }

        public async Task<List<BeneficiaryReadDto>> FetchBenficiaries()
        {
            var benficiaries = await _endpoints.FetchBenficiaries();
            return benficiaries;
        }

        public async Task<List<TemplateReadDto>> FetchTemplates()
        {
            var templates = await _endpoints.FetchTemplates();
            return templates;
        }


    }
}
