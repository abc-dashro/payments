﻿using Indo.Currencies;
using Indo.FlowApi;
using Indo.FreeOfPayments;
using Indo.PaymentReceiveDeliveries;
using Indo.PaymentReceiveDeliverType;
using Indo.PaymentSelectAccounts;
using Indo.PaymentTypes;
using Indo.PortfolioHomeAddresses;
using Indo.SourceInstruments;
using Indo.SourcePortfolios;
using Indo.SourceSettlementPlaces;
using Indo.SwiftCodes;
using Indo.Swifts;
using Indo.SwiftsMT542;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.BackOfficeFOPs
{
    public class BackOfficeFOPAppService : IndoAppService, IBackOfficeFOPAppService
    {
        private readonly Endpoints _endpoints;
        public BackOfficeFOPAppService()
        {
            _endpoints = new Endpoints();
        }

        public async Task<List<PortfoliosReadDto>> FetchSourcePortfolios(string? SpId)
        {
            var portfolioTypes = await _endpoints.FetchSourcePortfolios(SpId);
            return portfolioTypes;
        }
        public async Task<List<PaymentTypesReadDto>> FetchPaymentTypes(string? ctyId)
        {
            var paymentTypes = await _endpoints.FetchPaymentTypes(ctyId);
            return paymentTypes;
        }
        public async Task<List<FlowCurrency>> FetchCurrencies()
        {
            var currency = await _endpoints.FetchCurrencies("");

            return currency;
        }
        public async Task<List<InstrumentsReadDto>> FetchSourceInstruments(string ctyId)
        {
            var instruments = await _endpoints.FetchSourceInstruments(ctyId);
            return instruments;
        }
        public async Task<List<SettlementPlacesReadDto>> FetchSourceSettlementPlaces(string? ctyId)
        {
            var settlementPlaces = await _endpoints.FetchSourceSettlementPlaces(ctyId);
            return settlementPlaces;
        }
        public async Task<List<SwiftCodeReadDto>> FetchSwiftCodes(string? scId, string? swiftCode)
        {
            var swiftCodes = await _endpoints.FetchSwiftCodes(scId, swiftCode);
            return swiftCodes;
        }
        public async Task<string> FetchSwift(SwiftDto? swiftDto)
        {
            var swiftData = await _endpoints.FetchSwift(swiftDto);
            if (swiftData != null)
            {
                return swiftData.Message;
            }
            return "Error";
        }
        public async Task<string> FetchSwiftMT542(SwiftMT542Dto? swiftMT542Dto)
        {
            var swiftData = await _endpoints.FetchSwiftMT542(swiftMT542Dto);
            if (swiftData != null)
            {
                return swiftData.Message;
            }
            return "Error";
        }
        

        public async Task<string> InsertFOPPayment(FOP fop)
        {
            var freeOfPaymentData = await _endpoints.InsertFOPPayment(fop);
            if (freeOfPaymentData != null)
            {
                return freeOfPaymentData.Message;
            }

            return "Error";
        }
        public async Task<string> UpdateFOPPayment(FOP fop)
        {
            var freeOfPaymentData = await _endpoints.UpdateFOPPayment(fop);
            if (freeOfPaymentData != null)
            {
                return freeOfPaymentData.Message;
            }

            return "Error";
        }
        
        public async Task<PaymentHomeAddressReadDto> FetchPaymentHomeAddress()
        {
            var paymentHomeAddress = await _endpoints.FetchPaymentHomeAddress();
            return paymentHomeAddress;
        }
        public async Task<List<PaymentReceiveDeliverTypeReadDto>> FetchPaymentReceiveDeliverType()
        {
            return await _endpoints.FetchPaymentReceiveDeliverType();
        }
        public async Task<List<PaymentSelectAccountReadDto>> FetchPaymentSelectAccount(string? spId)
        {
            return await _endpoints.FetchPaymentSelectAccount(spId);
        }
        public async Task<List<PaymentRecieveDeliveriesReadDto>> FetchPaymentReceiveDeliveries(string prdId)
        {
            return await _endpoints.FetchPaymentReceiveDeliveries(prdId); ;
        }
    }
}
