﻿using Indo.CashMovements;
using Indo.FlowApi;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.CashMainMovements
{
    public class CashMainMovementAppService : IndoAppService, ICashMainMovementAppService
    {
        private readonly Endpoints _endpoints;

        public CashMainMovementAppService()
        {
            _endpoints = new Endpoints();
        }

        public async Task<List<CashMainMovementsReadDto>> GetListAsync()
        {
            List<CashMainMovementsReadDto> cashOtherMovements = await _endpoints.FetchCashMovements<CashMainMovementsReadDto>(CashMovementTypes.Main);
            if (cashOtherMovements != null)
            {
                return cashOtherMovements;
            }
            else
            {
                return null;
            }
        }
    }
}