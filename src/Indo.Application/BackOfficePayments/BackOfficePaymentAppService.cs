﻿using Indo.Benficiaries;
using Indo.Currencies;
using Indo.CutOffs;
using Indo.FlowApi;
using Indo.IntermediaryCodes;
using Indo.PaymentCountries;
using Indo.PaymentGridViews;
using Indo.PaymentRARangeType;
using Indo.Payments;
using Indo.PaymentSelectAccounts;
using Indo.PaymentTypes;
using Indo.PortfolioHomeAddresses;
using Indo.SourceClients;
using Indo.SourcePortfolios;
using Indo.SwiftBackOfficePayment;
using Indo.SwiftBackOfficePaymentmt200;
using Indo.SwiftBackOfficePaymentMt210;
using Indo.SwiftBackOfficePaymentMt299;
using Indo.SwiftCodes;
using Indo.Templates;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.BackOfficePayment
{
    public class BackOfficePaymentAppService : IndoAppService, IBackOfficePaymentAppService
    {
        private readonly Endpoints _endpoints;
        public BackOfficePaymentAppService()
        {
            _endpoints = new Endpoints();
        }
        public async Task<string> InsertPayment(Payment payment)
        {
            var paymentData = await _endpoints.InsertPayment(payment);
            if (paymentData != null)
            {
                return paymentData.Message;
            }

            return "Error";
        }
        public async Task<string> UpdatePayment(Payment payment)
        {
            var paymentData = await _endpoints.UpdatePayment(payment);
            if (paymentData != null)
            {
                return paymentData.Message;
            }

            return "Error";
        }
        public async Task<List<PaymentTypesReadDto>> FetchMoveTypes(string? ptId)
        {
            var moveTypes = await _endpoints.FetchPaymentTypes(ptId);
            return moveTypes;
        }
        public async Task<List<PortfoliosReadDto>> FetchPortfolio(string? SpId)
        {
            var portFolios = await _endpoints.FetchSourcePortfolios(SpId);
            return portFolios;
        }

        public async Task<List<PaymentOthersDto>> FetchPaymentOthers(string? othId)
        {
            var paymentOthers = await _endpoints.FetchPaymentOthers(othId);
            return paymentOthers;
        }

        public async Task<List<FlowCurrency>> FetchCurrencies(string currValue)
        {
            var getCurrency = await _endpoints.FetchCurrencies(currValue);
            return getCurrency;
        }

        public async Task<List<PaymentCountriesReadDto>> FetchPaymentCountries(string? ctyId)
        {
            var getCountries = await _endpoints.FetchPaymentCountries(ctyId);
            return getCountries;
        }

        public async Task<List<TemplateReadDto>> FetchTemplates()
        {
            var getTemplates = await _endpoints.FetchTemplates();
            return getTemplates;
        }
        public async Task<List<PaymentAddressReadDto>> FetchPaymentAddress(string? paId, string? tempName)
        {
            var paymentAddress = await _endpoints.FetchPaymentAddress(paId, tempName);
            return paymentAddress;
        }
        

        public async Task<List<BeneficiaryReadDto>> FetchBenficiaries()
        {
            var getBenficiaries = await _endpoints.FetchBenficiaries();
            return getBenficiaries;
        }

        public async Task<List<ClientRelationships>> FetchSourceClientRelationships()
        {
            var getClientRelationships = await _endpoints.FetchSourceClientRelationships();
            return getClientRelationships;
        }

        public async Task<List<IntermediaryCodesReadDto>> FetchIntermediaryCodes(string? ibank_Id)
        {
            var getIntermediaryCodes = await _endpoints.FetchIntermediaryCodes(ibank_Id);
            return getIntermediaryCodes;
        }
        public async Task<List<PaymentSelectAccountReadDto>> FetchPaymentSelectAccount(string? spId)
        {
            return await _endpoints.FetchPaymentSelectAccount(spId);
        }
        public async Task<string> FetchSwiftBackOfficePayment(SwiftBackOfficePaymentDto? swiftBackOfficePaymentDto)
        {
            var swiftData = await _endpoints.FetchSwiftBackOfficePayment(swiftBackOfficePaymentDto);
            if (swiftData != null)
            {
                return swiftData.Message;
            }
            return "Error";
        }

        public async Task<string> FetchSwiftBackOfficePaymentmt200(SwiftBackOfficePaymentmt200Dto? swiftBackOfficePaymentmt200Dto)
        {
            var swiftDatamt200 = await _endpoints.FetchSwiftBackOfficePaymentmt200(swiftBackOfficePaymentmt200Dto);
            if (swiftDatamt200 != null)
            {
                return swiftDatamt200.Message;
            }
            return "Error";
        }
        public async Task<string> FetchSwiftBackOfficePaymentMt210(SwiftBackOfficePaymentMt210Dto? swiftBackOfficePaymentMt210Dto)
        {
            var swiftDataMt210 = await _endpoints.FetchSwiftBackOfficePaymentMt210(swiftBackOfficePaymentMt210Dto);
            if (swiftDataMt210 != null)
            {
                return swiftDataMt210.Message;
            }
            return "Error";
        }

        public async Task<string> FetchSwiftBackOfficePaymentMt299(SwiftBackOfficePaymentMt299Dto? swiftBackOfficePaymentMt299Dto)
        {
            var swiftDataMt299 = await _endpoints.FetchSwiftBackOfficePaymentMt299(swiftBackOfficePaymentMt299Dto);
            if (swiftDataMt299 != null)
            {
                return swiftDataMt299.Message;
            }
            return "Error";
        }

        public async Task<PaymentHomeAddressReadDto> FetchPaymentHomeAddress()
        {
            var paymentHomeAddress = await _endpoints.FetchPaymentHomeAddress();
            return paymentHomeAddress;
        }
        public async Task<List<SwiftCodeReadDto>> FetchSwiftCodes(string? scId, string? swiftCode)
        {
            var swiftCodes = await _endpoints.FetchSwiftCodes(scId, swiftCode);
            return swiftCodes;
        }

        public async Task<List<CutOffPayments>> FetchCutOffPayments(string? brokerCode, string? currCode)
        {
            var cutOffPayment = await _endpoints.FetchCutOffPayments(brokerCode, currCode);
            return cutOffPayment;
        }

        public async Task<List<PaymentGridViewReadDto>> FetchPaymentGridView(string? pId)
        {
            var paymentGridView = await _endpoints.FetchPaymentGridView(pId);
            return paymentGridView;
        }

        public async Task<List<PaymentRARangeTypeReadDto>> FetchPaymentRARangeTypes(string? ptId)
        {
            var getPaymentRARangeType = await _endpoints.FetchPaymentRARangeTypes(ptId);
            return getPaymentRARangeType;
        }
    }
}
