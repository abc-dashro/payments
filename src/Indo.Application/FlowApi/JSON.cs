﻿using Newtonsoft.Json;

namespace Indo.FlowApi
{
    public class JSON
    {
        public static string Serializer(object payload)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.Indented;
            settings.NullValueHandling = NullValueHandling.Ignore;
            return JsonConvert.SerializeObject(payload, settings);
        }

        public static string Deserializer(object payload)
        {
            return JsonConvert.DeserializeObject<dynamic>((string)payload);
        }

        public static object Deserializer<T>(string responseBody)
        {
            return JsonConvert.DeserializeObject<T>(responseBody);
        }
    }
}