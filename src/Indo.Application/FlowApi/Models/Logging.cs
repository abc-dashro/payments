﻿namespace Indo.FlowApi.Models
{
    public class Logging
    {
        public string Level { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
    }
}