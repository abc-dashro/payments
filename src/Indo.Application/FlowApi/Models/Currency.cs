﻿using System.ComponentModel.DataAnnotations;
using static Indo.FlowApi.Extensions.AttributeExtensions;

namespace Indo.FlowApi.Models
{
    public class Currency
    {
        public int? CR_Code { get; set; }
        [StringLength(3)]
        [Required]
        public string CR_Name { get; set; }
        [StringLength(50)]
        [Required]
        public string CR_FullName { get; set; }
        public string CR_PrSymbol { get; set; }
        [StringLength(3)]
        public string CR_Shortcut { get; set; }
        public string CR_Reverse { get; set; }
        public string CR_ISO { get; set; }
    }
}