﻿using Indo.AuditLogs;
using Indo.Benficiaries;
using Indo.Brokers;
using Indo.CorporateActionPayments;
using Indo.Currencies;
using Indo.CutOffs;
using Indo.Fees;
using Indo.FeeSchedules;
using Indo.FileExports;
using Indo.FilePaths;
using Indo.FlowApi.Models;
using Indo.FreeOfPayments;
using Indo.GppMappingCodes;
using Indo.ImsReconciliations;
using Indo.IncomingFunds;
using Indo.IntermediaryCodes;
using Indo.PaymentCountries;
using Indo.PaymentFirmPortfolios;
using Indo.PaymentGridViews;
using Indo.PaymentMessageTypes;
using Indo.PaymentRARangeType;
using Indo.PaymentReceiveDeliveries;
using Indo.PaymentReceiveDeliverType;
using Indo.Payments;
using Indo.PaymentSelectAccounts;
using Indo.PaymentTypes;
using Indo.PortfolioHomeAddresses;
using Indo.Portfolios;
using Indo.ReadSwifts;
using Indo.SourceAccounts;
using Indo.SourceBranches;
using Indo.SourceClients;
using Indo.SourceInstruments;
using Indo.SourcePortfolios;
using Indo.SourcePortfolioTypes;
using Indo.SourceSettlementAccounts;
using Indo.SourceSettlementPlaces;
using Indo.Statuses;
using Indo.SwiftBackOfficePayment;
using Indo.SwiftBackOfficePaymentmt200;
using Indo.SwiftBackOfficePaymentMt210;
using Indo.SwiftBackOfficePaymentMt299;
using Indo.SwiftCodes;
using Indo.SwiftReadDetails;
using Indo.SwiftReadErrors;
using Indo.Swifts;
using Indo.SwiftsMT542;
using Indo.Templates;
using Indo.TransferFees;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Currency = Indo.FlowApi.Models.Currency;

namespace Indo.FlowApi
{
    public class Endpoints
    {
        public static HttpResponseMessage responseMessage;
        private readonly string urlPrefix = "https://flowapi.dashrosolutions.com/api/v1/Flow/";
        //private readonly string urlPrefix = "https://localhost:44341/api/v1/Flow/";
        private readonly string urlPrefixSwift = "https://swiftapi.dashrosolutions.com/api/v1/Swift/";
        private readonly string apiKey = "051c07ca-20e8-4848-b4c1-fe15913d1f67";
        private readonly int clientId = 6198;

        /// <summary>
        /// Constructor.
        /// </summary>
        public Endpoints()
        {
        }

        /// <summary>
        /// Fetch a currency or all currencies.
        /// </summary>
        /// <param name="currValue"></param>
        /// <returns></returns>
        public async Task<List<FlowCurrency>> FetchCurrencies(string currValue)
        {
            string url = $"{urlPrefix}Currency";

            if (!string.IsNullOrEmpty(currValue))
            {
                url += $"?curr={currValue}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                List<FlowCurrency> currencies = new();
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var currenclyList = (List<FlowCurrency>)JSON.Deserializer<List<FlowCurrency>>(responseBody);
                return currenclyList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Update the selected currency.
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateCurrency(Currency currency, string userName)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (currency != null)
            {
                //Serialise the Payload.

                var payload = JSON.Serializer(currency);

                string url = $"{urlPrefix}Currency?userName={userName}&clientId={clientId}";

                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Insert a  new currency.
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertCurrency(Currency currency, string userName)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (currency != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(currency);

                string url = $"{urlPrefix}Currency?userName={userName}&clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete currency Endpoint
        /// </summary>
        /// <param name="currencyId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteCurrency(double currencyId)
        {
            UserMessage userMessage = new();
            using var client = new HttpClient();

            string url = $"{urlPrefix}Currency?currencyId={currencyId}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Fetch a currency or all currencies.
        /// </summary>
        /// <param name="currValue"></param>
        /// <returns></returns>
        public async Task<List<CutOffPayments>> FetchCutOffPayments(string brokerCode, string currCode)
        {
            var _requestTimeout = TimeSpan.FromSeconds(130);

            string url = $"{urlPrefix}CutOff";
            if (!string.IsNullOrEmpty(brokerCode) && !string.IsNullOrEmpty(currCode))
            {
                url += $"?brokerCode={brokerCode}&currCode={currCode}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                //List<CutOffPayments> currencies = new List<CutOffPayments>();
                using (var client = new HttpClient())
                {
                    client.Timeout = _requestTimeout;

                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _cutoffList = (List<CutOffPayments>)JSON.Deserializer<List<CutOffPayments>>(_responseBody);
                return _cutoffList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Update the Cut Off Payment
        /// </summary>
        /// <param name="cutoffPayment"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateCutOffPayments(CutOffPayment cutoffPayment)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (cutoffPayment != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(cutoffPayment);

                string url = $"{urlPrefix}CutOff?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Insert a new Cut Off Payment.
        /// </summary>
        /// <param name="cutoffPayment"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertCutOffPayment(CutOffPayment cutoffPayment)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (cutoffPayment != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(cutoffPayment);

                string url = $"{urlPrefix}CutOff?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete the selected Cut Off Payment.
        /// </summary>
        /// <param name="brokerCode"></param>
        /// <param name="curcode"></param>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteCutOffPayment(int brokerCode, int curcode, string userName)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}CutOff?brokerCode={brokerCode}&currCode={curcode}&userName={userName}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Fetch a Transfer Fee or all Transfer Fees.
        /// </summary>
        /// <param name="currValue"></param>
        /// <returns></returns>
        public async Task<List<TransferFeesReadDto>> FetchTransferFees(string? pfCode)
        {
            string url = $"{urlPrefix}TransferFee";

            if (!string.IsNullOrEmpty(pfCode))
            {
                url += $"?pfCode={pfCode}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _transferFeesList = (List<TransferFeesReadDto>)JSON.Deserializer<List<TransferFeesReadDto>>(_responseBody);
                return _transferFeesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Update Transfer Fees.
        /// </summary>
        /// <param name="transferFees"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateTransferFees(TransferFeeUpdateDto transferFees)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (transferFees != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(transferFees);

                string url = $"{urlPrefix}TransferFee?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Insert a new Transfer Fee.
        /// </summary>
        /// <param name="transferFees"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertTransferFees(TransferFeeCreateDto transferFees)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (transferFees != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(transferFees);

                string url = $"{urlPrefix}TransferFee?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete a Trasfer Fee.
        /// </summary>
        /// <param name="portfolioCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteTransferFee(int portfolioCode)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}TransferFee?pfCode={portfolioCode}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Fetch a Swift Code or all Swift Codes.
        /// </summary>
        /// <param name="scId"></param>
        /// <returns></returns>
        public async Task<List<SwiftCodeReadDto>> FetchSwiftCodes(string? scId, string? swiftCode)
        {
            string url = $"{urlPrefix}SwiftCode";
            if (!string.IsNullOrEmpty(scId) && scId != "-1")
            {
                url += $"?scId={scId}&clientId={clientId}";
            }
            else if (!string.IsNullOrEmpty(swiftCode))
            {
                url += $"?sc_SwiftCode={swiftCode}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _swiftCodesList = (List<SwiftCodeReadDto>)JSON.Deserializer<List<SwiftCodeReadDto>>(_responseBody);
                return _swiftCodesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Update a Swift Code.
        /// </summary>
        /// <param name="swiftCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateSwiftCode(SwiftCodeUpdateDto swiftCode)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (swiftCode != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(swiftCode);

                string url = $"{urlPrefix}SwiftCode?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Insert a new Swift Code.
        /// </summary>
        /// <param name="swiftCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertSwiftCode(SwiftCodeCreateDto swiftCode)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (swiftCode != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(swiftCode);

                string url = $"{urlPrefix}SwiftCode?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete the Swift Code.
        /// </summary>
        /// <param name="scId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteSwiftCode(int scId)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}SwiftCode?scId={scId}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Fetch a GPP Mapping Code or all Mapping Codes.
        /// </summary>
        /// <param name="gpp_Id"></param>
        /// <returns></returns>
        public async Task<List<GppMappingCodesReadDto>> FetchGppMappingCodes(string? gpp_Id)
        {
            string url = $"{urlPrefix}GppMappingCode";
            if (!string.IsNullOrEmpty(gpp_Id))
            {
                url += $"?gpp_Id={gpp_Id}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _gppMappingCodesList = (List<GppMappingCodesReadDto>)JSON.Deserializer<List<GppMappingCodesReadDto>>(_responseBody);
                return _gppMappingCodesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Update a GPP Mapping Code.
        /// </summary>
        /// <param name="gppMappingCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateGppMappingCode(GppMappingCodeUpdateDto gppMappingCode)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (gppMappingCode != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(gppMappingCode);

                string url = $"{urlPrefix}GppMappingCode?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Insert a new GPP Mapping Code.
        /// </summary>
        /// <param name="gppMappingCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertGppMappingCode(GppMappingCodeCreateDto gppMappingCode)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (gppMappingCode != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(gppMappingCode);

                string url = $"{urlPrefix}GppMappingCode?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete a GPP Mapping Code.
        /// </summary>
        /// <param name="gpp_Id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteGppMappingCode(int gpp_Id)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}GppMappingCode?gpp_Id={gpp_Id}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Fetch all or a single Intermediary Code.
        /// </summary>
        /// <param name="ibank_Id"></param>
        /// <returns></returns>
        public async Task<List<IntermediaryCodesReadDto>> FetchIntermediaryCodes(string? ibank_Id)
        {
            string url = $"{urlPrefix}IntermediaryCode";
            if (!string.IsNullOrEmpty(ibank_Id))
            {
                url += $"?ibank_Id={ibank_Id}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _intermediaryCodesList = (List<IntermediaryCodesReadDto>)JSON.Deserializer<List<IntermediaryCodesReadDto>>(_responseBody);
                return _intermediaryCodesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Update an Intermediary code.
        /// </summary>
        /// <param name="intermediaryCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateIntermediaryCode(IntermediaryCodeUpdateDto intermediaryCode)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (intermediaryCode != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(intermediaryCode);

                string url = $"{urlPrefix}IntermediaryCode?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Insert a new Intermediary Code.
        /// </summary>
        /// <param name="intermediaryCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertIntermediaryCode(IntermediaryCodeCreateDto intermediaryCode)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (intermediaryCode != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(intermediaryCode);

                string url = $"{urlPrefix}IntermediaryCode?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete an Intermediary Code.
        /// </summary>
        /// <param name="ibank_Id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteIntermediaryCode(int ibank_Id)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}IntermediaryCode?ibank_Id={ibank_Id}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Fetch all Broker Balances.
        /// </summary>
        /// <param name="branchCode"></param>
        /// <returns></returns>
        public async Task<List<BrokerBalancesReadDto>> FetchBrokerBalances(string branchCode)
        {
            string url = $"{urlPrefix}BrokerBalance?branchCode={branchCode}&clientId={clientId}";
            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _brokerBalancesList = (List<BrokerBalancesReadDto>)JSON.Deserializer<List<BrokerBalancesReadDto>>(_responseBody);
                return _brokerBalancesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Fetch all Portfolio Balances.
        /// </summary>
        /// <param name="branchCode"></param>
        /// <returns></returns>
        public async Task<List<PortfolioBalancesReadDto>> FetchPortfolioBalances(string branchCode)
        {
            string url = $"{urlPrefix}PortfolioBalance?branchCode={branchCode}&clientId={clientId}";
            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _portfolioBalancesList = (List<PortfolioBalancesReadDto>)JSON.Deserializer<List<PortfolioBalancesReadDto>>(_responseBody);
                return _portfolioBalancesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Fetch all Swift Reads.
        /// </summary>
        /// <returns></returns>
        public async Task<List<SwiftReads>> FetchSwiftReads()
        {
            string url = $"{urlPrefix}SwiftRead?clientId={clientId}";
            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _readSwiftsList = (List<SwiftReads>)JSON.Deserializer<List<SwiftReads>>(_responseBody);
                return _readSwiftsList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Fetch one or all Fee Schedules.
        /// </summary>
        /// <param name="fs_Id"></param>
        /// <returns></returns>
        public async Task<List<FeeSchedulesReadDto>> FetchFeeSchedules(string fs_Id)
        {
            string url = $"{urlPrefix}FeeSchedule";
            if (!string.IsNullOrEmpty(fs_Id))
            {
                url += $"?fs_Id={fs_Id}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _feeSchedulesList = (List<FeeSchedulesReadDto>)JSON.Deserializer<List<FeeSchedulesReadDto>>(_responseBody);
                return _feeSchedulesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Fetch Fee Schedule Link.
        /// </summary>
        /// <param name="fs_Id"></param>
        /// <returns></returns>
        public async Task<FeeScheduleLinkReadDto> FetchFeeScheduleLink(string fs_Id)
        {
            string url = $"{urlPrefix}FeeScheduleLink?fs_Id={fs_Id}";
            if (!string.IsNullOrEmpty(fs_Id))
            {
                url += $"?fs_Id={fs_Id}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }
            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _feeSchedulesLink = (FeeScheduleLinkReadDto)JSON.Deserializer<FeeScheduleLinkReadDto>(_responseBody);
                return _feeSchedulesLink;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Fetch IMS Reconciliations.
        /// </summary>
        /// <param name="imsReconciliationRequest"></param>
        /// <returns></returns>
        public async Task<List<ImsReconciliationsReadDto>> FetchImsReconciliations(ImsReconciliationRequest imsReconciliationRequest)
        {
            using var client = new HttpClient();
            if (imsReconciliationRequest != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(imsReconciliationRequest);

                string url = $"{urlPrefix}Reconciliation?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var _responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var _imsReconciliations = (List<ImsReconciliationsReadDto>)JSON.Deserializer<List<ImsReconciliationsReadDto>>(_responseBody);
                        return _imsReconciliations;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    Logging logging = new()
                    {
                        Level = "Error",
                        Exception = ex.StackTrace,
                        Message = ex.Message
                    };

                    var _response = await InsertToLog(logging, 0);
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Fetch Corporate Actions for Mandatory and Voluntary
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="corpActType"></param>
        /// <param name="archive"></param>
        /// <param name="ca_Id"></param>
        /// <returns></returns>
        public async Task<List<T>> FetchCorporateActionsMV<T>(string corpActType, bool archive, int? ca_Id)
        {
            string url = $"{urlPrefix}CorporateActionsMV?corporateActionType={corpActType}&archived={archive}";
            if (ca_Id != null)
            {
                url += $"&ca_Id={ca_Id}&clientId={clientId}";
            }
            else
            {
                url += $"&clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _corpActionList = (List<T>)JSON.Deserializer<List<T>>(_responseBody);
                return _corpActionList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                var _response = await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Fetch Payment Corporate Actions.
        /// </summary>
        /// <param name="archive"></param>
        /// <returns></returns>
        public async Task<List<CorporateActionPaymentReadDto>> FetchCorporateActionsPayment(bool archive)
        {
            string url = $"{urlPrefix}CorporateActionsPayment?archived={archive}&clientId={clientId}";
            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _corpActionPaymentList = (List<CorporateActionPaymentReadDto>)JSON.Deserializer<List<CorporateActionPaymentReadDto>>(_responseBody);
                return _corpActionPaymentList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                var _response = await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Archive Corporate Actions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="archiveMVCA"></param>
        /// <returns></returns>
        public async Task<UserMessage> ArchiveCorporateActions<T>(T archiveMVCA)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (archiveMVCA != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(archiveMVCA);

                string url = $"{urlPrefix}CorporateActions&clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Fetch all Cash Movements
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<List<T>> FetchCashMovements<T>(bool type)
        {
            string url = $"{urlPrefix}CashMovements?type={type}&clientId={clientId}";
            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _cashMovements = (List<T>)JSON.Deserializer<List<T>>(_responseBody);
                return _cashMovements;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Fetch Payment Message Type or all Message Types
        /// </summary>
        /// <param name="m_Id"></param>
        /// <returns></returns>
        public async Task<List<PaymentMessageTypesReadDto>> FetchPaymentMessageTypes(string? m_Id)
        {
            string url = $"{urlPrefix}MessageType";

            if (!string.IsNullOrEmpty(m_Id))
            {
                url += $"?m_Id={m_Id}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _paymentMessageTypesList = (List<PaymentMessageTypesReadDto>)JSON.Deserializer<List<PaymentMessageTypesReadDto>>(_responseBody);
                return _paymentMessageTypesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Delete the selected Payment Message Type.
        /// </summary>
        /// <param name="m_Id"></param>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeletePaymentMessageTypes(int m_Id, string userName)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}MessageType?m_Id={m_Id}&userName={userName}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Update the selected Payment Message Type.
        /// </summary>
        /// <param name="paymentMessageTypeUpdateDto"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdatePaymentMessageType(PaymentMessageTypeUpdateDto paymentMessageTypeUpdateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (paymentMessageTypeUpdateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(paymentMessageTypeUpdateDto);

                string url = $"{urlPrefix}MessageType?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Insert new Payment Message Type.
        /// </summary>
        /// <param name="paymentMessageTypeCreateDto"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertPaymentMessageType(PaymentMessageTypeCreateDto paymentMessageTypeCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (paymentMessageTypeCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(paymentMessageTypeCreateDto);

                string url = $"{urlPrefix}MessageType?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Fetch the Audit Log.
        /// </summary>
        /// <returns></returns>
        public async Task<List<AuditLog>> FetchAuditLog()
        {
            string url = $"{urlPrefix}Logging?clientId={clientId}";

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _auditLogList = (List<AuditLog>)JSON.Deserializer<List<AuditLog>>(_responseBody);
                return _auditLogList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Fetch Payment Range Countries.
        /// </summary>
        /// <param name="ctyId"></param>
        /// <returns></returns>
        public async Task<List<PaymentCountriesReadDto>> FetchPaymentCountries(string? ctyId)
        {
            string url = $"{urlPrefix}PaymentCountry";
            if (!string.IsNullOrEmpty(ctyId) && ctyId != "-1")
            {
                url += $"?ctyId={ctyId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _paymentCountriesList = (List<PaymentCountriesReadDto>)JSON.Deserializer<List<PaymentCountriesReadDto>>(_responseBody);
                return _paymentCountriesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Update Payment Country.
        /// </summary>
        /// <param name="paymentCountryUpdateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdatePaymentCountry(PaymentCountryUpdateDto paymentCountryUpdateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (paymentCountryUpdateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(paymentCountryUpdateDto);

                string url = $"{urlPrefix}PaymentCountry?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete payment Country
        /// </summary>
        /// <param name="m_Id"></param>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeletePaymentCountry(int m_Id, string userName)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}PaymentCountry?ctyId={m_Id}&userName={userName}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Insert new Payment Country
        /// </summary>
        /// <param name="paymentMessageTypeCreateDto"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertPaymentCountry(PaymentCountryCreateDto paymentCountryCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (paymentCountryCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(paymentCountryCreateDto);

                string url = $"{urlPrefix}PaymentCountry?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Fetch all or 1 Payment Receive Deliveries.
        /// </summary>
        /// <param name="prdId"></param>
        /// <returns></returns>
        public async Task<List<PaymentRecieveDeliveriesReadDto>> FetchPaymentReceiveDeliveries(string? prdId)
        {
            string url = $"{urlPrefix}PaymentReceiveDeliver";
            if (!string.IsNullOrEmpty(prdId))
            {
                url += $"?prdId={prdId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _paymentMessageTypesList = (List<PaymentRecieveDeliveriesReadDto>)JSON.Deserializer<List<PaymentRecieveDeliveriesReadDto>>(_responseBody);
                return _paymentMessageTypesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Fetch Source Account or all Accounts.
        /// </summary>
        /// <param name="prdId"></param>
        /// <returns></returns>
        public async Task<List<AccountsReadDto>> FetchSourceAccounts(string? ScId)
        {
            string url = $"{urlPrefix}Account";
            if (!string.IsNullOrEmpty(ScId))
            {
                url += $"?ScId={ScId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _accountsList = (List<AccountsReadDto>)JSON.Deserializer<List<AccountsReadDto>>(_responseBody);
                return _accountsList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Insert a new Source Account.
        /// </summary>
        /// <param name="accountCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertSourceAccount(AccountCreateDto accountCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (accountCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(accountCreateDto);

                string url = $"{urlPrefix}Account?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete the selected Source Account.
        /// </summary>
        /// <param name="scId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteSourceAccount(int scId)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}Account?sa_Id={scId}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Update the selected Source Account.
        /// </summary>
        /// <param name="transferFees"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateSourceAccount(AccountUpdateDto accountUpdateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (accountUpdateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(accountUpdateDto);

                string url = $"{urlPrefix}Account?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }
        public async Task<List<PaymentOthersDto>> FetchPaymentOthers(string? othId)
        {
            string url = $"{urlPrefix}PaymentOther";
            if (!string.IsNullOrEmpty(othId) && othId != "-1")
            {
                url += $"?othId={othId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _paymentOthersList = (List<PaymentOthersDto>)JSON.Deserializer<List<PaymentOthersDto>>(_responseBody);
                return _paymentOthersList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Fetch all or a single Branch details.
        /// </summary>
        /// <param name="SbrId"></param>
        /// <returns></returns>
        public async Task<List<BranchesReadDto>> FetchSourceBranches(string? SbrId)
        {
            string url = $"{urlPrefix}Branch";
            if (!string.IsNullOrEmpty(SbrId))
            {
                url += $"?sbrId={SbrId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _branchesList = (List<BranchesReadDto>)JSON.Deserializer<List<BranchesReadDto>>(_responseBody);
                return _branchesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Delete the selected Branch.
        /// </summary>
        /// <param name="sbrId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteSourceBranch(int sbrId)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}Branch?sbrId={sbrId}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Insert a new Branch details.
        /// </summary>
        /// <param name="branchCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertSourceBranch(BranchCreateDto branchCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (branchCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(branchCreateDto);

                string url = $"{urlPrefix}Branch?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Update the selected Branh details.
        /// </summary>
        /// <param name="branchUpdateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateSourceBranch(BranchUpdateDto branchUpdateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (branchUpdateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(branchUpdateDto);

                string url = $"{urlPrefix}Branch?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Fetch all or a single Client details.
        /// </summary>
        /// <param name="SclId"></param>
        /// <returns></returns>
        public async Task<List<ClientsReadDto>> FetchSourceClients(string? SclId)
        {
            string url = $"{urlPrefix}Client";

            if (!string.IsNullOrEmpty(SclId))
            {
                url += $"?scl_Id={SclId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _clientsList = (List<ClientsReadDto>)JSON.Deserializer<List<ClientsReadDto>>(_responseBody);
                return _clientsList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Insert new Client Details.
        /// </summary>
        /// <param name="clientCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertSourceClient(ClientCreateDto clientCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (clientCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(clientCreateDto);

                string url = $"{urlPrefix}Client?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Fetch a Client Details or all Client Details.
        /// </summary>
        /// <returns></returns>
        public async Task<List<ClientRelationships>> FetchSourceClientRelationships()
        {
            string url = $"{urlPrefix}ClientRelationship?clientId={clientId}";

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _clientRelationshipList = (List<ClientRelationships>)JSON.Deserializer<List<ClientRelationships>>(_responseBody);
                return _clientRelationshipList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Update the selected Client Details.
        /// </summary>
        /// <param name="clientUpdateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateSourceClient(ClientUpdateDto clientUpdateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (clientUpdateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(clientUpdateDto);

                string url = $"{urlPrefix}Client?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete the selected Client.
        /// </summary>
        /// <param name="sclId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteSourceClient(int sclId)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}Client?sclId={sclId}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Fetch all Instruments or a single Instrument.
        /// </summary>
        /// <param name="SiId"></param>
        /// <returns></returns>
        public async Task<List<InstrumentsReadDto>> FetchSourceInstruments(string SiId)
        {
            string url = $"{urlPrefix}Instrument";

            if (!string.IsNullOrEmpty(SiId) && SiId != "-1")
            {
                url += $"?siId={SiId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _instrumentsList = (List<InstrumentsReadDto>)JSON.Deserializer<List<InstrumentsReadDto>>(_responseBody);
                return _instrumentsList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Insert new Instrument.
        /// </summary>
        /// <param name="instrumentCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertSourceInstrument(InstrumentCreateDto instrumentCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (instrumentCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(instrumentCreateDto);

                string url = $"{urlPrefix}Instrument?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete the selected Source Instrument.
        /// </summary>
        /// <param name="siId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteSourceInstrument(int siId)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}Instrument?siId={siId}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Update the selected Instrument.
        /// </summary>
        /// <param name="instrumentCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateSourceInstrument(InstrumentCreateDto instrumentCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (instrumentCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(instrumentCreateDto);

                string url = $"{urlPrefix}Instrument?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Fetch a Portfolio  or all Portfolios.
        /// </summary>
        /// <param name="SpId"></param>
        /// <returns></returns>
        public async Task<List<PortfoliosReadDto>> FetchSourcePortfolios(string? SpId)
        {
            string url = $"{urlPrefix}Portfolio";
            if (!string.IsNullOrEmpty(SpId) && SpId != "-1")
            {
                url += $"?spId={SpId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _portfoliosList = (List<PortfoliosReadDto>)JSON.Deserializer<List<PortfoliosReadDto>>(_responseBody);
                return _portfoliosList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Fetch a Portfolio Type or all Portfolio Types.
        /// </summary>
        /// <param name="SptId"></param>
        /// <returns></returns>
        public async Task<List<PortfolioTypesReadDto>> FetchSourcePortfolioTypes(string? SptId)
        {
            string url = $"{urlPrefix}PortfolioType";
            if (!string.IsNullOrEmpty(SptId))
            {
                url += $"?sptId={SptId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _portfolioTypesList = (List<PortfolioTypesReadDto>)JSON.Deserializer<List<PortfolioTypesReadDto>>(_responseBody);
                return _portfolioTypesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Inser new Portfolio.
        /// </summary>
        /// <param name="instrumentCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertSourcePortfolio(PortfolioCreateDto portfolioCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (portfolioCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(portfolioCreateDto);

                string url = $"{urlPrefix}Portfolio?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Update the selected Portfolio.
        /// </summary>
        /// <param name="portfolioCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateSourcePortfolio(PortfolioCreateDto portfolioCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (portfolioCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(portfolioCreateDto);

                string url = $"{urlPrefix}Portfolio?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete the selected Portfolio.
        /// </summary>
        /// <param name="spId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteSourcePortfolio(int spId)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}Portfolio?spId={spId}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Insert a new Portfolio Type.
        /// </summary>
        /// <param name="portfolioTypeCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertSourcePortfolioType(PortfolioTypeCreateDto portfolioTypeCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (portfolioTypeCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(portfolioTypeCreateDto);

                string url = $"{urlPrefix}PortfolioType?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Update the selected Portfolio Type
        /// </summary>
        /// <param name="portfolioCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateSourcePortfolioType(PortfolioTypeCreateDto portfolioTypeCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (portfolioTypeCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(portfolioTypeCreateDto);

                string url = $"{urlPrefix}PortfolioType?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete the selected Portfolio Type.
        /// </summary>
        /// <param name="sptId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteSourcePortfolioType(int sptId)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}PortfolioType?sptId={sptId}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Fetch a Settlement Account or all Settlement Accounts.
        /// </summary>
        /// <param name="SsaId"></param>
        /// <returns></returns>
        public async Task<List<SettlementAccountsReadDto>> FetchSourceSettlementAccount(string? SsaId)
        {
            string url = $"{urlPrefix}SettlementAccount";
            if (!string.IsNullOrEmpty(SsaId))
            {
                url += $"?ssaId={SsaId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _settlementAccountsList = (List<SettlementAccountsReadDto>)JSON.Deserializer<List<SettlementAccountsReadDto>>(_responseBody);
                return _settlementAccountsList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Insert a new Settlement Account.
        /// </summary>
        /// <param name="settlementAccountCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertSourceSettlementAccount(SettlementAccountCreateDto settlementAccountCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (settlementAccountCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(settlementAccountCreateDto);

                string url = $"{urlPrefix}SettlementAccount?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Update the selected Settlement Account.
        /// </summary>
        /// <param name="settlementAccountCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateSourceSettlementAccount(SettlementAccountCreateDto settlementAccountCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (settlementAccountCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(settlementAccountCreateDto);

                string url = $"{urlPrefix}SettlementAccount?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        public async Task<UserMessage> DeleteSourceSettlementAccount(int ssaId)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}SettlementAccount?ssaId={ssaId}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Fetch a Settlement Place or all Settlement Places.
        /// </summary>
        /// <param name="sspId"></param>
        /// <returns></returns>
        public async Task<List<SettlementPlacesReadDto>> FetchSourceSettlementPlaces(string? sspId)
        {
            string url = $"{urlPrefix}SettlementPlace";
            if (!string.IsNullOrEmpty(sspId))
            {
                url += $"?sspId={sspId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _settlementAccountsList = (List<SettlementPlacesReadDto>)JSON.Deserializer<List<SettlementPlacesReadDto>>(_responseBody);
                return _settlementAccountsList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Insert new Source Settlement Place.
        /// </summary>
        /// <param name="settlementPlaceCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertSourceSettlementPlace(SettlementPlaceCreateDto settlementPlaceCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (settlementPlaceCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(settlementPlaceCreateDto);

                string url = $"{urlPrefix}SettlementPlace?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Update the selected Settlement Place.
        /// </summary>
        /// <param name="settlementPlaceCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateSourceSettlementPlace(SettlementPlaceCreateDto settlementPlaceCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (settlementPlaceCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(settlementPlaceCreateDto);

                string url = $"{urlPrefix}SettlementPlace?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete the slected Settlement Place.
        /// </summary>
        /// <param name="sspId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteSourceSettlementPlace(int sspId)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}SettlementPlace?sspId={sspId}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Fetch all the Status or a selected Status.
        /// </summary>
        /// <param name="sId"></param>
        /// <returns></returns>
        public async Task<List<StatusReadDto>> FetchStatus(string? sId)
        {
            string url = $"{urlPrefix}Status";
            if (!string.IsNullOrEmpty(sId))
            {
                url += $"?sId={sId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _statusList = (List<StatusReadDto>)JSON.Deserializer<List<StatusReadDto>>(_responseBody);
                return _statusList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Insert the new Status.
        /// </summary>
        /// <param name="statusCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertStatus(StatusCreateDto statusCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (statusCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(statusCreateDto);

                string url = $"{urlPrefix}Status?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Update the selected Status.
        /// </summary>
        /// <param name="statusCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateStatus(StatusCreateDto statusCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (statusCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(statusCreateDto);

                string url = $"{urlPrefix}Status?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete the selected Status.
        /// </summary>
        /// <param name="sId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeleteStatus(int sId)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}Status?sId={sId}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Fetch a Payment Type or all Payment Types.
        /// </summary>
        /// <param name="ptId"></param>
        /// <returns></returns>
        public async Task<List<PaymentTypesReadDto>> FetchPaymentTypes(string? ptId)
        {
            string url = $"{urlPrefix}PaymentType";
            if (!string.IsNullOrEmpty(ptId) && ptId != "-1")
            {
                url += $"?ptId={ptId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _paymentTypeList = (List<PaymentTypesReadDto>)JSON.Deserializer<List<PaymentTypesReadDto>>(_responseBody);
                return _paymentTypeList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// Update the selected Payment Type.
        /// </summary>
        /// <param name="paymentTypeCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdatePaymentType(PaymentTypeCreateDto paymentTypeCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (paymentTypeCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(paymentTypeCreateDto);

                string url = $"{urlPrefix}PaymentType?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Insert a new Payment Type.
        /// </summary>
        /// <param name="paymentTypeCreateDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertPaymentType(PaymentTypeCreateDto paymentTypeCreateDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (paymentTypeCreateDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(paymentTypeCreateDto);

                string url = $"{urlPrefix}PaymentType?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Delete the selected Payment Type.
        /// </summary>
        /// <param name="ptId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserMessage> DeletePaymentType(int ptId)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            string url = $"{urlPrefix}PaymentType?ptId={ptId}&clientId={clientId}";
            try
            {
                client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                responseMessage = await client.DeleteAsync(url);
                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return userMessage;
                }
                else
                {
                    return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                }
            }
            catch (Exception ex)
            {
                userMessage.Message = ex.Message;
                return userMessage;
            }
        }

        /// <summary>
        /// Fetch all the current Incoming Funds.
        /// </summary>
        /// <returns></returns>
        public async Task<List<IncomingFundsReadDto>> FetchIncomingFunds()
        {
            string url = $"{urlPrefix}IncomingFunds?clientId={clientId}";

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _incomingFundsList = (List<IncomingFundsReadDto>)JSON.Deserializer<List<IncomingFundsReadDto>>(_responseBody);
                return _incomingFundsList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        public async Task<List<SwiftReadErrorsReadDto>> FetchSwiftReadErrors()
        {
            string url = $"{urlPrefix}SwiftReadErrors?clientId={clientId}";

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _swiftReadErrorsList = (List<SwiftReadErrorsReadDto>)JSON.Deserializer<List<SwiftReadErrorsReadDto>>(_responseBody);
                return _swiftReadErrorsList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        public async Task<List<SwiftReadDetailsReadDto>> FetchSwiftReadDetails()
        {
            string url = $"{urlPrefix}SwiftReadDetails?clientId={clientId}";

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _swiftReadDetailsList = (List<SwiftReadDetailsReadDto>)JSON.Deserializer<List<SwiftReadDetailsReadDto>>(_responseBody);
                return _swiftReadDetailsList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }


        /// <summary>
        /// Insert a new Payment.
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertPayment(Payment payment)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (payment != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(payment);

                string url = $"{urlPrefix}Payment?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    string newId = (string)JObject.Parse(responseBody)["newID"];
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        userMessage.Message = responseMessage.ReasonPhrase;
                        userMessage.Message = newId;
                        return userMessage;
                    }
                    else
                    {
                        userMessage.Message = responseBody;
                        return userMessage;
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    Logging logging = new()
                    {
                        Level = "Error",
                        Exception = ex.StackTrace,
                        Message = ex.Message
                    };

                    await InsertToLog(logging, 0);
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Update the selected Payment.
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdatePayment(Payment payment)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (payment != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(payment);

                string url = $"{urlPrefix}Payment?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        userMessage.Message = responseMessage.ReasonPhrase;
                        return userMessage;
                    }
                    else
                    {
                        userMessage.Message = responseBody;
                        return userMessage;
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    Logging logging = new()
                    {
                        Level = "Error",
                        Exception = ex.StackTrace,
                        Message = ex.Message
                    };

                    await InsertToLog(logging, 0);
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Insert a new FOP Payment.
        /// </summary>
        /// <param name="fop"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertFOPPayment(FOP fop)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (fop != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(fop);

                string url = $"{urlPrefix}PaymentFOP?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    string newId = (string)JObject.Parse(responseBody)["newID"];
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        userMessage.Message = responseMessage.ReasonPhrase;
                        userMessage.Message = newId;
                        return userMessage;
                    }
                    else
                    {
                        userMessage.Message = responseBody;
                        return userMessage;
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    Logging logging = new()
                    {
                        Level = "Error",
                        Exception = ex.StackTrace,
                        Message = ex.Message
                    };

                    await InsertToLog(logging, 0);
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Update the selected Payment.
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        public async Task<UserMessage> UpdateFOPPayment(FOP fop)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (fop != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(fop);

                string url = $"{urlPrefix}PaymentFOP?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        userMessage.Message = responseMessage.ReasonPhrase;
                        return userMessage;
                    }
                    else
                    {
                        userMessage.Message = responseBody;
                        return userMessage;
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Fetch Benficiary Types.
        /// </summary>
        /// <param name="pfCode"></param>
        /// <returns></returns>
        public async Task<List<BeneficiaryReadDto>> FetchBenficiaries()
        {
            string url = $"{urlPrefix}Beneficiary?clientId={clientId}";

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _beneficiary = (List<BeneficiaryReadDto>)JSON.Deserializer<List<BeneficiaryReadDto>>(_responseBody);
                return _beneficiary;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }


        /// <summary>
        /// Fetch Template.
        /// </summary>
        /// <param name="paId"></param>
        /// <returns></returns>
        public async Task<List<TemplateReadDto>> FetchTemplates()
        {
            string url = $"{urlPrefix}Template?clientId={clientId}";

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _template = (List<TemplateReadDto>)JSON.Deserializer<List<TemplateReadDto>>(_responseBody);
                return _template;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        public async Task<List<PaymentGridViewReadDto>> FetchPaymentGridView(string? pId)
        {
            string url = $"{urlPrefix}PaymentGridView";
            if (!string.IsNullOrEmpty(pId))
            {
                url += $"?pId={pId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }
            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _paymentGridView = (List<PaymentGridViewReadDto>)JSON.Deserializer<List<PaymentGridViewReadDto>>(_responseBody);
                return _paymentGridView;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        public async Task<List<PaymentSelectAccountReadDto>> FetchPaymentSelectAccount(string? spId)
        {
            string url = $"{urlPrefix}PaymentSelectAccount";
            //string url = $"{urlPrefix}PaymentGridView";
            if (!string.IsNullOrEmpty(spId))
            {
                url += $"?spId={spId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _paymentSelectAccount = (List<PaymentSelectAccountReadDto>)JSON.Deserializer<List<PaymentSelectAccountReadDto>>(_responseBody);

          

                return _paymentSelectAccount;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// FetchSwift.
        /// </summary>
        /// <param name="SwiftDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> FetchSwift(SwiftDto swiftDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (swiftDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(swiftDto);

                string url = $"{urlPrefixSwift}MT540?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);                    
                    userMessage.Message = responseBody;
                    return userMessage;                    
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        public async Task<UserMessage> FetchSwiftMT542(SwiftMT542Dto swiftMT542Dto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (swiftMT542Dto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(swiftMT542Dto);

                string url = $"{urlPrefixSwift}MT542?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);                    
                    userMessage.Message = responseBody;
                    return userMessage;                    
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        public async Task<List<FilePathReadDto>> FetchFilePaths()
        {
            string url = $"{urlPrefix}FilePath?clientId={clientId}";

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _filePaths = (List<FilePathReadDto>)JSON.Deserializer<List<FilePathReadDto>>(_responseBody);
                return _filePaths;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        public async Task<List<FileExportReadDto>> FetchFileExport()
        {
            string url = $"{urlPrefix}FileExport?clientId={clientId}";

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _fileExport = (List<FileExportReadDto>)JSON.Deserializer<List<FileExportReadDto>>(_responseBody);
                return _fileExport;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        public async Task<PaymentHomeAddressReadDto> FetchPaymentHomeAddress()
        {
            string url = $"{urlPrefix}PaymentHomeAddress?clientId={clientId}";

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var result = (PaymentHomeAddressReadDto)JSON.Deserializer<PaymentHomeAddressReadDto>(_responseBody);
                return result;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        public async Task<List<PaymentAddressReadDto>> FetchPaymentAddress(string? paId, string? tempName)
        {
            string url = $"{urlPrefix}PaymentAddress";
            if (!string.IsNullOrEmpty(paId) && paId != "-1")
            {
                url += $"?paId={paId}&clientId={clientId}";
            }
            else if (!string.IsNullOrEmpty(tempName))
            {
                url += $"?templateName={tempName}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var result = (List<PaymentAddressReadDto>)JSON.Deserializer<List<PaymentAddressReadDto>>(_responseBody);
                return result;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        public async Task<List<PaymentFirmPortfolioReadDto>> FetchFirmPortfolio()
        {
            string url = $"{urlPrefix}FirmPortfolio?clientId={clientId}";

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _paymentHomeAddress = (List<PaymentFirmPortfolioReadDto>)JSON.Deserializer<List<PaymentFirmPortfolioReadDto>>(_responseBody);
                return _paymentHomeAddress;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }
        public async Task<List<PaymentReceiveDeliverTypeReadDto>> FetchPaymentReceiveDeliverType()
        {
            string url = $"{urlPrefix}PaymentReceiveDeliverType?clientId={clientId}";

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                return (List<PaymentReceiveDeliverTypeReadDto>)JSON.Deserializer<List<PaymentReceiveDeliverTypeReadDto>>(_responseBody);
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        /// <summary>
        /// FetchSwiftBackOfficePayment.
        /// </summary>
        /// <param name="SwiftDto"></param>
        /// <returns></returns>
        public async Task<UserMessage> FetchSwiftBackOfficePayment(SwiftBackOfficePaymentDto swiftBackOfficePaymentDto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (swiftBackOfficePaymentDto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(swiftBackOfficePaymentDto);

                string url = $"{urlPrefixSwift}MT103?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        userMessage.Message = responseBody;
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        public async Task<List<BrokersReadDto>> FetchPaymentBrokers(string? bCode)
        {
            string url = $"{urlPrefix}PaymentBroker";
            if (!string.IsNullOrEmpty(bCode))
            {
                url += $"?bCode={bCode}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _settlementAccountsList = (List<BrokersReadDto>)JSON.Deserializer<List<BrokersReadDto>>(_responseBody);
                return _settlementAccountsList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        public async Task<UserMessage> FetchSwiftBackOfficePaymentmt200(SwiftBackOfficePaymentmt200Dto swiftBackOfficePaymentmt200Dto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (swiftBackOfficePaymentmt200Dto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(swiftBackOfficePaymentmt200Dto);

                string url = $"{urlPrefixSwift}MT200?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);                    
                    userMessage.Message = responseBody;
                    return userMessage;                    
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        public async Task<UserMessage> FetchSwiftBackOfficePaymentMt210(SwiftBackOfficePaymentMt210Dto swiftBackOfficePaymentMt210Dto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (swiftBackOfficePaymentMt210Dto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(swiftBackOfficePaymentMt210Dto);

                string url = $"{urlPrefixSwift}MT210?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    userMessage.Message = responseBody;
                    return userMessage;                    
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        public async Task<UserMessage> FetchSwiftBackOfficePaymentMt299(SwiftBackOfficePaymentMt299Dto swiftBackOfficePaymentMt299Dto)
        {
            UserMessage userMessage = new();

            using var client = new HttpClient();
            if (swiftBackOfficePaymentMt299Dto != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(swiftBackOfficePaymentMt299Dto);

                string url = $"{urlPrefixSwift}MT299?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    userMessage.Message = responseBody;
                    return userMessage;                    
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        public async Task<List<BrokerClearerReadDto>> FetchBrokerCleaers(string? brkId, string? clrId)
        {
            string url = $"{urlPrefix}BrokerClearer?";

            if (string.IsNullOrEmpty(brkId) && string.IsNullOrEmpty(clrId))
                {
                    url += $"?clientId={clientId}";
                }
            else if (!string.IsNullOrEmpty(brkId) && string.IsNullOrEmpty(clrId))
                {
                    url += $"?brkId={brkId}&clientId={clientId}";
                }
            else if (string.IsNullOrEmpty(brkId) && !string.IsNullOrEmpty(clrId))
            {
                url += $"?clrId={clrId}&clientId={clientId}";
            }
            else
            {
                url += $"?brkId={brkId}&clrId={clrId}&clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _brokerClearers = (List<BrokerClearerReadDto>)JSON.Deserializer<List<BrokerClearerReadDto>>(_responseBody);
                return _brokerClearers;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

        //  *** End Of Main Endpoints ***


        /// <summary>
        /// Insert logging detials to the table FlowApiLogging.
        /// </summary>
        /// <param name="logging"></param>
        /// <returns></returns>
        public async Task<UserMessage> InsertToLog(Logging logging, int userId)
        {
            UserMessage userMessage = new();

            using HttpClient client = new();
            if (logging != null)
            {
                //Serialise the Payload.
                var payload = JSON.Serializer(logging);

                string url = $"{urlPrefix}Logging?clientId={clientId}";
                try
                {
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    StringContent requestContent = new(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PutAsync(url, requestContent);
                    string responseBody = await responseMessage.Content.ReadAsStringAsync();
                    var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return userMessage;
                    }
                    else
                    {
                        return userMessage = (UserMessage)JSON.Deserializer<UserMessage>(responseBody);
                    }
                }
                catch (Exception ex)
                {
                    userMessage.Message = ex.Message;
                    return userMessage;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Fetch Payment Ra Range Type.
        /// </summary>
        /// <param name="ptId"></param>
        /// <returns></returns>
        public async Task<List<PaymentRARangeTypeReadDto>> FetchPaymentRARangeTypes(string? ptId)
        {
            string url = $"{urlPrefix}PaymentRARangeType";
            if (!string.IsNullOrEmpty(ptId) && ptId != "-1")
            {
                url += $"?ctyId={ptId}&clientId={clientId}";
            }
            else
            {
                url += $"?clientId={clientId}";
            }

            try
            {
                using (var client = new HttpClient())
                {
                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("ApiKey", apiKey);
                    responseMessage = await client.GetAsync(url);
                }
                string _responseBody = await responseMessage.Content.ReadAsStringAsync();
                var _responseCode = Convert.ToInt32(responseMessage.StatusCode);

                var _paymentCountriesList = (List<PaymentRARangeTypeReadDto>)JSON.Deserializer<List<PaymentRARangeTypeReadDto>>(_responseBody);
                return _paymentCountriesList;
            }
            catch (Exception ex)
            {
                Logging logging = new()
                {
                    Level = "Error",
                    Exception = ex.StackTrace,
                    Message = ex.Message
                };

                await InsertToLog(logging, 0);
                return null;
            }
        }

    }
}