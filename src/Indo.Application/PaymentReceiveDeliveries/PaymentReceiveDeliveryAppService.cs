﻿using Indo.FlowApi;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.PaymentReceiveDeliveries
{
    public class PaymentReceiveDeliveryAppService : IndoAppService, IPaymentReceiveDeliveryAppService
    {
        private readonly Endpoints _endpoints;

        public PaymentReceiveDeliveryAppService()
        {
            _endpoints = new Endpoints();
        }

        public async Task<List<PaymentRecieveDeliveriesReadDto>> GetListAsync()
        {
            List<PaymentRecieveDeliveriesReadDto> paymentReceiveDeliver = await _endpoints.FetchPaymentReceiveDeliveries(string.Empty);
            if (paymentReceiveDeliver != null)
            {
                return paymentReceiveDeliver;
            }
            else
            {
                return null;
            }
        }
    }
}