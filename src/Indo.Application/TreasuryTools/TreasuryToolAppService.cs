﻿using Indo.Currencies;
using Indo.FlowApi;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.TreasuryTools
{
    public class TreasuryToolAppService : IndoAppService, ITreasuryToolAppService
    {
        private readonly Endpoints _endpoints;
        public TreasuryToolAppService()
        {
            _endpoints = new Endpoints();
        }

        public List<MovementReadDto> GetMovementsList()
        {
            var dtos = new List<MovementReadDto>
            {
                new MovementReadDto{ Portfolio ="Portfolio A", CurrentBalanceA=100.28f, CurrentBalanceB=150.25f },
                new MovementReadDto{ Portfolio ="Portfolio B", CurrentBalanceA=200.28f, CurrentBalanceB=250.25f }
            };

            return dtos;
        }

        public async Task<List<FlowCurrency>> GetCurrencyListAsync()
        {
            var dtos = await _endpoints.FetchCurrencies("");

            return dtos;
        }
    }
}
