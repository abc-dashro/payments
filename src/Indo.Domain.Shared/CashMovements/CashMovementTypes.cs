﻿namespace Indo.CashMovements
{
    public static class CashMovementTypes
    {
        public const bool Other = true;
        public const bool Main = false;
    }
}