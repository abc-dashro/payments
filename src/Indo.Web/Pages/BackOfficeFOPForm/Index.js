﻿$(function () {
    var PaymentId = (new URL(document.location)).searchParams.get("id");
    /* Localization */
    var l = abp.localization.getResource('Indo');
    $("select").removeClass("custom-select");
    var ClientSwiftCode;
    var BrokerSwiftCode;
    var DeliveringAgentSwiftCode;
    var SettlementSwiftCode;
    var ClearerSwiftCode;
    $('.symboleOk').hide();
    $('.symboleRemove').hide();
    $('.symboleOkDelivAgent').hide();
    $('.symboleRemoveDelivAgent').hide();
    $('.symboleOkCle').hide();
    $('.symboleRemoveCle').hide();
    $('.symboleOkBrkrAcc').hide();
    $('.symboleRemoveBrkrAcc').hide();
    $(".error").hide();
    $(".errorClearer").hide();
    $(".errorSfiwtcode").hide();
    $(".errorPlaceOfSettle").hide();
    var instrumentData;
    $('#validatSwiftBrkrAcc').attr('disabled', true);
    $('#validatSwiftClearer').attr('disabled', true);
    $('#validatSwift').attr('disabled', true);
    $('#validatSwiftPlaceOfSettle').attr('disabled', true);
    $('.errorAmount').hide();
    $('#ViewModel_Amount').keyup();
    if (PaymentId) {
        $("#loader").show();        
        $(".tabbable").addClass("blurContent");
    }
    else {
        $("#loader").hide();
        $(".tabbable").removeClass("blurContent");
    }
    
    var BrokerAccountNumber;
    var validateForm = false;    
    $('#btnNewTrans').on('click', function () {
        if (PaymentId) { window.location.href = '/BackOfficeFOPForm'; }
        else { clearFields(); }
    });
    var Res_msg = $('#statusMessaage').val();
    if (Res_msg.length > 0) {

        if (Res_msg) {
            abp.notify.success(Res_msg);
            clearFields();
        } else {
            abp.notify.warn(Res_msg);
        }
    }
    $("#ViewModel_Amount").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorAmount'>Amount should be greater than 0<span>")
    $('.errorAmount').hide();
    $('#ViewModel_Amount').keyup(function (e) {
        var amt = $('#ViewModel_Amount').val();
        var formatAmt;
        formatAmt = parseFloat(amt.replaceAll(',', ""));

        if (formatAmt <= 0 || formatAmt == 0) {
            $('.errorAmount').show();
            validateForm = false;
        }
        else {
            $('.errorAmount').hide();
            validateForm = true;
        }
    });

    $("#ViewModel_Amount").focusin(function () {
        $('#ViewModel_Amount').val(parseFloat($('#ViewModel_Amount').val().replaceAll(',', "")));
    });

    $("#ViewModel_Amount").focusout(function () {
        var amt = $('#ViewModel_Amount').val();
        var formatAmt;
        if (!amt.includes(',')) {
            formatAmt = parseFloat(amt).toLocaleString();
        }
        else {
            formatAmt = parseFloat(amt.replaceAll(',', "")).toLocaleString();
        }
        if (!amt.includes('.')) {
            $('#ViewModel_Amount').val(formatAmt);
        } else {
            var modifyAmt = parseFloat(formatAmt.replaceAll(',', "")).toFixed(2)
            $('#ViewModel_Amount').val(parseFloat(modifyAmt).toLocaleString());
        }
    });

    $(function () {
        $("#PayType, #ViewModel_CCY, #ViewModel_Instrument, #ViewModel_Isin, #ViewModel_CashAcc, #ViewModel_Portfolio").select2({
            placeholder: 'Select',
            language: {
                noResults: function (params) {
                    return "Searching...";
                }
            }
        });

        indo.backOfficeFOPs.backOfficeFOP.fetchPaymentHomeAddress("")
            .then(function (data) {
                if (data != null) {
                    ClientSwiftCode = data.swiftCode;
                }
            });

        $('input:radio[name="ViewModel.CheckboxInstrument"]').click(
            function () {
                loadInstrument();
                CheckboxInstrumentVal = $('input:radio[name="ViewModel.CheckboxInstrument"]').is(':checked') && $(this).val();
            });

        $('label[for="ViewModel.CheckboxInstrumentRadio1"]').css('margin-top', "2.5rem");        
        populateFields();
    });

    $('#PayType').on('change', function () {
        if ($("#PayType option:selected").text() == "Deliver Free") {
            $('label[for="ViewModel_sfiwtcode"]').html('Receive Agent Swift Code: ');
            $('label[for="ViewModel_DelivAgentSwiftCode"]').html('Receive Agent: ');
            $('label[for="ViewModel_DelAgtAccNo"]').html('Rec Agt Acc No: ');
        }
        else {
            $('label[for="ViewModel_sfiwtcode"]').html('Deliv Agent Swift Code: ');
            $('label[for="ViewModel_DelivAgentSwiftCode"]').html('Deliv Agent: ');
            $('label[for="ViewModel_DelAgtAccNo"]').html('Del Agt Acc No: ');
        }
    });
    $('#ViewModel_BrkrAcc').on('change', function () {
        var BankNameId = $('#ViewModel_BrkrAcc option:selected').val();
        indo.backOfficeFOPs.backOfficeFOP.fetchSwiftCodes(BankNameId)
            .then(function (data) {
                BrokerSwiftCode = data[0].sC_SwiftCode;
            });
    });
    $('#ViewModel_DelivAgent').on('change', function () {
        var BankNameId = $('#ViewModel_DelivAgent option:selected').val();
        indo.backOfficeFOPs.backOfficeFOP.fetchSwiftCodes(BankNameId)
            .then(function (data) {
                DeliveringAgentSwiftCode = data[0].sC_SwiftCode;
            });
    });
    $('#ViewModel_PlaceOfSettle').on('change', function () {
        var BankNameId = $('#ViewModel_PlaceOfSettle option:selected').val();
        indo.backOfficeFOPs.backOfficeFOP.fetchSwiftCodes(BankNameId)
            .then(function (data) {
                SettlementSwiftCode = data[0].sC_SwiftCode;
            });
    });

    $('#ViewModel_CCY').on('change', function () {
        triggerCashAcc();
        loadInstrument();
    });

    $("a[href='#tab5']").on('show.bs.tab', function (e) {
        $('#tab5').html("");
        if ($('#ViewModel_Amount').val().replaceAll(',', "") > 0) {
            if ($('#ViewModel_Instrument').val() > 0) {
                if (instrumentData != null) {
                    var InstrumentISIN = $('#ViewModel_Instrument option:selected').val();
                    var instrumentsData = instrumentData.filter(x => x.sI_ID == InstrumentISIN);
                }

                if ($("#PayType option:selected").text() == "Receive Free") {
                    const swiftDto = {
                        clientSwiftCode: ClientSwiftCode ? ClientSwiftCode : "",
                        brokerSwiftCode: BrokerSwiftCode ? BrokerSwiftCode : "",
                        orderRef: "REFER0000000",
                        brokerAccountNumber: BrokerAccountNumber ? BrokerAccountNumber : "",
                        tradeType: "NEWM",
                        cancelOrderRef: "",
                        isEquity: true,
                        tradeDate: $('#ViewModel_TransDate').val(),
                        settleDate: $('#ViewModel_SettleDate').val(),
                        amount: parseFloat($('#ViewModel_Amount').val().replaceAll(',', "")),
                        instrumentISIN: instrumentsData.length != 0 ? instrumentsData[0].sI_ISIN : "",
                        clearerSwiftCode: ClearerSwiftCode ? ClearerSwiftCode : "",
                        deliveringAgentSwiftCode: DeliveringAgentSwiftCode ? DeliveringAgentSwiftCode : "",
                        deliveringAgent: $('#ViewModel_DelAgtAccNo').val() ? $('#ViewModel_DelAgtAccNo').val() : "",
                        settlementSwiftCode: SettlementSwiftCode ? SettlementSwiftCode : "",
                        instructions: $('#ViewModel_InstructionsId').val(),
                        email: "",
                        ftp: false
                    };
                    if (instrumentsData != null && instrumentsData[0].sI_ISIN != null && BrokerAccountNumber != null && BrokerSwiftCode != null && $('#ViewModel_DelAgtAccNo').val() != '' && $('#ViewModel_sfiwtcodeBrkrAcc').val() != '' && $('#ViewModel_sfiwtcode').val() != '') {
                        indo.backOfficeFOPs.backOfficeFOP.fetchSwift(swiftDto)
                            .then(function (data) {
                                if (data != null) {
                                    var swiftRec = data.split("\n").join("<br />");
                                    $('#tab5').html(swiftRec);
                                }
                            });
                    }
                    else if (BrokerAccountNumber == null) {
                        abp.notify.warn("This broker's account number is empty. Please populate it for the broker");
                    }
                    else if (instrumentsData != null && instrumentsData[0].sI_ISIN == null) {
                        abp.notify.warn("The ISIN of selected instrument is empty. Please populate it");
                    }
                    else {
                        abp.notify.warn("Please fill required fields<br> 1.instrument<br> 2.deliv agent<br> 3.brkr acc<br> 4.Del Agt Acc No");
                    }

                }
                else {
                    const swiftDtoMT542 = {
                        clientSwiftCode: ClientSwiftCode ? ClientSwiftCode : "",
                        brokerSwiftCode: BrokerSwiftCode ? BrokerSwiftCode : "",
                        orderRef: "DELIV0000000",
                        brokerAccountNo: BrokerAccountNumber ? BrokerAccountNumber : "",
                        tradeType: "NEWM",
                        cancelOrderRef: "",
                        isEquity: true,
                        tradeDate: $('#ViewModel_TransDate').val(),
                        settleDate: $('#ViewModel_SettleDate').val(),
                        amount: parseFloat($('#ViewModel_Amount').val().replaceAll(',', "")),
                        instrumentISIN: instrumentsData.length != 0 ? instrumentsData[0].sI_ISIN : "",
                        clearerSwiftCode: ClearerSwiftCode ? ClearerSwiftCode : "",
                        agentAccountNo: $('#ViewModel_DelAgtAccNo').val() ? $('#ViewModel_DelAgtAccNo').val() : "",
                        settlementSwiftCode: SettlementSwiftCode ? SettlementSwiftCode : "",
                        instructions: $('#ViewModel_InstructionsId').val(),
                        email: "",
                        ftp: false
                    };
                    if (instrumentsData != null && instrumentsData[0].sI_ISIN != null && BrokerAccountNumber != null && BrokerSwiftCode != null && $('#ViewModel_DelAgtAccNo').val() != '' && $('#ViewModel_sfiwtcodeBrkrAcc').val() != '' && $('#ViewModel_sfiwtcodeClearer').val() != '' && $('#ViewModel_sfiwtcodePlaceOfSettle').val() != '' && $('#ViewModel_sfiwtcode').val() != '') {
                        indo.backOfficeFOPs.backOfficeFOP.fetchSwiftMT542(swiftDtoMT542)
                            .then(function (data) {
                                if (data != null) {
                                    var swiftRec = data.split("\n").join("<br />");
                                    $('#tab5').html(swiftRec);
                                }
                            });
                    }
                    else if (BrokerAccountNumber == null) {
                        abp.notify.warn("This broker's account number is empty. Please populate it for the broker");
                    }
                    else if (instrumentsData != null && instrumentsData[0].sI_ISIN == null) {
                        abp.notify.warn("The ISIN of selected instrument is empty. Please populate it");
                    }
                    else {
                        abp.notify.warn("Please fill required fields<br> 1.Instrument<br> 2.Deliv Agent<br> 3.Brkr Acc<br> 4.Clearer<br> 5.Place of Settle<br> 6.Del Agt Acc No");
                    }
                }
            }
            else {
                abp.notify.warn("Please fill required fields <br> 1.Instrument<br> 2.Deliv Agent <br> 3.Brkr Acc <br> 4.Clearer <br> 5.Place of Settle <br> 6.Del Agt Acc No");
            }
        }
        else {
            abp.notify.warn("Amount should be greater than 0");
        }
    });

    $('#ViewModel_Instrument').on('select2:opening', function (e) {
        var selected_CCY = $('#ViewModel_CCY option:selected').text() != '' && $('#ViewModel_CCY option:selected').text() != 'Select';
        if (!selected_CCY) {
            abp.notify.warn("Please select CCY")
        }
    });

    function loadInstrument(e) {
        $('#ViewModel_Instrument').html("")
        if (instrumentData != null) {
            filterInstruments(instrumentData);
        }
        else {
            indo.backOfficeFOPs.backOfficeFOP.fetchSourceInstruments(-1)
                .then(function (data) {
                    if (data != null) {
                        instrumentData = data;
                        filterInstruments(data);
                    }
                });
        }
    }

    function filterInstruments(data) {
        var ccyVal = $('#ViewModel_CCY option:selected').text();
        if ($('input[name="ViewModel.CheckboxInstrument"]:checked').val() == 1) {
            var instrumentsData = data.filter(x => x.cR_Name1 == ccyVal).map(({ sI_ID, sI_Name }) => ({ id: sI_ID, text: sI_Name }));
            instrumentsData.unshift({ id: -1, text: 'Select' })
            $("#ViewModel_Instrument").select2({
                data: instrumentsData,
                placeholder: 'Select',
            });
        }
        else {
            var instrumentsData = data.filter(x => x.cR_Name1 == ccyVal).map(({ sI_ID, sI_ISIN }) => ({ id: sI_ID, text: sI_ISIN }));
            instrumentsData.unshift({ id: -1, text: 'Select' })
            $("#ViewModel_Instrument").select2({
                data: instrumentsData,
                placeholder: 'Select',
            });
        }
        populateInstrumentCashAcc();
    }

    $('#validatSwift').on('click', function () {
        var swiftCode = $('#ViewModel_sfiwtcode').val()
        indo.backOfficeFOPs.backOfficeFOP.fetchSwiftCodes(-1, swiftCode)
            .then(function (data) {
                if (data.length != 0) {
                    $('.symboleOkDelivAgent').show();
                    $('.symboleRemoveDelivAgent').hide();
                    $('#ViewModel_DelivAgentSwiftCode').val(data[0].sC_BankName);
                    $('#ViewModel_DelivAgent').val(data[0].sC_SwiftCode);
                    DeliveringAgentSwiftCode = data[0].sC_SwiftCode;
                }
                else {
                    $('.symboleOkDelivAgent').hide();
                    $('.symboleRemoveDelivAgent').show();
                    $('#ViewModel_DelivAgentSwiftCode').val("");
                }
            })
    });
    $('#validatSwiftPlaceOfSettle').on('click', function () {
        var swiftCode = $('#ViewModel_sfiwtcodePlaceOfSettle').val()
        indo.backOfficeFOPs.backOfficeFOP.fetchSwiftCodes(-1, swiftCode)
            .then(function (data) {
                if (data.length != 0) {
                    $('.symboleOk').show();
                    $('.symboleRemove').hide();
                    $('#ViewModel_PlaceOfSettleSwiftCode').val(data[0].sC_BankName);
                    $('#ViewModel_PlaceOfSettle').val(data[0].sC_SwiftCode);
                    SettlementSwiftCode = data[0].sC_SwiftCode;
                }
                else {
                    $('.symboleOk').hide();
                    $('.symboleRemove').show();
                    $('#ViewModel_PlaceOfSettleSwiftCode').val("");
                }
            })
    });
    $('#validatSwiftBrkrAcc').on('click', function () {
        var swiftCode = $('#ViewModel_sfiwtcodeBrkrAcc').val()
        indo.backOfficeFOPs.backOfficeFOP.fetchSwiftCodes(-1, swiftCode)
            .then(function (data) {
                if (data.length != 0) {
                    $('.symboleOkBrkrAcc').show();
                    $('.symboleRemoveBrkrAcc').hide();
                    $('#ViewModel_BrkrAccSwiftCode').val(data[0].sC_BankName);
                    $('#ViewModel_BrkrAcc').val(data[0].sC_SwiftCode);
                    BrokerSwiftCode = data[0].sC_SwiftCode;
                    BrokerAccountNumber = data[0].sC_AccountNumber;

                } else {
                    $('.symboleOkBrkrAcc').hide();
                    $('.symboleRemoveBrkrAcc').show();
                    $('#ViewModel_BrkrAccSwiftCode').val("");
                }
            })
    });
    $('#validatSwiftClearer').on('click', function () {
        var swiftCode = $('#ViewModel_sfiwtcodeClearer').val()
        indo.backOfficeFOPs.backOfficeFOP.fetchSwiftCodes(-1, swiftCode)
            .then(function (data) {
                if (data.length != 0) {
                    $('.symboleOkCle').show();
                    $('.symboleRemoveCle').hide();
                    $('#ViewModel_ClearerSwiftCode').val(data[0].sC_BankName);
                    $('#ViewModel_Clearer').val(data[0].sC_SwiftCode);
                    ClearerSwiftCode = data[0].sC_SwiftCode;

                }
                else {
                    $('.symboleOkCle').hide();
                    $('.symboleRemoveCle').show();
                    $('#ViewModel_ClearerSwiftCode').val("");
                }
            })
    });

    function clearFields() {
        $('#ViewModel_BrkrAcc').val("");
        $('#ViewModel_Clearer').val("");
        $('#ViewModel_PlaceOfSettle').val("");
        $('#ViewModel_DelivAgent').val("");
        $('#ViewModel_sfiwtcodeClearer').val("");
        $('#ViewModel_sfiwtcodeBrkrAcc').val("");
        $('#ViewModel_sfiwtcodePlaceOfSettle').val("");
        $('#ViewModel_sfiwtcode').val("");
        $('#ViewModel_PlaceOfSettleSwiftCode').val("");
        $('#ViewModel_DelivAgentSwiftCode').val("");
        $('#ViewModel_ClearerSwiftCode').val("");
        $('#ViewModel_BrkrAccSwiftCode').val("");
        $('#ViewModel_Amount').val("0");
        $('#ViewModel_DelAgtAccNo').val("");
        $('#ViewModel_InstructionsId').val("");
        $("#ViewModel_TransDate").val(getTodate());
        $("#ViewModel_SettleDate").val(getTodate());
        $('#PayType').val('-1').trigger("change");
        $('#ViewModel_CCY').val('-1').trigger("change");
        $('#ViewModel_Portfolio').val('-1').trigger("change");
        $('#ViewModel_Instrument').val('-1').trigger("change");
        $('#ViewModel_CashAcc').val('-1').trigger("change");
        $('.symboleOk').hide();
        $('.symboleRemove').hide();
        $('.symboleOkDelivAgent').hide();
        $('.symboleRemoveDelivAgent').hide();
        $('.symboleOkCle').hide();
        $('.symboleRemoveCle').hide();
        $('.symboleOkBrkrAcc').hide();
        $('.symboleRemoveBrkrAcc').hide();
    };

    function getTodate() {
        var today = new Date();
        var yyyy = today.getFullYear();
        let mm = today.getMonth() + 1;
        let dd = today.getDate();
        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;
        return today = yyyy + '-' + mm + '-' + dd;
    }

    $('#ViewModel_Portfolio').on('change', function () {
        triggerCashAcc();
    });

    function triggerCashAcc() {
        var ccyVal = $('#ViewModel_CCY option:selected').text();
        $('#ViewModel_CashAcc option').remove();
        var portfolioVal = $('#ViewModel_Portfolio').val();
        indo.backOfficeFOPs.backOfficeFOP.fetchPaymentSelectAccount(portfolioVal)
            .then(function (data) {
                if (data != null) {
                    var cashAccData = data.filter(x => x.cR_Name1 == ccyVal).map(({ sA_ID, t_Name1 }) => ({ id: sA_ID, text: t_Name1 }));
                    cashAccData.unshift({ id: -1, text: 'Select' });

                    $("#ViewModel_CashAcc").select2({
                        data: cashAccData,
                        placeholder: 'Select',
                    });
                }

            })
    }

    $('#ViewModel_CashAcc').on('select2:opening', function (e) {
        if ($('#ViewModel_Portfolio').val() == '-1' || $('#ViewModel_Portfolio').val() == null || $('#ViewModel_CCY').val() == '-1' || $('#ViewModel_CCY').val() == null) {
            abp.notify.warn('Please select portfolio and CCY');
            $('#ViewModel_CashAcc').select2('close');
        }
    });
    $('#ViewModel_sfiwtcodeBrkrAcc').keyup(function (e) {
        if (e.target.value.length == 8 || e.target.value.length == 11) {
            $(".error").hide();
            $('#validatSwiftBrkrAcc').attr('disabled', false);
            validateForm = true;
        }
        else {
            $(".error").show();
            $('#validatSwiftBrkrAcc').attr('disabled', true);
            $('#ViewModel_BrkrAccSwiftCode').val('');
            $('.symboleOkBrkrAcc').hide();
            validateForm = false;
        }
    });
    $('#ViewModel_sfiwtcodeClearer').keyup(function (e) {
        if (e.target.value.length == 8 || e.target.value.length == 11) {
            $(".errorClearer").hide();
            $('#validatSwiftClearer').attr('disabled', false);
            validateForm = true;
        }
        else {
            $(".errorClearer").show();
            $('#validatSwiftClearer').attr('disabled', true);
            $('#ViewModel_ClearerSwiftCode').val('');
            $('.symboleOkCle').hide();
            validateForm = false;
        }
    });
    $('#ViewModel_sfiwtcode').keyup(function (e) {
        if (e.target.value.length == 8 || e.target.value.length == 11) {
            $(".errorSfiwtcode").hide();
            $('#validatSwift').attr('disabled', false);
            validateForm = true;
        }
        else {
            $(".errorSfiwtcode").show();
            $('#validatSwift').attr('disabled', true);
            $('#ViewModel_DelivAgentSwiftCode').val('');
            $('.symboleOkDelivAgent').hide();
            validateForm = false;
        }
    });
    $('#ViewModel_sfiwtcodePlaceOfSettle').keyup(function (e) {
        if (e.target.value.length == 8 || e.target.value.length == 11) {
            $(".errorPlaceOfSettle").hide();
            $('#validatSwiftPlaceOfSettle').attr('disabled', false);
            validateForm = true;
        }
        else {
            $(".errorPlaceOfSettle").show();
            $('#validatSwiftPlaceOfSettle').attr('disabled', true);
            $('#ViewModel_PlaceOfSettleSwiftCode').val('');
            $('.symboleOk').hide();
            validateForm = false;
        }
    });

    $("form").submit(function (e) {
        if (PaymentId) {
            $("#ViewModel_PaymentId").val(PaymentId);
        }
        if ($('#ViewModel_Amount').val() == '0') {
            $('#ViewModel_Amount').keyup();
            e.preventDefault();
            return false;
        }
        if ($("#ViewModel_SettleDate").val() < $("#ViewModel_TransDate").val()) {
            e.preventDefault();
            abp.notify.warn("Settle Date should be greater or equal to Trans Date");
            return false;
        }
        if (validateForm === false) {
            e.preventDefault();
            return false;
        }

        if (validateDate($("#ViewModel_TransDate").val()) < validateDate(new Date(new Date().setHours(0, 0, 0, 0))))
        {
            abp.notify.warn("Trans date should be from today onwards");
            e.preventDefault();
            return false;
        }
    });

    function validateDate(userDate) {
        var userFormattedDate = new Date(userDate);
        var dateString =
            userFormattedDate.getUTCFullYear() + "/" +
            ("0" + (userFormattedDate.getUTCMonth() + 1)).slice(-2) + "/" +
            ("0" + userFormattedDate.getUTCDate()).slice(-2) + " " +
            ("0" + userFormattedDate.getUTCHours()).slice(-2) + ":" +
            ("0" + userFormattedDate.getUTCMinutes()).slice(-2) + ":" +
            ("0" + userFormattedDate.getUTCSeconds()).slice(-2);
        return dateString;
    }

    function populateFields() {
        indo.backOfficeFOPs.backOfficeFOP.fetchPaymentReceiveDeliveries(PaymentId)
            .then(function (data) {
                if (data != null) {
                    var selectedGridData = data;
                    $("#PayType").val(selectedGridData[0].prD_RecType).trigger("change");
                    if ($("#ViewModel_Portfolio").val() == '-1') {
                        $("#ViewModel_Portfolio").val(selectedGridData[0].prD_PortfolioCode).trigger("change");
                    }
                    if ($("#ViewModel_CCY").val() == '-1') {
                        $("#ViewModel_CCY").val(selectedGridData[0].prD_CCYCode).trigger("change");
                    }
                    if ($("#ViewModel_Amount").val() == '0') {
                        $("#ViewModel_Amount").val(selectedGridData[0].prD_Amount);
                    }
                    $('#ViewModel_Checkbox1').prop('checked', selectedGridData[0].prD_SendIMS);
                    $('#ViewModel_Checkbox2').prop('checked', selectedGridData[0].prD_SendMiniOMS);
                    $("#ViewModel_TransDate").val(selectedGridData[0].prD_TransDate.substr(0, 10))
                    $("#ViewModel_SettleDate").val(selectedGridData[0].prD_SettleDate.substr(0, 10))
                    $("#ViewModel_InstructionsId").val(selectedGridData[0].prD_Instructions);
                    $("#ViewModel_DelAgtAccNo").val(selectedGridData[0].prD_AgentAccountNo);
                    $("#ViewModel_sfiwtcodeBrkrAcc").val(selectedGridData[0].prD_BrokerCode);
                    $("#ViewModel_sfiwtcodeClearer").val(selectedGridData[0].prD_ClearerCode);
                    $("#ViewModel_sfiwtcodePlaceOfSettle").val(selectedGridData[0].prD_PlaceofSettlement);
                    if ($('#ViewModel_sfiwtcode').val() == '') {
                        $("#ViewModel_sfiwtcode").val(selectedGridData[0].prD_Agent);
                    }
                    $("#ViewModel_Instrument").val(selectedGridData[0].prD_InstCode).trigger("change");
                    $("#ViewModel_CashAcc").val(selectedGridData[0].prD_CashAccountCode).trigger("change");
                    if (selectedGridData[0].prD_BrokerCode != null && selectedGridData[0].prD_BrokerCode != '') {
                        $('#ViewModel_sfiwtcodeBrkrAcc').keyup();
                        $("#validatSwiftBrkrAcc").trigger("click");
                    }
                    if (selectedGridData[0].prD_Agent != null && selectedGridData[0].prD_Agent != '') {
                        $('#ViewModel_sfiwtcode').keyup();
                        $("#validatSwift").trigger("click");
                    }
                    if (selectedGridData[0].prD_ClearerCode != null && selectedGridData[0].prD_ClearerCode != '') {
                        $('#ViewModel_sfiwtcodeClearer').keyup();
                        $("#validatSwiftClearer").trigger("click");
                    }
                    if (selectedGridData[0].prD_PlaceofSettlement != null && selectedGridData[0].prD_PlaceofSettlement != '') {
                        $('#ViewModel_sfiwtcodePlaceOfSettle').keyup();
                        $("#validatSwiftPlaceOfSettle").trigger("click");
                    }
                    $("#ViewModel_Amount").focusout();
                }
            });
    }

    function populateInstrumentCashAcc() {
        indo.backOfficeFOPs.backOfficeFOP.fetchPaymentReceiveDeliveries(PaymentId)
            .then(function (data) {
                if (data != null) {
                    var selectedGridData = data;
                    $("#ViewModel_Instrument").val(selectedGridData[0].prD_InstCode).trigger("change");
                    $("#ViewModel_CashAcc").val(selectedGridData[0].prD_CashAccountCode).trigger("change");
                    $("#loader").hide();
                    $(".tabbable").removeClass("blurContent");
                }
            });
    }

});

