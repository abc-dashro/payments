using Indo.BackOfficeFOPs;
using Indo.FreeOfPayments;
using Indo.Swifts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;


namespace Indo.Web.Pages.BackOfficeFOPForm
{
    public class IndexModel : IndoPageModel
    {
        private readonly IBackOfficeFOPAppService _backOfficeFOPAppService;
        public IndexModel(IBackOfficeFOPAppService backOfficeFOPAppService)
        {
            _backOfficeFOPAppService = backOfficeFOPAppService;
        }

        [BindProperty]
        public BackOfficeFOPViewModel ViewModel { get; set; }

        public List<SelectListItem> PortfolioList { get; set; }
        public List<SelectListItem> PayTypeList { get; set; }
        public List<SelectListItem> InstrumentList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "1", Text = "Instrument", Selected = true},
            new SelectListItem { Value = "2", Text = "ISIN"}
        };
        public List<SelectListItem> InstrumentListForDropdown { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> IsinListForDropdown { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> CCYList { get; set; }

        public void OnGetAsync()
        {
            ViewModel = new BackOfficeFOPViewModel
            {
                TransDate = DateTime.Now,
                SettleDate = DateTime.Now,
            };

            var portfolioType = _backOfficeFOPAppService.FetchSourcePortfolios("").Result;
            PortfolioList = portfolioType.Select(x => new SelectListItem() { Text = x.SP_Name, Value = x.SP_ID.ToString() }).OrderBy(x => x.Text).ToList();
            PortfolioList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });

            var Type = _backOfficeFOPAppService.FetchPaymentReceiveDeliverType().Result;
            PayTypeList = Type.Select(x => new SelectListItem() { Text = x.rD_Type, Value = x.rD_ID.ToString() }).OrderBy(x => x.Text).ToList();
            PayTypeList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });

            var Ccy = _backOfficeFOPAppService.FetchCurrencies().Result;
            CCYList = Ccy.Select(x => new SelectListItem() { Text = x.Shortcut, Value = x.Id.ToString() }).OrderBy(x => x.Text).ToList();
            CCYList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });
        }

        //public IActionResult OnPost()
        public async Task<IActionResult> OnPostAsync()
        {
            try
            {
                FOP fop = new FOP()
                {
                    PRD_ID = ViewModel.PaymentId,
                    PRD_PortfolioCode = ViewModel.Portfolio,
                    PRD_RecType = ViewModel.PayType,
                    PRD_InstCode = ViewModel.Instrument < 0 ? 0 : ViewModel.Instrument,
                    PRD_Instructions = ViewModel.InstructionsId,
                    PRD_BrokerCode = !string.IsNullOrEmpty(ViewModel.BrkrAcc) ? ViewModel.BrkrAcc : string.Empty,
                    PRD_CashAccountCode = ViewModel.CashAcc,
                    PRD_ClearerCode = !string.IsNullOrEmpty(ViewModel.Clearer) ? ViewModel.Clearer : string.Empty,
                    PRD_Amount = ViewModel.Amount,
                    PRD_CCYCode = ViewModel.CCY,
                    PRD_TransDate = ViewModel.TransDate,
                    PRD_SettleDate = ViewModel.SettleDate,
                    PRD_Agent = ViewModel.DelivAgent,
                    PRD_AgentAccountNo = ViewModel.DelAgtAccNo,
                    PRD_PlaceofSettlement = ViewModel.PlaceOfSettle,
                    PRD_Username = $"{CurrentUser.Name} {CurrentUser.SurName}",
                    PRD_Email = CurrentUser.Email,
                    PRD_SendIMS = ViewModel.Checkbox1,
                    PRD_SendMiniOMS = ViewModel.Checkbox2

                };
                string data = null;

                if (ViewModel.PaymentId != 0)
                {
                    data = _backOfficeFOPAppService.UpdateFOPPayment(fop).Result;
                }
                else
                {
                    data = _backOfficeFOPAppService.InsertFOPPayment(fop).Result;
                }
                if (data != null && ViewModel.PaymentId == 0)
                {
                    TempData["StatusMessage"] =  "Form Submitted Succesfully with id " + data;
                    return RedirectToPage();
                }
                else if (data != null && ViewModel.PaymentId != 0)
                {
                    TempData["StatusMessage"] = "Form Updated Succesfully";
                    return RedirectToPage();
                }
                else
                {
                    throw new UserFriendlyException($"{data}");
                }                
            }
            catch (Exception ex)
            {             
                TempData["StatusMessage"] = $"{ex.Message}";
                return Page();             
            }
        }

        public class BackOfficeFOPViewModel
        {
            public BackOfficeFOPViewModel()
            {
                PlaceOfSettle = string.Empty;
                DelivAgent = string.Empty;
                Clearer = string.Empty;
                BrkrAcc = string.Empty;
            }
            public int Id { get; set; }
            [Required]
            [Range(0, int.MaxValue, ErrorMessage = "{0} is required")]
            [SelectItems(nameof(PayType))]
            [Display(Name = "Type: ")]
            public int PayType { get; set; }

            [Required]
            [Range(0, int.MaxValue, ErrorMessage = "{0} is required")]
            [SelectItems(nameof(Portfolio))]
            [Display(Name = "Portfolio: ")]
            public int Portfolio { get; set; }


            [Display(Name = " ")]
            public int? Instrument { get; set; }


            [Display(Name = " ")]
            public int? Isin { get; set; }


            [Display(Name = "Instrument: ")]
            public string CheckboxInstrument { get; set; }


            [Display(Name = "ISIN: ")]
            public string CheckboxIsin { get; set; }

            [Display(Name = "Instructions/ID: ")]
            public string InstructionsId { get; set; }


            [Display(Name = "Brkr Acc: ")]
            public string BrkrAcc { get; set; }

            [DisabledInput]
            [Display(Name = "Brkr Acc: ")]
            public string BrkrAccSwiftCode { get; set; }

            [Required]
            [Range(0, int.MaxValue, ErrorMessage = "{0} is required")]
            [SelectItems(nameof(CashAcc))]
            [Display(Name = "Cash Acc: ")]
            public int CashAcc { get; set; }


            [Display(Name = "Clearer: ")]
            public string Clearer { get; set; }

            [DisabledInput]
            [Display(Name = "Clearer: ")]
            public string ClearerSwiftCode { get; set; }


            [Required(ErrorMessage = "Amount is required")]
            [Display(Name = "Amount: ")]
            public double Amount { get; set; }

            [Required]
            [Range(0, int.MaxValue, ErrorMessage = "{0} is required")]
            [Display(Name = "CCY: ")]
            [SelectItems(nameof(CCY))]
            public int CCY { get; set; }

            [DataType(DataType.Date)]
            [Display(Name = "Trans Date: ")]
            public DateTime TransDate { get; set; }

            [DataType(DataType.Date)]
            [Display(Name = "Settle Date: ")]
            public DateTime SettleDate { get; set; }

            [Display(Name = "Deliv Agent: ")]
            public string DelivAgent { get; set; }
            
            [DisabledInput]
            [Display(Name = "Deliv Agent: ")]
            public string DelivAgentSwiftCode { get; set; }
            

            [Required]
            [Display(Name = "Del Agt Acc No: ")]
            public string DelAgtAccNo { get; set; }

            [Display(Name = "Place of Settle: ")]
            public string PlaceOfSettle { get; set; }

            [DisabledInput]
            [Display(Name = "Place of Settle: ")]
            public string PlaceOfSettleSwiftCode { get; set; }
            

            [Display(Name = "Send Swift")]
            public bool Checkbox1 { get; set; }
            [Display(Name = "Send To System")]
            public bool Checkbox2 { get; set; }

            [Required]
            [Display(Name = "Deliv Agent Swift Code: ")]
            public string sfiwtcode { get; set; }


            [Required]
            [Display(Name = "Place of Settle Swift Code: ")]
            public string sfiwtcodePlaceOfSettle { get; set; }

            [Required]
            [Display(Name = "Brkr Acc Swift Code: ")]
            public string sfiwtcodeBrkrAcc { get; set; }

            [Required]
            [Display(Name = "Clearer Swift Code: ")]
            public string sfiwtcodeClearer { get; set; }
            public int PaymentId { get; set; }


        }
    }
}
