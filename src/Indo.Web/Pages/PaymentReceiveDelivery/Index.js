﻿$(function () {
    /* Localization */
    var l = abp.localization.getResource('Indo');

    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes();
    var dateTime = date + '_' + time;

    /* load grid once */
    loadGrid();

    /* Syncfusion */
    ej.base.enableRipple(window.ripple);
    var grid = new ej.grids.Grid({
        enableInfiniteScrolling: false,
        allowPaging: true,
        pageSettings: { currentPage: 1, pageSize: 100, pageSizes: ["10", "20", "100", "200", "All"] },
        //allowGrouping: true,
        //groupSettings: { columns: ['p_Status'] },
        allowSorting: true,
        allowFiltering: true,
        filterSettings: { type: 'Excel' },
        allowSelection: true,
        selectionSettings: { persistSelection: true, type: 'Single' },
        enableHover: true,
        allowExcelExport: true,
        allowPdfExport: true,
        allowTextWrap: false,
        toolbar: [
            'ExcelExport', 'Search', { type: 'Separator' }, 'PdfExport',
            { type: 'Separator' },
            { text: 'Help', tooltipText: 'Help', id: 'HelpCustom' },
        ],
        columns: [
            { type: 'checkbox', width: 10 },
            {
                field: 'prD_ID', isPrimaryKey: true, headerText: 'ID', textAlign: 'right', width: 10
            },
            {
                field: 'prD_StatusID', headerText: 'Status Id', visible: false
            },
            {
                field: 'prD_MessageTypeID', headerText: 'Msg Type Id', textAlign: 'Left', width: 60
            },
            {
                field: 'prD_RecType', headerText: 'Rec Type', textAlign: 'center', width: 60
            },
            {
                field: 'prD_OrderRef', headerText: 'Order Ref', textAlign: 'center', width: 60
            },
            {
                field: 'prD_PortfolioCode', headerText: 'Portfolio Code', textAlign: 'center', width: 60
            },
            {
                field: 'prD_CCYCode', headerText: 'CCY Code', textAlign: 'center', width: 60
            },
            {
                field: 'prD_InstCode', headerText: 'Instrument Code', textAlign: 'center', width: 60
            },
            {
                field: 'prD_BrokerCode', headerText: 'Broker Codee', textAlign: 'Left', width: 60
            },
            {
                field: 'prD_CashAccountCode', headerText: 'Cash Acc Code', textAlign: 'Left', width: 60
            },
            {
                field: 'prD_ClearerCode', headerText: 'Clr Code', textAlign: 'Left', width: 80
            },
            {
                field: 'prD_TransDate', headerText: 'Tran Date', textAlign: 'center', width: 80, type: 'dateTime', format: 'dd-MM-yyyy', allowFiltering: false
            },
            {
                field: 'prD_SettleDate', headerText: 'Settle Date', textAlign: 'center', width: 80, type: 'dateTime', format: 'dd-MM-yyyy', allowFiltering: false
            },
            {
                field: 'prD_Amount', headerText: 'Amount', textAlign: 'right', width: 50, format: "N2"
            },
            {
                field: 'prD_Agent', headerText: 'Agent', textAlign: 'left', width: 130
            },
            {
                field: 'prD_AgentAccountNo', headerText: 'Agnt Acc No', textAlign: 'Left', width: 160
            },
            {
                field: 'prD_PlaceofSettlement', headerText: 'Place of Settlement', textAlign: 'Left', width: 180
            },
            {
                field: 'prD_Instructions', headerText: 'Instructions', textAlign: 'left', width: 100
            },
            {
                field: 'prD_SwiftReturned', headerText: 'Swift Rtd', textAlign: 'Left', width: 50
            },
            {
                field: 'prD_SwiftAcknowledged', headerText: 'Swift Ackn', textAlign: 'Left', width: 50
            },
            {
                field: 'prD_SwiftFile', headerText: 'Swift File', textAlign: 'Left', width: 60
            },
            {
                field: 'prD_SwiftStatus', headerText: 'Swift Status', textAlign: 'Left', width: 60
            },
            {
                field: 'prD_Username', headerText: 'Username', textAlign: 'Left', width: 70
            },
            {
                field: 'prD_AuthorisedUsername', headerText: 'Auth Username', textAlign: 'Left', width: 70
            },
            {
                field: 'prD_AuthorisedTime', headerText: 'Auth Time', textAlign: 'center', width: 100, type: 'dateTime', format: 'dd-MM-yyyy HH:mm', allowFiltering: false
            },
            {
                field: 'prD_CreatedTime', headerText: 'Created Time', textAlign: 'center', width: 100, type: 'dateTime', format: 'dd-MM-yyyy HH:mm', allowFiltering: false
            },
            {
                field: 'prD_ModifiedTime', headerText: 'Modified Time', textAlign: 'center', width: 100, type: 'dateTime', format: 'dd-MM-yyyy HH:mm', allowFiltering: false
            },
            {
                field: 'prD_SwiftSentTime', headerText: 'Swift Sent Time', textAlign: 'center', width: 100, type: 'dateTime', format: 'dd-MM-yyyy HH:mm', allowFiltering: false
            },
            {
                field: 'prD_SwiftReceivedTime', headerText: 'Swift Rcd Time Ackgd', textAlign: 'center', width: 100, type: 'dateTime', format: 'dd-MM-yyyy HH:mm', allowFiltering: false
            },
            {
                field: 'prD_ConfirmAcknowledged', headerText: 'Confirm Ackgd', textAlign: 'Left', width: 50
            },
            {
                field: 'prD_ConfirmReceivedTime', headerText: 'Confirm Time Ref', textAlign: 'center', width: 100, type: 'dateTime', format: 'dd-MM-yyyy HH:mm', allowFiltering: false
            },
            {
                field: 'prD_ConfirmSwiftFile', headerText: 'Confirm Swift File Time', textAlign: 'left', width: 50
            },
            {
                field: 'prD_StatusAcknowledged', headerText: 'Status Ackgd', textAlign: 'center', width: 50
            },
            {
                field: 'prD_StatusReceivedTime', headerText: 'Status Rec Time', textAlign: 'center', width: 100, type: 'dateTime', format: 'dd-MM-yyyy HH:mm', allowFiltering: false
            },
            {
                field: 'prD_StatusSwiftFile', headerText: 'Swift File Status', textAlign: 'left', width: 80
            },
            {
                field: 'prD_SwiftCancelSentTime', headerText: 'PSwift Cancel Time', textAlign: 'center', width: 20
            },
            {
                field: 'prD_SwiftCancelReceivedTime', headerText: 'Swift Cancel Received Time', textAlign: 'center', width: 100, type: 'dateTime', format: 'dd-MM-yyyy HH:mm', allowFiltering: false
            },
            {
                field: 'p_PaidReceivedOutPayFile', headerText: 'Paid Received OutPay File', textAlign: 'center', width: 100, type: 'dateTime', format: 'dd-MM-yyyy HH:mm', allowFiltering: false
            },
            {
                field: 'prD_SendIMS', headerText: 'Send IMS', textAlign: 'Left', width: 50
            },
            {
                field: 'prD_IMSAcknowledged', headerText: 'IMS Ackgd', textAlign: 'Left', width: 50
            },
            {
                field: 'prD_IMSSentTime', headerText: 'IMS Sent Time', textAlign: 'center', width: 100, type: 'dateTime', format: 'dd-MM-yyyy HH:mm', allowFiltering: false
            },
            {
                field: 'prD_IMSFile', headerText: 'IMS File', textAlign: 'Left', width: 150
            },
            {
                field: 'prD_IMSStatus', headerText: 'IMS Status', textAlign: 'Left', width: 60
            },
            {
                field: 'prD_SendMiniOMS', headerText: 'Send to System', textAlign: 'Left', width: 80
            },
            {
                field: 'prD_MiniOMSSentTime', headerText: 'Mini OMS Sent Time', textAlign: 'center', width: 100, type: 'dateTime', format: 'dd-MM-yyyy HH:mm', allowFiltering: false
            },
            {
                field: 'prD_Email', headerText: 'Email', textAlign: 'left', width: 60
            },
            {
                field: 'prD_FilePath', headerText: 'File Path', textAlign: 'left', width: 60
            },
            {
                field: 'prD_LinkedID', headerText: 'Linked Id', textAlign: 'left', width: 60
            }
        ],
        ////aggregates: [{
        ////    columns: [
        ////        {
        ////            type: 'Sum',
        ////            field: 'p_Amount',
        ////            format: 'N1',
        ////            groupFooterTemplate: 'Total Amount: ${Sum}'
        ////        },
        ////        {
        ////            type: 'Sum',
        ////            field: 'orderBalanceUnblockedIncPending',
        ////            format: 'N2',
        ////            groupFooterTemplate: 'Total Order Balance: ${Sum}'
        ////        }
        ////    ]
        ////}],
        rowHeight: 32,
        beforeDataBound: () => {
            grid.showSpinner();
        },
        dataBound: () => {
            grid.element.querySelector('.e-toolbar-left').style.left = grid.element.querySelector('.e-toolbar-right').getBoundingClientRect().width + 'px';
            grid.autoFitColumns();
            grid.hideSpinner();
        },
        excelExportComplete: () => {
            grid.hideSpinner();
        },
        pdfExportComplete: () => {
            grid.hideSpinner();
        },
        created: () => {
            var gridHeight = ($(".pcoded-main-container").height()) - ($(".e-gridheader").outerHeight()) - ($(".e-toolbar").outerHeight()) - ($(".e-gridpager").outerHeight());
            grid.height = gridHeight;
        },
        rowSelecting: () => {
            if (grid.getSelectedRecords().length) {
                grid.clearSelection();
            }
        },
        rowSelected: () => {
            if (grid.getSelectedRecords().length) {
                var id = grid.getSelectedRecords()[0].prD_ID;
                window.location.href = '/BackOfficeFOPForm?id=' + id;
            }
        },
        toolbarClick: (args) => {
            if (args.item.id === 'Grid_excelexport') {
                var exportProperties = {
                    fileName: "PaymentsReceiveDeliver_" + dateTime + ".xlsx"
                };
                grid.showSpinner();
                grid.excelExport(exportProperties);
            }
            if (args.item.id === 'Grid_pdfexport') {
                var exportProperties = {
                    pageOrientation: 'Landscape',
                    pageSize: 'A4',
                    header: {
                        fromTop: 0,
                        height: 50,
                        contents: [
                            {
                                type: 'PageNumber',
                                pageNumberType: 'Arabic',
                                format: 'Page {$current} of {$total}',
                                position: { x: 0, y: 25 },
                                style: { textBrushColor: '#0008ff', fontSize: 12, hAlign: 'Center' }
                            }
                        ]
                    },
                    theme: {
                        header: { fontColor: '#000000', fontName: 'Calibri', fontSize: 8, bold: true, borders: { color: '#000000', lineStyle: 'Thin' } },
                        caption: { fontColor: '#0e1230', fontName: 'Calibri', fontSize: 9, bold: false },
                        record: { fontColor: '#1E377F', fontName: 'Calibri', fontSize: 8, bold: false }
                    },
                    fileName: "PaymentsReceiveDeliver" + dateTime + ".pdf"
                };
                grid.showSpinner();
                grid.pdfExport(exportProperties);
            }
            if (args.item.id === 'HelpCustom') {
                helpModal.open();
            }
        },
    });

    function loadGrid() {
        indo.paymentReceiveDeliveries.paymentReceiveDelivery.getList()
            .then(function (data) {
                grid.dataSource = data;
                grid.appendTo('#Grid');
            });
    }

    /* Popup Modal */
    var helpModal = new abp.ModalManager(abp.appPath + 'PaymentReceiveDelivery/Help');
});