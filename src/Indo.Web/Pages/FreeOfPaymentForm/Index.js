﻿$(function () {
    /* Localization */
    var l = abp.localization.getResource('Indo');
    var maxLimitArray = [];
    var typeElement;
    var typeValue;

    var isinElement;
    var isinValue;

    var CCYElem;
    var CCYValue;
    var type;
    var isin;
    var CCY;
    var globalPath = "/";
    indo.freeOfPayments.freeOfPayment.getTypeList()
        .then(function (data) {
            type = data;
            console.log(type)
        });
    indo.freeOfPayments.freeOfPayment.getISINList()
        .then(function (data) {
            isin = data;
            console.log(isin)
        });
    indo.freeOfPayments.freeOfPayment.getCCYList()
        .then(function (data) {
            CCY = data;
            console.log(CCY)
        });
    var globalFile;
    var globalFolderName = 'FreeOfPaymentForm';

    ej.grids.Grid.Inject(ej.grids.Edit, ej.grids.Toolbar);
    var grid = new ej.grids.Grid({
        toolbar: ['Add', 'Edit', 'Update', 'Cancel', 'Delete'],
        editSettings: { allowEditing: true, allowAdding: true, allowDeleting: true },
        isResponsive: true,
        enableResponsiveRow: true,
        columns: [
            {
                field: 'Type', headerText: 'Type', width: 60, editType: 'dropDownEdit', validationRules: { required: true },
                edit: {
                    create: function () {
                        typeElement = document.createElement('input');
                        return typeElement;
                    },
                    read: function () {
                        return typeValue.text;
                    },
                    destroy: function () {
                        typeValue.destroy();
                    },
                    write: function () {
                        typeValue = new ej.dropdowns.DropDownList({
                            dataSource: type,
                            fields: { value: 'typeID', text: 'typeName' },
                            placeholder: 'Select a type',
                            floatLabelType: 'Never'
                        });
                        typeValue.appendTo(typeElement);
                    }
                }
            },
            {
                field: 'TransDate', headerText: 'Trans Date', width: 40, allowEditing: true, type: "date", editType: 'datepickeredit', format: 'dd-MMM-yyyy', edit: { params: { format: 'dd-MMM-yyyy' } }
            },
            {
                field: 'SettleDate', headerText: 'Settle Date', width: 40, allowEditing: true, editType: 'datepickeredit', type: "date", format: 'dd-MMM-yyyy', edit: { params: { format: 'dd-MMM-yyyy' } }
            },
            {
                field: 'ISIN', headerText: 'ISIN', width: 40, editType: 'dropDownEdit',
                edit: {
                    create: function () {
                        isinElement = document.createElement('input');
                        return isinElement;
                    },
                    read: function () {
                        return isinValue.text;
                    },
                    destroy: function () {
                        isinValue.destroy();
                    },
                    write: function () {
                        isinValue = new ej.dropdowns.DropDownList({
                            dataSource: isin,
                            fields: { value: 'isinid', text: 'isinName' },
                            placeholder: 'Select a ISIN',
                            floatLabelType: 'Never'
                        });
                        isinValue.appendTo(isinElement);
                    }
                }
            },
            {
                field: 'Amount', headerText: 'Amount', width: 50, allowEditing: true, textAlign: 'Right', type: 'Number', validationRules: { min: 0 }, editType: 'numericedit', format: 'N2'
            },
            {
                field: 'CCY', headerText: 'CCY', width: 60, validationRules: { required: true }, editType: 'dropDownEdit',
                edit: {
                    create: function () {
                        CCYElem = document.createElement('input');
                        return CCYElem;
                    },
                    read: function () {
                        return CCYValue.text;
                    },
                    destroy: function () {
                        CCYValue.destroy();
                    },
                    write: function () {
                        CCYValue = new ej.dropdowns.DropDownList({
                            dataSource: CCY,
                            fields: { value: 'ccyid', text: 'ccyName' },
                            placeholder: 'Select a CCY',
                            floatLabelCCY: 'Never'
                        });
                        CCYValue.appendTo(CCYElem);
                    }
                }
            },
        ],
    });

    $("#Portfolio").change(function () {
        manageDirectory();
    });

    function manageDirectory() {
        var portfolioText = $("#Portfolio option:selected").text();
        var newFolderName = portfolioText;
        globalPath = "/" + newFolderName + "/";
        globalFile.createFolder(portfolioText)
        globalFile.refresh();
    }


    $("select").removeClass("custom-select");

    loadGrid()
    function loadGrid() {
        indo.freeOfPayments.freeOfPayment.getFOPRequestList()
            .then(function (data) {
                grid.dataSource = data;
                grid.appendTo('#Grid');
            });
        $("#gridCard").show();
    }

    onMinMaxChargeChange();

    $("#ViewModel_MgtFeesMaxCharge").on('blur', function () { onMinMaxChargeChange(); });

    function onMinMaxChargeChange() {
        $("#tblBodyDynamicTableRowsForFees").empty();
        var minCharge = $("#ViewModel_MgtFeesMinCharge").val();
        var maxCharge = $("#ViewModel_MgtFeesMaxCharge").val();
        if (minCharge && !isNaN(minCharge) && !isNaN(parseFloat(minCharge)) && maxCharge && !isNaN(maxCharge) && !isNaN(parseFloat(maxCharge)) && (minCharge < maxCharge)) {
            maxLimitArray = [];
            if (maxLimitArray.indexOf(maxCharge) === -1) {
                maxLimitArray.push(maxCharge);
            }
            generateDynamicRowsForFees(minCharge, maxCharge);
        }
        else {
            $("#tblBodyDynamicTableRowsForFees").html('<tr><td colspan="3">Min and max charge is not valid</td></tr>');
        }
    }

    /* File manager Second Tab Start*/

    var fileManagerFirstOpen = true;
    loadFileManager();
    setTimeout(function () {
        refreshFile();
    }, 1000)
    
    function loadFileManager() {
        var hostUrl = '/';
        var fileObject = new ej.filemanager.FileManager({
            ajaxSettings: {
                url: hostUrl + 'api/FileManagerFees/FileOperations',
                getImageUrl: hostUrl + 'api/FileManagerFees/GetImage',
                uploadUrl: hostUrl + 'api/FileManagerFees/Upload',
                downloadUrl: hostUrl + 'api/FileManagerFees/Download?folder=' + globalFolderName
            },
            toolbarSettings: { items: ['Upload', 'Delete', 'Refresh'], visible: true },
            beforeSend: (beforeSendEvent) => {
                var request = JSON.parse(beforeSendEvent.ajaxSettings.data);
                if (request.action == "read") {
                    request.path = globalPath
                    request.data = []
                    var updatedRequest = JSON.stringify(request);
                    beforeSendEvent.ajaxSettings.data = updatedRequest
                }
                if (beforeSendEvent.action == "Upload") {
                    request[0].path = globalPath
                    var updatedRequest = JSON.stringify(request);
                    beforeSendEvent.ajaxSettings.data = updatedRequest
                }
                if (request.action == "delete") {
                    request.path = globalPath
                    var updatedRequest = JSON.stringify(request);
                    beforeSendEvent.ajaxSettings.data = updatedRequest
                }
                beforeSendEvent.ajaxSettings.beforeSend = function (beforeSendEvent) {
                    beforeSendEvent.httpRequest.setRequestHeader('folder', globalFolderName);
                }
            },
            beforeImageLoad: (beforeImageLoadEvent) => {
                beforeImageLoadEvent.imageUrl = beforeImageLoadEvent.imageUrl + '&folder=' + globalFolderName;
            },
            created: () => {
                var fileManagerHeight = ($(".pcoded-main-container").height()) - 5;
                fileObject.height = fileManagerHeight;
            },
            view: 'Details',
            failure: (event) => {
                if (event.action == 'create' && event.action == 'delete'  && event.error.code == '400') {
                    globalFile.refresh();                    
                }
            }
        });
        globalFile = fileObject;
        fileObject.appendTo('#filemanager');
    }
    function refreshFile() {
        try {
            var folderNames = [];
            globalFile.selectAll();
            var allDir = globalFile.getSelectedFiles();
            for (var dir = 0; dir < allDir.length; dir++) {
                folderNames.push(allDir[dir].name);
            }
            globalFile.deleteFiles(folderNames);
            globalFile.refresh();
        } catch (e) {
        }        
    }
    $("form").submit(function (e) {
        $("#ViewModel_FOPFilePath").val("wwwroot/FeesFiles/FreeOfPaymentForm" + globalPath);
        var currentViewRecords = grid.dataSource;        
        currentViewRecords.forEach(function (result) {
            if (typeof result.Type === 'string') {
                var fopGridType = type.find(x => x.typeName === result.Type)
                result.Type = fopGridType.typeID;
            }
            if (typeof result.ISIN === 'string') {
                var fopGridIsin = isin.find(x => x.isinName === result.ISIN)
                result.ISIN = fopGridIsin.isinid;
            }
            if (typeof result.CCY === 'string') {
                var fopGridCCY = CCY.find(x => x.ccyName === result.CCY)
                result.CCY = fopGridCCY.ccyid;
            }
        });
        $("#ViewModel_GridList").val(JSON.stringify(currentViewRecords));        
    });
});
