using Indo.FreeOfPayments;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;
using Newtonsoft.Json;
using Castle.Core.Internal;
using IdentityServer4.Extensions;
using System.Linq;
using System.Threading.Tasks;

namespace Indo.Web.Pages.FreeOfPaymentForm
{

    public class IndexModel : IndoPageModel
    {
        private readonly IFreeOfPaymentAppService _freeOfPaymentAppService;
        public IndexModel(IFreeOfPaymentAppService freeOfPaymentAppService)
        {
            _freeOfPaymentAppService = freeOfPaymentAppService;
        }

        [BindProperty]
        public FreeOfPaymentViewModel ViewModel { get; set; }

        public List<SelectListItem> PortfolioList { get; set; } 
        public List<SelectListItem> PayTypeList { get; set; } 

        public async Task OnGetAsync()
        {
            var portfolioList = await _freeOfPaymentAppService.FetchSourcePortfolios("");
            PortfolioList = portfolioList.Select(x => new SelectListItem() { Text = x.SP_Name, Value = x.SP_ID.ToString() }).OrderBy(x => x.Text).ToList();
            PortfolioList.Add(new SelectListItem { Value = "-1", Text = "Select" });

            var payType = await _freeOfPaymentAppService.FetchPaymentTypes("");
            PayTypeList = payType.Select(x => new SelectListItem() { Text = x.PT_PaymentType, Value = x.PT_ID.ToString() }).OrderBy(x => x.Text).ToList();
            PayTypeList.Add(new SelectListItem { Value = "-1", Text = "Select" });
        }

        public IActionResult OnPost()
        {
            try
            {
                var formData = Request.Form;
                if (formData != null && formData.ContainsKey("ViewModel.GridList"))
                {
                    var gridList = formData["ViewModel.GridList"];
                    if (gridList.Count > 0 && gridList[0] != "[]")
                    {
                        var gridData = JsonConvert.DeserializeObject<List<FOPRequestReadDto>>(gridList);
                        if (gridData != null && gridData.Count() > 0)
                        {
                            ViewModel.GridList = gridData;
                        }
                    }
                }

                FOP fop = new FOP()
                {
                    PRD_Email = ViewModel.EmailTextArea,
                    PRD_ID = ViewModel.Id,
                    PRD_PortfolioCode = ViewModel.Portfolio,
                    PRD_RecType = ViewModel.PayType,
                    PRD_FilePath = ViewModel.FOPFilePath,
                };

                var data = _freeOfPaymentAppService.InsertFOPPayment(fop).Result;
                return NoContent();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException($"{ex.Message}");
            }
        }

        public class FreeOfPaymentViewModel
        {
            public int Id { get; set; }

            [SelectItems(nameof(PayType))]
            [Display(Name = "Pay Type: ")]
            public int PayType { get; set; }

            [SelectItems(nameof(Portfolio))]
            [Display(Name = "Portfolio: ")]
            public int Portfolio { get; set; }

            public string EmailTextArea { get; set; }
            public string FOPFilePath { get; set; }
            public List<FOPRequestReadDto> GridList { get; set; }

        }
    }
}
