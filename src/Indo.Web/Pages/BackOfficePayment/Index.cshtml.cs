using Indo.BackOfficePayment;
using Indo.Payments;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;

namespace Indo.Web.Pages.BackOfficePayment
{
    public class IndexModel : IndoPageModel
    {
        private readonly IBackOfficePaymentAppService _backOfficePaymentAppService;
        public IndexModel(IBackOfficePaymentAppService backOfficePaymentAppService)
        {
            _backOfficePaymentAppService = backOfficePaymentAppService;
        }
        [BindProperty]
        public BackOfficePaymentViewModel ViewModel { get; set; }
        public List<SelectListItem> MoveTypeList { get; set; }
        public List<SelectListItem> PortfolioList { get; set; }
        public List<SelectListItem> PaymentOthersList { get; set; }
        public List<SelectListItem> CCYList { get; set; }
        public List<SelectListItem> AccountList { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> CountryList { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> TemplateList { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> TypeList { get; set; }
        public List<SelectListItem> RelationshipList { get; set; }
        public List<SelectListItem> IntermediaryBankList { get; set; } = new List<SelectListItem>();

        public void OnGetAsync()
        {
            ViewModel = new BackOfficePaymentViewModel
            {
                TransDate = DateTime.Now,
                SettleDate = DateTime.Now
            };

            var portfolioType = _backOfficePaymentAppService.FetchPortfolio("").Result;
            PortfolioList = portfolioType.Select(x => new SelectListItem() { Text = x.SP_Name, Value = x.SP_ID.ToString() }).OrderBy(x => x.Text).ToList();
            PortfolioList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });

            var payType = _backOfficePaymentAppService.FetchMoveTypes("").Result;
            MoveTypeList = payType.Select(x => new SelectListItem() { Text = x.PT_PaymentType, Value = x.PT_ID.ToString() }).OrderBy(x => x.Text).ToList();
            MoveTypeList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });

            var Ccy = _backOfficePaymentAppService.FetchCurrencies("").Result;
            CCYList = Ccy.Select(x => new SelectListItem() { Text = x.Shortcut, Value = x.Id.ToString() }).OrderBy(x => x.Text).ToList();
            CCYList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });

            var countries = _backOfficePaymentAppService.FetchPaymentCountries("").Result;
            CountryList = countries.Select(x => new SelectListItem() { Text = x.Ctry_Name, Value = x.Ctry_ID.ToString() }).OrderBy(x => x.Text).ToList();
            CountryList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });

            var benTypeList = _backOfficePaymentAppService.FetchBenficiaries().Result;
            TypeList = benTypeList.Select(x => new SelectListItem() { Text = x.BeneficiaryType, Value = x.BeneficiaryTypeID.ToString() }).OrderBy(x => x.Text).ToList();
            TypeList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });

            var relationshipList = _backOfficePaymentAppService.FetchPaymentRARangeTypes("").Result;
            RelationshipList = relationshipList.Select(x => new SelectListItem() { Text = x.PTy_Description, Value = x.PTy_ID.ToString() }).OrderBy(x => x.Text).ToList();
            RelationshipList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });

            var paymentOther = _backOfficePaymentAppService.FetchPaymentOthers("").Result;
            PaymentOthersList = paymentOther.Select(x => new SelectListItem() { Text = x.OtH_Type, Value = x.OTH_ID.ToString() }).OrderBy(x => x.Text).ToList();
            PaymentOthersList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });

            //var templateList = _backOfficePaymentAppService.FetchTemplates().Result;
            //templateList.RemoveAll(x => x.pa_TemplateName.Equals("") || x.pa_TemplateName.Equals("-1") || x.pa_TemplateName.Length < 5);
            //TemplateList = templateList.Select(x => new SelectListItem() { Text = x.pa_TemplateName, Value = x.pa_ID.ToString() }).OrderBy(x => x.Text).ToList();
            //TemplateList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });
        }

        public IActionResult OnPost()
        {
            try
            {
                Payment Payment = new Payment()
                {
                    P_ID = ViewModel.PaymentId,
                    P_PaymentType = ViewModel.MoveTypeVal,
                    P_PaymentTypeID = ViewModel.MoveType,
                    P_MessageTypeID = ViewModel.MessageId.HasValue && ViewModel.MessageId > 0 ? ViewModel.MessageId.Value : 0,
                    P_PortfolioID = ViewModel.Portfolio.HasValue && ViewModel.Portfolio > 0 ? ViewModel.Portfolio.Value : (ViewModel.Order_Portfolio.HasValue && ViewModel.Order_Portfolio > 0 ? ViewModel.Order_Portfolio.Value : 0),
                    P_Portfolio = !string.IsNullOrEmpty(ViewModel.Portfolioval) && ViewModel.Portfolioval != "Select" ? ViewModel.Portfolioval : string.Empty,
                    P_TransDate = ViewModel.TransDate,
                    P_SettleDate = ViewModel.SettleDate,
                    P_Amount = ViewModel.Amount,
                    P_CCYID = ViewModel.CCY != 0 ? ViewModel.CCY : ViewModel.HiddenCCY,
                    P_CCY = ViewModel.CCYVal,
                    P_OrderRef = !string.IsNullOrEmpty(ViewModel.Order_Ref) ? ViewModel.Order_Ref : string.Empty,
                    P_OrderBrokerID = 0,
                    P_OrderAccountID = ViewModel.Order_Account.HasValue && ViewModel.Order_Account > 0 ? ViewModel.Order_Account.Value : 0,
                    P_OrderAccount = !string.IsNullOrEmpty(ViewModel.Order_AccountVal) && ViewModel.Order_AccountVal != "Select" ? ViewModel.Order_AccountVal : string.Empty,
                    P_OrderName = !string.IsNullOrEmpty(ViewModel.Order_Name) ? ViewModel.Order_Name : string.Empty,
                    P_OrderAddress1 = !string.IsNullOrEmpty(ViewModel.Order_Address1) ? ViewModel.Order_Address1 : string.Empty,
                    P_OrderAddress2 = !string.IsNullOrEmpty(ViewModel.Order_City) ? ViewModel.Order_City : string.Empty,
                    P_OrderZip = !string.IsNullOrEmpty(ViewModel.Order_PostZip) ? ViewModel.Order_PostZip : string.Empty,
                    P_OrderCountryID = ViewModel.Order_Country.HasValue && ViewModel.Order_Country > 0 ? ViewModel.Order_Country.Value : 0,
                    P_OrderSwIFt = !string.IsNullOrEmpty(ViewModel.Order_Swift) ? ViewModel.Order_Swift : string.Empty,
                    P_OrderIBAN = !string.IsNullOrEmpty(ViewModel.Order_IBAN) ? ViewModel.Order_IBAN : string.Empty,
                    //P_OrderOther = !string.IsNullOrEmpty(ViewModel.Order_Other) ? ViewModel.Order_Other : string.Empty,
                    P_OrderVO_ID = 0,
                    P_BenRef = ViewModel.BenMain_Order_Ref,
                    P_BenBrokerID = 0,
                    P_BenAccountID = ViewModel.BenMain_Account.HasValue && ViewModel.BenMain_Account > 0 ? ViewModel.BenMain_Account.Value : 0,
                    P_BenAccount = !string.IsNullOrEmpty(ViewModel.BenMain_AccountVal) && ViewModel.BenMain_AccountVal != "Select" ? ViewModel.BenMain_AccountVal : string.Empty,
                    P_BenAddressID = 0,
                    P_BenTypeID = ViewModel.BenMain_Type.HasValue && ViewModel.BenMain_Type > 0 ? ViewModel.BenMain_Type.Value : 0,
                    P_BenName = !string.IsNullOrEmpty(ViewModel.BenMain_Name) ? ViewModel.BenMain_Name : (!string.IsNullOrEmpty(ViewModel.BenMain_BusinessName) ? ViewModel.BenMain_BusinessName : string.Empty),
                    P_BenFirstname = !string.IsNullOrEmpty(ViewModel.BenMain_FirstName) ? ViewModel.BenMain_FirstName : string.Empty,
                    P_BenMiddlename = !string.IsNullOrEmpty(ViewModel.BenMain_MiddleName) ? ViewModel.BenMain_MiddleName : string.Empty,
                    P_BenSurname = !string.IsNullOrEmpty(ViewModel.BenMain_Surname) ? ViewModel.BenMain_Surname : string.Empty,
                    P_BenAddress1 = !string.IsNullOrEmpty(ViewModel.BenMain_Address) ? ViewModel.BenMain_Address : string.Empty,
                    P_BenAddress2 = !string.IsNullOrEmpty(ViewModel.BenMain_City) ? ViewModel.BenMain_City : string.Empty,
                    P_BenZip = !string.IsNullOrEmpty(ViewModel.BenMain_PostZip) ? ViewModel.BenMain_PostZip : string.Empty,
                    P_BenCounty = !string.IsNullOrEmpty(ViewModel.BenMain_County) ? ViewModel.BenMain_County : string.Empty,
                    P_BenCountryID = ViewModel.BenMain_Country.HasValue && ViewModel.BenMain_Country > 0 ? ViewModel.BenMain_Country.Value : 0,
                    P_BenSwIFt = !string.IsNullOrEmpty(ViewModel.BenDetailSendSWIFT_Swift57A) ? ViewModel.BenDetailSendSWIFT_Swift57A : string.Empty,
                    P_BenIBAN = !string.IsNullOrEmpty(ViewModel.BenDetailSendSWIFT_IBAN59) ? ViewModel.BenDetailSendSWIFT_IBAN59 : string.Empty,
                    P_BenOther = !string.IsNullOrEmpty(ViewModel.BenDetailSendSWIFT_Other) ? ViewModel.BenDetailSendSWIFT_Other : string.Empty,
                    P_BenBIK = "",
                    P_BenINN = "",
                    P_BenRelationshipID = ViewModel.BenMain_Relationship.HasValue && ViewModel.BenMain_Relationship > 0 ? ViewModel.BenMain_Relationship.Value : 0,
                    P_BenPortfolioID = ViewModel.BenMain_Portfolio.HasValue && ViewModel.BenMain_Portfolio > 0 ? ViewModel.BenMain_Portfolio.Value : 0,
                    P_BenPortfolio = !string.IsNullOrEmpty(ViewModel.BenMain_PortfolioVal) && ViewModel.BenMain_PortfolioVal != "Select" ? ViewModel.BenMain_PortfolioVal : string.Empty,
                    P_BenCCYID = ViewModel.CCY,
                    P_BenCCY = ViewModel.BenCCY,
                    P_ExchangeRate = 1,
                    //P_BenTemplateSelected = ViewModel.BenMain_Template.HasValue && ViewModel.BenMain_Template > 0 ? ViewModel.BenMain_Template.Value : 0,
                    P_BenTemplateName= string.Empty,
                    P_BenSubBankAddressID = 0,
                    P_BenSubBankName = !string.IsNullOrEmpty(ViewModel.BenDetail_IntermediaryBankSwiftCode) ? ViewModel.BenDetail_IntermediaryBankSwiftCode : string.Empty,
                    P_BenSubBankAddress1 = !string.IsNullOrEmpty(ViewModel.BenDetail_Address) ? ViewModel.BenDetail_Address : string.Empty,
                    P_BenSubBankAddress2 = !string.IsNullOrEmpty(ViewModel.BenDetail_City) ? ViewModel.BenDetail_City : string.Empty,
                    P_BenSubBankZip = !string.IsNullOrEmpty(ViewModel.BenDetail_Zip) ? ViewModel.BenDetail_Zip : string.Empty,
                    P_BenSubBankSwIFt = !string.IsNullOrEmpty(ViewModel.BenDetail_Swift56A) ? ViewModel.BenDetail_Swift56A : string.Empty,
                    P_BenSubBankIBAN = !string.IsNullOrEmpty(ViewModel.BenDetail_IBAN57A) ? ViewModel.BenDetail_IBAN57A : string.Empty,
                    P_BenSubBankOther = !string.IsNullOrEmpty(ViewModel.BenDetail_Other) ? ViewModel.BenDetail_Other : string.Empty,
                    P_BenSubBankBIK = "",
                    P_BenSubBankINN = "",
                    P_BenBankAddressID = 0,
                    P_BenBankName = !string.IsNullOrEmpty(ViewModel.BenDetailAcc_AccountBankNameSwiftCode) ? ViewModel.BenDetailAcc_AccountBankNameSwiftCode : string.Empty,                    
                    P_BenBankAddress1 = !string.IsNullOrEmpty(ViewModel.BenDetailAcc_Address) ? ViewModel.BenDetailAcc_Address : string.Empty,
                    P_BenBankAddress2 = !string.IsNullOrEmpty(ViewModel.BenDetailAcc_City) ? ViewModel.BenDetailAcc_City : string.Empty,
                    P_BenBankZip = !string.IsNullOrEmpty(ViewModel.BenDetailAcc_Zip) ? ViewModel.BenDetailAcc_Zip : string.Empty,
                    P_BenBankSwIFt = !string.IsNullOrEmpty(ViewModel.BenDetailAcc_Swift56A) ? ViewModel.BenDetailAcc_Swift56A : string.Empty,                    
                    P_BenBankIBAN = !string.IsNullOrEmpty(ViewModel.BenDetailAcc_IBAN57A) ? ViewModel.BenDetailAcc_IBAN57A : string.Empty,
                    P_BenBankOther = !string.IsNullOrEmpty(ViewModel.BenDetailAcc_Other) ? ViewModel.BenDetailAcc_Other : string.Empty,                    
                    P_BenBankBIK = "",
                    P_BenBankINN = "",
                    P_Comments = !string.IsNullOrEmpty(ViewModel.Comments) ? ViewModel.Comments : string.Empty,
                    P_SendSwIFt = ViewModel.Checkbox1,
                    P_SendIMS = ViewModel.Checkbox2,
                    P_SendSwIFt2 = false,
                    P_SendIMS2 = false,
                    P_SwIFtUrgent = true,
                    P_SwIFtSendRef = true,
                    P_Other_ID = ViewModel.PaymentOthers.HasValue && ViewModel.PaymentOthers > 0 ? ViewModel.PaymentOthers.Value : 0,
                    P_PortfolioFee = 0,
                    P_PortfolioFeeCCY = "",
                    P_PortfolioFeeINT = 0,
                    P_PortfolioFeeCCYINT = "",
                    P_PortfolioFeeAccountID = 0,
                    P_PortfolioFeeRMSAccountID = 0,
                    P_PortfolioIsFee = false,
                    P_PortfolioIsCommission = false,
                    P_PortfolioFeeId = 0,
                    P_PaymentFilePath = ViewModel.BackOfficePaymentFilePath,
                    P_Username = $"{CurrentUser.Name} {CurrentUser.SurName}",
                    P_AuthorisedUsername = "",
                    P_SourceSystem = "",
                    P_AboveThreshold = false,
                    //P_IMSRef = "",
                    P_SentApproveEmailcompliance = 0,
                    PR_PaymentRequested = false,
                    PR_PaymentRequestedType = 0,
                    PR_PaymentRequestedRate = 0,
                    PR_PaymentRequestedFrom = "",
                    PR_PaymentRequestedEmail = "",
                    PR_PaymentRequestedBeneficiaryName = "",
                    PR_CheckedSantions = true,
                    PR_CheckedPEP = true,
                    PR_CheckedCDD = true,
                    PR_IssuesDisclosed = "",
                    PR_CheckedBankName = "",
                    PR_CheckedBankCountryID = 0,
                    PR_CheckedBankCountry = "",
                    PR_MM_BS = 0,
                    PR_MM_InstrCode = 0,
                    PR_MM_DurationType = 0,
                    PR_MM_StartDate = ViewModel.TransDate,
                    PR_MM_EndDate = ViewModel.TransDate,
                    PR_MM_FirmMove = true,
                    P_OrderOther = !string.IsNullOrEmpty(ViewModel.sendTo) ? ViewModel.sendTo : string.Empty,
                    P_IMSRef = !string.IsNullOrEmpty(ViewModel.relatedRef) ? ViewModel.relatedRef : string.Empty,
                    P_BenSubBankCountryID = ViewModel.BenInter_Country.HasValue && ViewModel.BenInter_Country > 0 ? ViewModel.BenInter_Country.Value : 0,
                    P_BenBankCountryID = ViewModel.BenAcc_Country.HasValue && ViewModel.BenAcc_Country > 0 ? ViewModel.BenAcc_Country.Value : 0
                };
                string data = null;

                if (ViewModel.PaymentId != 0)
                {
                    data = _backOfficePaymentAppService.UpdatePayment(Payment).Result;
                }
                else
                {
                    data = _backOfficePaymentAppService.InsertPayment(Payment).Result;
                }


                if (data != null && ViewModel.PaymentId == 0)
                {
                    TempData["StatusMessage"] = "Form Submitted Succesfully with id " + data;
                    return RedirectToPage();
                }
                else if (data != null && ViewModel.PaymentId != 0)
                {
                    TempData["StatusMessage"] = "Form Updated Succesfully";
                    return RedirectToPage();
                }
                else
                {
                    throw new UserFriendlyException($"{data}");
                }
            }
            catch (Exception ex)
            {
                TempData["StatusMessage"] = $"{ex.Message}";
                return RedirectToPage();
            }
        }

        public class BackOfficePaymentViewModel
        {
            [Required]
            [Range(0, int.MaxValue, ErrorMessage = "{0} is required")]
            [SelectItems(nameof(MoveType))]
            [Display(Name = "Move Type: ")]
            public int MoveType { get; set; }
            public string MoveTypeVal { get; set; }
            

            [SelectItems(nameof(Portfolio))]
            [Display(Name = "Portfolio: ")]
            public int? Portfolio { get; set; }
            public string Portfolioval { get; set; }


            [SelectItems(nameof(PaymentOthers))]
            [Display(Name = "Fee Type: ")]
            public int? PaymentOthers { get; set; }

            [Required]
            [Range(0, int.MaxValue, ErrorMessage = "{0} is required")]
            [SelectItems(nameof(CCY))]
            [Display(Name = "CCY: ")]
            public int CCY { get; set; }
            public int HiddenCCY { get; set; }
            
            public string CCYVal { get; set; }

            [Display(Name = "CCY: ")]
            public string BenCCY { get; set; }

            [Required(ErrorMessage = "Amount is required")]
            [Display(Name = "Amount: ")]
            public double Amount { get; set; }

            [Display(Name = "Rate: ")]
            public float? rate { get; set; }

            [DataType(DataType.Date)]
            [Display(Name = "Trans Date: ")]
            public DateTime TransDate { get; set; }

            [DataType(DataType.Date)]
            [Display(Name = "Settle Date: ")]
            public DateTime SettleDate { get; set; }

            [TextArea]
            [Display(Name = "Message: ")]
            public string Comments { get; set; }


            [Display(Name = "Portfolio: ")]
            public int? Order_Portfolio { get; set; }

            [Display(Name = "Portfolio: ")]
            public string Order_PaymentOthers { get; set; }

            [Display(Name = "Account: ")]
            public int? Order_Account { get; set; }
            public string Order_AccountVal { get; set; }

            [Display(Name = "Balance: ")]
            public float Order_Balance { get; set; }

            [Display(Name = "Name: ")]
            public string Order_Name { get; set; }

            [Display(Name = "Ref: ")]
            public string Order_Ref { get; set; }

            [Display(Name = "Address 1: ")]
            public string Order_Address1 { get; set; }

            [Display(Name = "Swift (52A): ")]
            public string Order_Swift { get; set; }

            [Display(Name = "City: ")]
            public string Order_City { get; set; }

            
            [Display(Name = "IBAN (50K): ")]
            public string Order_IBAN { get; set; }

            [Display(Name = "Post/Zip: ")]
            public string Order_PostZip { get; set; }

            [Display(Name = "Other (72): ")]
            public string Order_Other { get; set; }

            
            [Display(Name = "Country: ")]
            public int? Order_Country { get; set; }

            [SelectItems(nameof(BenMain_Portfolio))]
           
            [Display(Name = "Portfolio: ")]
            public int? BenMain_Portfolio { get; set; }
            public string BenMain_PortfolioVal { get; set; }


            [Display(Name = "Account: ")]
            public int? BenMain_Account { get; set; }
            public string BenMain_AccountVal { get; set; }

            [Display(Name = "Template: ")]
            public int? BenMain_Template { get; set; }


            [Display(Name = "Name: ")]
            public string BenMain_Name { get; set; }

            [Display(Name = "Balance: ")]
            public float BenMain_Order_Balance { get; set; }

            [Display(Name = "Ref: ")]
            public string BenMain_Order_Ref { get; set; }

            [Display(Name = "Type: ")]
            public int? BenMain_Type { get; set; }

            [SelectItems(nameof(BenMain_Relationship))]
            [Display(Name = "Relationship: ")]
            public int? BenMain_Relationship { get; set; }

            [Display(Name = "First Name: ")]
            public string BenMain_FirstName { get; set; }

            [Display(Name = "Middle Name: ")]
            public string BenMain_MiddleName { get; set; }

            [Display(Name = "Surname: ")]
            public string BenMain_Surname { get; set; }

            [Display(Name = "Address: ")]
            public string BenMain_Address { get; set; }

            [Display(Name = "City: ")]
            public string BenMain_City { get; set; }

            [Display(Name = "County: ")]
            public string BenMain_County { get; set; }

            [Display(Name = "Post/Zip: ")]
            public string BenMain_PostZip { get; set; }

            [Display(Name = "Country: ")]
            public int? BenMain_Country { get; set; }
            public float BenMain_PendingOrder_Balance { get; set; }
            public float BenMain_PostOrder_Balance { get; set; }

            [Display(Name = "Tick to add intermediary bank")]
            public bool BenDetail_IntermediaryBankCheckbox { get; set; }

            [Display(Name = "")]
            public string BenDetail_IntermediaryBank { get; set; }

            [Display(Name = "Swift Code")]
            public string sfiwtcodeIntermediaryBank { get; set; }

            [Display(Name = "")]
            public string BenDetail_IntermediaryBankSwiftCode { get; set; }

            [Display(Name = "Address: ")]
            public string BenDetail_Address { get; set; }

            [Display(Name = "Swift(56A): ")]
            public string BenDetail_Swift56A { get; set; }

            [Display(Name = "City: ")]
            public string BenDetail_City { get; set; }

            
            [Display(Name = "IBAN(57A): ")]
            public string BenDetail_IBAN57A { get; set; }

            [Display(Name = "ZIP: ")]
            public string BenDetail_Zip { get; set; }

            [Display(Name = "")]
            public string BenDetail_ZipDropDown { get; set; }

            [Display(Name = "Other: ")]
            public string BenDetail_Other { get; set; }

            [Display(Name = "Account Bank Name: ")]
            public string BenDetailAcc_AccountBankName { get; set; }

            [Display(Name = "")]
            public string BenDetailAcc_AccountBankNameSwiftCode { get; set; }
            [Display(Name = "Swift Code:")]
            public string BenDetailAcc_AccountBankNameSwiftCodes { get; set; }

            [Display(Name = "Address: ")]
            public string BenDetailAcc_Address { get; set; }

            [Display(Name = "Swift(57A): ")]
            public string BenDetailAcc_Swift56A { get; set; }

            [Display(Name = "City: ")]
            public string BenDetailAcc_City { get; set; }

            [Display(Name = "IBAN(59): ")]
            public string BenDetailAcc_IBAN57A { get; set; }

            [Display(Name = "ZIP: ")]
            public string BenDetailAcc_Zip { get; set; }

            [Display(Name = "")]
            public string BenDetailAcc_ZipDropDown { get; set; }

            [Display(Name = "Other: ")]
            public string BenDetailAcc_Other { get; set; }

            [Display(Name = "Tick to send SWIFT urgently")]
            public bool BenDetailSendSWIFT_SendSWIFTCheckbox { get; set; }

            [Display(Name = "Swift (57A): ")]
            public string BenDetailSendSWIFT_Swift57A { get; set; }

            [Display(Name = "IBAN (59): ")]
            public string BenDetailSendSWIFT_IBAN59 { get; set; }

            [Display(Name = "Other/Swift reference: ")]
            public string BenDetailSendSWIFT_Other { get; set; }
            public string BackOfficePaymentFilePath { get; set; }
            [Display(Name = "Business Name: ")]
            public string BenMain_BusinessName { get; set; }
            [Display(Name = "Send Swift")]
            public bool Checkbox1 { get; set; }
            [Display(Name = "Send To System")]
            public bool Checkbox2 { get; set; }
            public int PaymentId { get; set; }
            public int? MessageId { get; set; }            

            [Display(Name = "SendTo: (SwiftCode): ")]
            public string sendTo { get; set; }
            [Display(Name = "Related Reference: ")]
            public string relatedRef { get; set; }

            [Display(Name = "Country: ")]
            public int? BenInter_Country { get; set; }

            [Display(Name = "Country: ")]
            public int? BenAcc_Country { get; set; }

        }
    }
}
