﻿$(function () {
    var PaymentId = (new URL(document.location)).searchParams.get("id");
    var l = abp.localization.getResource('Indo');

    var globalFile;
    var globalFolderName = 'BackOfficePaymentForm';
    var globalPath = "/";
    var ClientSwiftCode;
    var items = [];
    var benCCY;
    var benRate;
    loadDropDowns();
    $("select").removeClass("custom-select");
    $("#FeeTypeShoHide").hide();
    $("#benTemplateShoHide").hide();
    $("#FeeTypelistShoHide").hide();
    var paymentSelectAccountData;
    var templateData;
    var paymentHomeAddressData;
    var defaultSwift = '';
    var messageId = '';
    var validateForm = false;
    validateFormBenAcc = false;
    validateFormAcc = false;
    validateFormBenCountry = false;
    validateFormBenName = false;
    validateFormFeeType = false;
    validateFormRel = false;
    validateFormBenType = false;
    validateFormBusiness = false;
    validateFormIndi = false;
    validateFormPortfolio = false;
    validateFormOrderPortfolio = false;
    validateFormBenPortfolio = false;
    validateFormBenSwift = false;
    validateFormBenAccInsSwift = false;
    validateFormBenAccInsIBAN = false;
    validateFormBenIBAN = false;
    var orderTabShow = true;
    var BenTabShow = true;
    $('.symboleOkInterm').hide();
    $('.symboleRemoveInterm').hide();
    $('.symboleOk_').hide();
    $('.symboleRemove_').hide();
    $('.symboleOkOrder_Swift').hide();
    $('.symboleRemoveOrder_Swift').hide();
    $('.symboleOkSwift57A').hide();
    $('.symboleRemoveSwift57A').hide();
    $('.errorFeetype').hide();
    $(".hideShowbasedOn3rdparty").hide();
    $('#ViewModel_BenMain_Type').val(-1).trigger("change");
    $(".symboleOkBenDetail").hide();
    $(".symboleRemoveBenDetail").hide();
    $(".symboleOkBenDetailInter").hide();
    $(".symboleRemoveBenDetailInter").hide();
    $('.symboleOkBenDetailAcc').hide();
    $('.symboleRemoveBenDetailAcc').hide();
    $('#reqTabContent').hide();
    $('.symboleOkSendTo').hide();
    $('.symboleRemoveSendTo').hide();
    var ShowMessageDetails = false;
    $("#ViewModel_Amount").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorAmount'>Amount should be greater than 0<span>")
    $('.errorAmount').hide();
    $('#ViewModel_Amount').keyup(function (e) {
        var amt = $('#ViewModel_Amount').val();
        var formatAmt;
        formatAmt = parseFloat(amt.replaceAll(',', ""));

        if (formatAmt <= 0 || formatAmt == 0) {
            $('.errorAmount').show();
            validateForm = false;
        }
        else {
            $('.errorAmount').hide();
            validateForm = true;
        }
    });
    $("#ViewModel_Amount").focusin(function () {
        $('#ViewModel_Amount').val(parseFloat($('#ViewModel_Amount').val().replaceAll(',', "")));
    });
    $("#ViewModel_Amount").focusout(function () {
        var amt = $('#ViewModel_Amount').val();
        var formatAmt;
        if (!amt.includes(',')) {
            formatAmt = parseFloat(amt).toLocaleString();
        }
        else {
            formatAmt = parseFloat(amt.replaceAll(',', "")).toLocaleString();
        }
        if (!amt.includes('.')) {
            $('#ViewModel_Amount').val(formatAmt + '.00');
        } else {
            var modifyAmt = parseFloat(formatAmt.replaceAll(',', "")).toFixed(2)
            $('#ViewModel_Amount').val(parseFloat(modifyAmt).toLocaleString());
        }
        $('#balanceInc').text($('#ViewModel_Amount').val());
        $('#balanceIncBen').text($('#ViewModel_Amount').val());
        benRate = $('#ViewModel_Amount').val();
        postBalanceCal();
        postBalanceCalBen();
        changeBalanceColor();
        //if (parseFloat($('#postBalance').text().replaceAll(',', "")) < 0 && PaymentId == null && $("#ViewModel_Order_Account option:selected").val() != '-1' && $("#ViewModel_Order_Account option:selected").val() != undefined) {
        //    abp.notify.error("WARNING: Pending/trans amount is greater than the available balance in the order account");
        //}
    });

    check3rdPartyPayment = false;
    $("#MoveType, #ViewModel_Portfolio,  #ViewModel_CCY, #ViewModel.Order_Portfolio, #ViewModel.Order_PaymentOthers, #ViewModel.Order_Account, #ViewModel_Order_Country, #ViewModel_BenMain_Template, #ViewModel_BenMain_Type, #ViewModel_BenMain_Relationship, #ViewModel_BenMain_Country, #ViewModel_BenMain_Portfolio, #ViewModel_BenMain_Account, #portfoliolist, #ViewModel_Order_Account, #ViewModel_PaymentOthers, #paymentotherlist, #ViewModel_BenInter_Country, #ViewModel_BenAcc_Country").select2({
        placeholder: 'Select',
        language: {
            noResults: function (params) {
                return "Searching...";
            }
        }
    });
    $('.symboleOk').hide();
    $('.symboleRemove').hide();
    $('#ViewModel_BenMain_Type').val(-1);
    //$('#beneficiaryTab-tab').hide();
    $('#businessName').hide();
    $("#portfoliolistShoHide").hide();
    $("#benPortfolioShoHide").hide();

    changeBalanceColor();
    if (PaymentId) {
        $("#loader").show();
        $(".card-body").addClass("blurContent");
    }
    else {
        $("#loader").hide();
        $(".card-body").removeClass("blurContent");
    }
    $('#validatSwiftIntermediaryBank').attr('disabled', true);
    $('#validatSwiftInstitution').attr('disabled', true);
    $('#ViewModel_CCY').on('change', function (e) {
        manageDirectory();
        $('#currencyShortcut').text("")
        if ($('#ViewModel_CCY option:selected').text() != "Select") {
            $('#currencyShortcut').text($('#ViewModel_CCY option:selected').text());
        }
        triggerCashAcc();
        triggerCashAccBen();
        filtertemplate();
        clearAccountSelection();
        clearBenAccSelection();
        clearBenTempSelection();
        benCCY = $('#ViewModel_CCY option:selected').text();
        $(".FieldsTempShowHide").removeClass("disabledbutton");
        $(".accounntSelectionShowHide").removeClass("disabledbutton");
        $('#balance').text('00');
    });

    $('#ViewModel_Portfolio').on('change', function () {
        if (paymentSelectAccountData != null) {
            var portfolioVal = $("#ViewModel_Portfolio option:selected").val();
            var balanceAmt = paymentSelectAccountData.find(x => x.sP_ID == portfolioVal);
            if (balanceAmt != null) {
                $('#balance').text(parseFloat(balanceAmt.t_Code));
            }
            $('#balance').text('00');
            postBalanceCalBen();
            changeBalanceColor();
            filtertemplate();
        }
        if ($('#MoveType option:selected').text() != 'Other Movements') {
            triggerCashAcc();
            triggerCashAccBen();
        }
        manageDirectory();
        clearAccountSelection();
        clearBenAccSelection();
        clearBenTempSelection();
        $(".FieldsTempShowHide").removeClass("disabledbutton");
        $(".accounntSelectionShowHide").removeClass("disabledbutton");
    });

    $('#portfoliolist').on('change', function () {
        if ($('#MoveType option:selected').text() == 'Other Movements') {
            triggerCashAcc();
        }
        $(".FieldsTempShowHide").removeClass("disabledbutton");
        $(".accounntSelectionShowHide").removeClass("disabledbutton");
    });

    $('#ViewModel_BenMain_Portfolio').on('change', function () {
        if ($('#MoveType option:selected').text() == 'Other Movements') {
            triggerCashAccBen();
        }
    });

    var Res_msg = $('#statusMessaage').val();
    if (Res_msg.length > 0) {
        if (Res_msg) {
            abp.notify.success(Res_msg);
        } else {
            abp.notify.warn(Res_msg);
        }
    }
    var templateList;
    function filtertemplate() {
        $('#ViewModel_BenMain_Template option').remove();
        var ccyVal = $('#ViewModel_CCY').val();
        var portfolioVal = $('#ViewModel_Portfolio').val();
        var templates = [];
        if (templateList == undefined || templateList == null) {
            indo.backOfficePayment.backOfficePayment.fetchPaymentAddress(-1, "")
                .then(function (data) {
                    if (data != null) {
                        templateList = data;
                        templates = templateList.filter(x => x.pa_PortfolioID == portfolioVal && x.pa_CCYID == ccyVal && x.pa_TemplateName != "" && x.pa_TemplateName != "-1").map(({ pa_ID, pa_TemplateName }) => ({ id: pa_ID, text: pa_TemplateName }));
                        var uniqueTemplate = templates.reduce((unique, o) => {
                            if (!unique.some(obj => obj.text === o.text)) {
                                unique.push(o);
                            }
                            return unique;
                        }, []);
                        uniqueTemplate.unshift({ id: -1, text: 'Select' });
                        $("#ViewModel_BenMain_Template").select2({
                            data: uniqueTemplate,
                            placeholder: 'Select',
                        });
                        //populateTemplate();
                    }
                });
        }
        else {
            templates = templateList.filter(x => x.pa_PortfolioID == portfolioVal && x.pa_CCYID == ccyVal && x.pa_TemplateName != "" && x.pa_TemplateName != "-1").map(({ pa_ID, pa_TemplateName }) => ({ id: pa_ID, text: pa_TemplateName }));
            var uniqueTemplate = templates.reduce((unique, o) => {
                if (!unique.some(obj => obj.text === o.text)) {
                    unique.push(o);
                }
                return unique;
            }, []);
            uniqueTemplate.unshift({ id: -1, text: 'Select' });
            $("#ViewModel_BenMain_Template").select2({
                data: uniqueTemplate,
                placeholder: 'Select',
            });
            // populateTemplate();
        }
    };

    $('#ViewModel_BenMain_Template').on('change', function (e) {
        var tempName = $('#ViewModel_BenMain_Template option:selected').text();
        if (e.target.value != '-1' && e.target.value.trim() != '') {
            indo.backOfficePayment.backOfficePayment.fetchPaymentAddress(-1, tempName)
                .then(function (data) {
                    if (data != null) {
                        var selectedTemp2 = data.find(x => x.pa_TemplateSelected == 2);
                        if (selectedTemp2) {
                            $("#ViewModel_BenMain_Relationship").val(data[0].pa_RelationshipID).trigger("change");
                            $("#ViewModel_BenMain_Type").val(data[0].pa_BeneficiaryTypeID).trigger("change");
                            showHideBenBusiness();
                            $('#ViewModel_BenMain_BusinessName').val(data[0].pa_Name);
                            $('#ViewModel_BenMain_FirstName').val(data[0].pa_Firstname);
                            $('#ViewModel_BenMain_MiddleName').val(data[0].pa_Middlename);
                            $('#ViewModel_BenMain_Surname').val(data[0].pa_Surname);
                            $('#ViewModel_BenMain_Address').val(data[0].pa_Address1);
                            $('#ViewModel_BenMain_City').val(data[0].pa_Address2);
                            $('#ViewModel_BenMain_County').val(data[0].pa_County);
                            $('#ViewModel_BenMain_PostZip').val(data[0].pa_Zip);
                            $('#ViewModel_BenMain_Country').val(data[0].pa_CountryID).trigger("change");
                            $("#ViewModel_BenDetailSendSWIFT_Swift57A").val(data[0].pa_Swift);
                            $('#ViewModel_BenDetailSendSWIFT_Swift57A').keyup();
                            $("#ViewModel_BenDetailSendSWIFT_IBAN59").val(data[0].pa_IBAN);
                            $("#ViewModel_BenDetailSendSWIFT_IBAN59").keyup();
                        }
                        else {
                            clearBenBankDetailData();
                        }
                        var selectedTemp3 = data.find(x => x.pa_TemplateSelected == 3);
                        if (selectedTemp3) {
                            $("#ViewModel_BenDetail_Address").val(selectedTemp3.pa_Address1);
                            $("#ViewModel_BenDetail_Swift56A").val(selectedTemp3.pa_Swift);
                            $("#ViewModel_BenDetail_City").val(selectedTemp3.pa_Address2);
                            $("#ViewModel_BenDetail_IBAN57A").val(selectedTemp3.pa_IBAN);
                            $("#ViewModel_BenDetail_IBAN57A").keyup();
                            $("#ViewModel_BenDetail_Zip").val(selectedTemp3.pa_Zip);
                            $("#ViewModel_BenDetail_Other").val(selectedTemp3.pa_Other);
                            $("#ViewModel_BenDetail_IntermediaryBankSwiftCode").val(selectedTemp3.pa_Name);
                            $("#ViewModel_BenInter_Country").val(selectedTemp3.pa_CountryID).trigger("change");
                        }
                        else {
                            clearBenIntermediaryData();
                        }
                        var selectedTemp4 = data.find(x => x.pa_TemplateSelected == 4);
                        if (selectedTemp4) {
                            $("#ViewModel_BenDetailAcc_Address").val(selectedTemp4.pa_Address1);
                            $("#ViewModel_BenDetailAcc_Swift56A").val(selectedTemp4.pa_Swift);
                            $("#ViewModel_BenDetailAcc_City").val(selectedTemp4.pa_Address2);
                            $("#ViewModel_BenDetailAcc_IBAN57A").val(selectedTemp4.pa_IBAN);
                            $("#ViewModel_BenDetailAcc_IBAN57A").keyup();
                            $("#ViewModel_BenDetailAcc_Zip").val(selectedTemp4.pa_Zip);
                            $("#ViewModel_BenDetailAcc_Other").val(selectedTemp4.pa_Other);
                            $("#ViewModel_BenDetailAcc_AccountBankNameSwiftCode").val(selectedTemp4.pa_Name);
                            $("#ViewModel_BenAcc_Country").val(selectedTemp4.pa_CountryID).trigger("change");
                        }
                        else {
                            clearBenInstitutionData();
                        }
                    }
                    else {
                        clearBenBankDetailData();
                        clearBenIntermediaryData();
                        clearBenInstitutionData();
                    }
                });
            $(".FieldsTempShowHide").addClass("disabledbutton");
            $(".FieldsBenAccShowHide").addClass("disabledbutton");
        }
        else {
            clearBenTempSelection();

        }
    });

    function clearBenBankDetailData() {
        $("#ViewModel_BenDetailSendSWIFT_Swift57A").val('');
        $('#ViewModel_BenDetailSendSWIFT_Swift57A').keyup();
        $("#ViewModel_BenDetailSendSWIFT_IBAN59").val('');
        $('#ViewModel_BenDetailSendSWIFT_IBAN59').keyup();
    }

    function clearBenIntermediaryData() {
        $("#ViewModel_BenDetail_Address").val('');
        $("#ViewModel_BenDetail_Swift56A").val('');
        $("#ViewModel_BenDetail_City").val('');
        $("#ViewModel_BenDetail_IBAN57A").val('');
        $("#ViewModel_BenDetail_IBAN57A").keyup();
        $("#ViewModel_BenDetail_Zip").val('');
        $("#ViewModel_BenDetail_Other").val('');
        $("#ViewModel_BenInter_Country").val('').trigger("change");
        $("#ViewModel_BenDetail_IntermediaryBankSwiftCode").val('');
    }

    function clearBenInstitutionData() {
        $("#ViewModel_BenDetailAcc_Address").val('');
        $("#ViewModel_BenDetailAcc_Swift56A").val('');
        $("#ViewModel_BenDetailAcc_City").val('');
        $("#ViewModel_BenDetailAcc_IBAN57A").val('');
        $("#ViewModel_BenDetailAcc_IBAN57A").keyup();
        $("#ViewModel_BenDetailAcc_Zip").val('');
        $("#ViewModel_BenDetailAcc_Other").val('');
        $("#ViewModel_BenAcc_Country").val('').trigger("change");
        $("#ViewModel_BenDetailAcc_AccountBankNameSwiftCode").val('');
    }

    $('#ViewModel_Order_Account').on('change', function (e) {
        if (paymentSelectAccountData != null) {
            if (e.target.value != '-1') {
                var orderAccount = $('#ViewModel_Order_Account option:selected').text();
                var balanceOnselectAcc = paymentSelectAccountData.find(x => x.t_Name1 == orderAccount);
                var balance;
                if (balanceOnselectAcc != undefined) {
                    balance = formatBalance(parseFloat(balanceOnselectAcc.t_Code))
                    if (balance != null && balance != "NaN") {
                        addDecimal(balance);
                    }
                    $("#ViewModel_Order_Name").val(balanceOnselectAcc.t_Name1);
                    $('#ViewModel_Order_Address1').val(balanceOnselectAcc.b_Address);
                    $('#ViewModel_Order_City').val(balanceOnselectAcc.b_Area);
                    $('#ViewModel_Order_PostZip').val(balanceOnselectAcc.b_Zip);
                    $('#ViewModel_Order_Country option:selected').text(balanceOnselectAcc.sC_Country);
                    $('#ViewModel_Order_Swift').val(balanceOnselectAcc.b_SwiftAddress);
                    $('#ViewModel_Order_Swift').keyup();
                    $('#ViewModel_Order_IBAN').val(balanceOnselectAcc.t_IBAN);
                    $('#ViewModel_Order_IBAN').keyup();
                    $("#ViewModel_Order_Country").val(balanceOnselectAcc.sC_CountryID).trigger("change");
                    postBalanceCal();
                    ValidIBANConvertAndConfirm(balanceOnselectAcc.t_IBAN, 1);
                    if ($("#MoveType option:selected").val() != '-1') {
                        indo.backOfficeFOPs.backOfficeFOP.fetchPaymentTypes($("#MoveType option:selected").val())
                            .then(function (data) {
                                if (data != null && data[0].pT_Swift3rdPartyPayment != false) {
                                    if (balanceOnselectAcc.swift3rdPartyMessType != null) {
                                        defaultSwift = balanceOnselectAcc.swift3rdPartyMessType;
                                    }
                                }
                            });
                    }
                }
                $(".accounntSelectionShowHide").addClass("disabledbutton");
            }
            else {
                clearAccountSelection();
                $(".accounntSelectionShowHide").removeClass("disabledbutton");
                $('#balance').text('00');
            }
        }
    });

    $('#ViewModel_BenMain_Account').on('change', function (e) {
        if (paymentSelectAccountData != null) {
            if (e.target.value != '-1') {
                var orderAccount = $('#ViewModel_BenMain_Account option:selected').text();
                var balanceOnselectAcc = paymentSelectAccountData.find(x => x.t_Name1 == orderAccount);
                if (balanceOnselectAcc) {
                    $("#ViewModel_BenMain_Name").val(balanceOnselectAcc.t_Name1);
                    $('#ViewModel_BenMain_Address').val(balanceOnselectAcc.b_Address);
                    $('#ViewModel_BenMain_City').val(balanceOnselectAcc.b_Area);
                    $('#ViewModel_BenMain_PostZip').val(balanceOnselectAcc.b_Zip);
                    $('#ViewModel_BenMain_Country').val(balanceOnselectAcc.sC_CountryID).trigger("change");
                    $('#ViewModel_BenDetailSendSWIFT_Swift57A').val(balanceOnselectAcc.b_ShortCut1);
                    $('#ViewModel_BenDetailSendSWIFT_IBAN59').val(balanceOnselectAcc.t_IBAN);
                    $('#ViewModel_BenDetailSendSWIFT_Swift57A').keyup();
                    $("#ViewModel_BenDetailSendSWIFT_IBAN59").keyup();
                    var balance;
                    balance = formatBalance(parseFloat(balanceOnselectAcc.t_Code))
                    if (balance != null && balance != "NaN") {
                        addDecimalBen(balance);
                    }
                    postBalanceCalBen();
                    $(".FieldsBenAccShowHide").addClass("disabledbutton");
                }


            }
            else {
                clearBenAccSelection();
                $(".FieldsBenAccShowHide").removeClass("disabledbutton");
            }
        }
    });

    $('#ViewModel_Order_Swift').keyup(function (e) {
        var swiftCode = e.target.value;
        if (swiftCode) {
            if (swiftCode.length == 8 || swiftCode.length == 11) {
                $('.symboleOkOrder_Swift').show();
                $('.symboleRemoveOrder_Swift').hide();
            }
            else {
                $('.symboleOkOrder_Swift').hide();
                $('.symboleRemoveOrder_Swift').show();
            }
        }
        else {
            $('.symboleOkOrder_Swift').hide();
            $('.symboleRemoveOrder_Swift').hide();
        }
    });

    $('#ViewModel_BenDetailSendSWIFT_Swift57A').keyup(function (e) {

        var swiftCode = e.target.value;
        if (swiftCode) {
            if (swiftCode.length == 8 || swiftCode.length == 11) {
                $('.symboleOkSwift57A').show();
                $('.symboleRemoveSwift57A').hide();
            }
            else {
                $('.symboleOkSwift57A').hide();
                $('.symboleRemoveSwift57A').show();
            }

        } else {
            $('.symboleRemoveSwift57A').hide();
            $('.symboleOkSwift57A').hide();
        }
    });

    function postBalanceCal() {
        var balance = $('#balance').text();
        var balanceInc = $('#balanceInc').text();
        if (balanceInc.includes(".")) {
            balanceInc = parseFloat(balanceInc.replaceAll(",", ""));
        } else {
            balanceInc = parseFloat(balanceInc.replaceAll(/\D/g, ''));
        }
        var postBalance = parseFloat(balance.split('.')[0].replaceAll(/\D/g, '')) - (parseFloat(balanceInc));
        var newPostBalance = formatBalance(postBalance);
        if (!newPostBalance.includes(".")) {
            newPostBalance = newPostBalance + '.00';
        }
        $('#postBalance').text(newPostBalance);
        if (parseFloat($('#postBalance').text().replaceAll(',', "")) < 0 && $("#ViewModel_Order_Account option:selected").val() != '-1' && $("#ViewModel_Order_Account option:selected").val() != undefined && orderTabShow == true && $(".toast-message").text().length == 0) {
            abp.notify.error("WARNING: Pending/trans amount is greater than the available balance in the order account");
        }
        changeBalanceColor();
    }

    function postBalanceCalBen() {
        var balance = $('#balanceBen').text();
        var balanceInc = $('#balanceIncBen').text();
        if (balanceInc.includes(".")) {
            balanceInc = parseFloat(balanceInc.replaceAll(",", ""));
        } else {
            balanceInc = parseFloat(balanceInc.replaceAll(/\D/g, ''));
        }
        var postBalance = parseFloat(balance.split('.')[0].replaceAll(/\D/g, '')) + (parseFloat(balanceInc));
        var newPostBalance = formatBalance(postBalance);
        if (!newPostBalance.includes(".")) {
            newPostBalance = newPostBalance + '.00';
        }
        $('#postBalanceBen').text(newPostBalance);
        changeBalanceColor();
    }

    function changeBalanceColor() {
        if (parseFloat($('#balance').text().replaceAll(',', "")) < 0) {
            $('#balance').css('color', 'red');
        }
        else { $('#balance').css('color', 'blue'); }
        if (parseFloat($('#balanceInc').text().replaceAll(',', "")) < 0) {
            $('#balanceInc').css('color', 'red');
        }
        else { $('#balanceInc').css('color', 'blue'); }
        if (parseFloat($('#postBalance').text().replaceAll(',', "")) < 0) {
            $('#postBalance').css('color', 'red');
        }
        else { $('#postBalance').css('color', 'blue'); }
    }

    function manageDirectory() {
        var portfolioText;
        if ($('#MoveType option:selected').text() == 'Other Movements') {
            portfolioText = $("#portfoliolist option:selected").text();
        } else {
            portfolioText = $("#ViewModel_Portfolio option:selected").text();
        }
        var CCYText = $("#ViewModel_CCY option:selected").text();
        if (portfolioText != 'Select' && CCYText != 'Select') {
            var newFolderName = portfolioText + '-' + CCYText;
            globalPath = "/" + newFolderName + "/";
            globalFile.createFolder(newFolderName)
            globalFile.refresh();
        }
    }

    $('#MoveType').on('change', function () {

        var moveTypeValue = $("#MoveType option:selected").text();
        switch (moveTypeValue) {
            case ('External Payment'):
                $("#FeeTypelistShoHide").hide();
                $("#benPortfolioShoHide").hide();
                $("#FeeTypeShoHide").hide();
                $("#accountShoHide").show();
                $("#portfoliolistShoHide").hide();
                $(".FieldsTempShowHide").removeClass("disabledbutton");
                $(".FieldsBenAccShowHide").removeClass("disabledbutton");
                $("#showHideBenref").hide();
                //clearBenTempSelection();
                $('#ViewModel_BenMain_Template').val(-1).trigger("change");
                break;
            case ('Other Movements'):
                $("#FeeTypeShoHide").show();
                $("#accountShoHide").hide();
                $("#FeeTypelistShoHide").hide();
                $("#portfoliolistShoHide").show();
                $("#benPortfolioShoHide").show();
                $('#ViewModel_Order_Account option').remove();
                $("#showHideBenref").show();
                $('#ViewModel_BenMain_Account option').remove();
                $('#ViewModel_Portfolio').val(-1).trigger("change")
                clearAccountSelection();
                clearBenAccSelection();
                clearBenTempSelection();
                break;
            case ('Internal Client to Client (Loan)'):
                $("#FeeTypelistShoHide").hide();
                $("#portfoliolistShoHide").hide();
                $("#benPortfolioShoHide").hide();
                $("#FeeTypeShoHide").hide();
                $("#showHideBenref").show();
                clearBenTempSelection();
                clearBenAccSelection();
                break;
            case ('External Client Payment'):
                $("#showHideBenref").hide();
                $("#accountShoHide").show();
                $("#FeeTypeShoHide").hide();
                $("#portfoliolistShoHide").hide();
                $("#benPortfolioShoHide").hide();
                clearBenAccSelection();
                clearBenTempSelection();
                $('#ViewModel_BenMain_Template').val(-1).trigger("change");
                $(".FieldsTempShowHide").removeClass("disabledbutton");
                $(".FieldsBenAccShowHide").removeClass("disabledbutton");
                break;
            default:
                $("#portfolioShoHide").show();
                $("#accountShoHide").show();
                $("#FeeTypeShoHide").hide();
                $("#FeeTypelistShoHide").hide();
                $("#portfoliolistShoHide").hide();
                $("#Benmainbalance").show();
                $("#benPortfolioShoHide").hide();
                $("#showHideBenref").show();
                clearBenTempSelection();
                clearBenAccSelection();
        }

        indo.backOfficeFOPs.backOfficeFOP.fetchPaymentTypes($("#MoveType option:selected").val())
            .then(function (data) {
                if (data != null) {
                    if (data[0].pT_ShortCode != null && data[0].pT_ShortCode != undefined) {

                        $('#ViewModel_Order_Ref').val(data[0].pT_ShortCode + '0000000');
                        $('#ViewModel_BenMain_Order_Ref').val(data[0].pT_ShortCode + '0000000_1');
                    }
                    if ($('#MoveType option:selected').text() != 'Other Movements') {
                        defaultSwift = data[0].m_MessageType;
                    }
                    var payTypeDetails = data.find(x => x.pT_PaymentType = moveTypeValue)
                    if (payTypeDetails.pT_ShowOrder) {
                        $('#ordertTab-tab').show();
                        $('.ordertTabContent').show();
                        $('#ordertTab-tab').click();
                        orderTabShow = true;
                    }
                    else {
                        $('#ordertTab-tab').hide();
                        $('.ordertTabContent').hide();
                        $('#beneficiaryTab-tab').click();
                        orderTabShow = false;
                    }
                    if (payTypeDetails.pT_ShowBeneficiary) {
                        $('#beneficiaryTab-tab').show();
                        $('.beneficiaryTabContent').show();
                    }
                    else {
                        $('#beneficiaryTab-tab').hide();
                        $('.beneficiaryTabContent').hide();
                        $('#ordertTab-tab').click();
                    }
                    if (payTypeDetails.pT_ShowOrder == false && payTypeDetails.pT_ShowBeneficiary == false) {
                        $("#files-tab").click();
                    }
                    if (payTypeDetails.pT_Swift3rdPartyPayment) {

                        $("#benTemplateShoHide").show();
                        $("#benAccountShoHide").hide();
                        $("#benNameShoHide").hide()
                        $(".hideShowbasedOn3rdparty").show();
                        check3rdPartyPayment = true;
                        $('#interInstitution-tab').show();
                        $('#accWithInstitution-tab').show();
                        $('#individualName').show();
                    }
                    else {
                        $("#benTemplateShoHide").hide();
                        $("#benAccountShoHide").show();
                        $("#benNameShoHide").show()
                        $(".hideShowbasedOn3rdparty").hide();
                        check3rdPartyPayment = false;
                        $('#interInstitution-tab').hide();
                        $('#accWithInstitution-tab').hide();
                        $('#individualName').hide();
                    }
                    if (payTypeDetails.pT_SwiftTransferPayment) {
                        $('#individualName').hide();
                        $('#businessName').hide();
                    }
                    else {
                        $('#individualName').show();
                        $('#businessName').show();
                    }
                    if (payTypeDetails.pT_DefaultSendSwift) {
                        $('#ViewModel_Checkbox1')[0].checked = true;
                    }
                    else {
                        $('#ViewModel_Checkbox1')[0].checked = false;
                    }
                    if (payTypeDetails.pT_DefaultSendIMS) {
                        $('#ViewModel_Checkbox2')[0].checked = true;
                    }
                    else {
                        $('#ViewModel_Checkbox2')[0].checked = false;
                    }
                    if (payTypeDetails.m_ID) {
                        messageId = payTypeDetails.m_ID;
                    }
                }
            });
    });

    $('#ViewModel_PaymentOthers').on('change', function (e) {
        if ($('#MoveType option:selected').text() == 'Other Movements') {
            if (e.target.value != '-1' && e.target.value != "" && e.target.value != null) {
                indo.backOfficePayment.backOfficePayment.fetchPaymentOthers(e.target.value)
                    .then(function (data) {
                        if (data != null) {
                            if ($('#MoveType option:selected').text() == 'Other Movements') {
                                defaultSwift = data[0].m_MessageType;
                            } else {
                                defaultSwift = null;
                            }
                            if (data[0].otH_ShowOrder) {
                                orderTabShow = true;
                                $('#ordertTab-tab').show();
                                $('.ordertTabContent').show();
                            }
                            else {
                                orderTabShow = false;
                                $('#ordertTab-tab').hide();
                                $('.ordertTabContent').hide();
                                $('#beneficiaryTab-tab').click();
                            }
                            if (data[0].otH_ShowBeneficiary) {
                                BenTabShow = true;
                                $('#beneficiaryTab-tab').show();
                                $('.beneficiaryTabContent').show();
                            }
                            else {
                                BenTabShow = false;
                                $('#beneficiaryTab-tab').hide();
                                $('.beneficiaryTabContent').hide();
                                $('#ordertTab-tab').click();
                            }
                            if (data[0].otH_ShowOrder == false && data[0].otH_ShowBeneficiary == false) {
                                $("#files-tab").click();
                            }
                            if (data[0].otH_ShowMessageDetails) {
                                if (PaymentId == null || PaymentId == undefined) {
                                    $("#ViewModel_Amount").val(0.01);
                                    $('#ViewModel_Amount').keyup()
                                    $("#ViewModel_CCY").val(2).trigger("change");
                                    $("#ViewModel_CCY").select2({ disabled: 'readonly' });
                                }
                                $("#ViewModel_Amount").addClass("disabledbutton");
                                $("#ViewModel_TransDate").addClass("disabledbutton");
                                $("#ViewModel_SettleDate").addClass("disabledbutton");
                                $("#requestTab-tab").click();
                                $('#reqTabContent').show();
                                ShowMessageDetails = true;
                                populateReqTab();
                            } else {
                                if (PaymentId == null || PaymentId == undefined) {
                                    $("#ViewModel_Amount").val(0.00);
                                    $('#ViewModel_Amount').keyup()
                                    $("#ViewModel_CCY").val(-1).trigger("change");
                                    $("#ViewModel_CCY").select2({ disabled: false });
                                }
                                $("#ViewModel_Amount").removeClass("disabledbutton");
                                $("#ViewModel_TransDate").removeClass("disabledbutton");
                                $("#ViewModel_SettleDate").removeClass("disabledbutton");
                                $('#reqTabContent').hide();
                                ShowMessageDetails = false;
                                populateReqTab();
                            }

                            if (data[0].otH_DefaultSendSwift) {
                                $('#ViewModel_Checkbox1')[0].checked = true;
                            }
                            else {
                                $('#ViewModel_Checkbox1')[0].checked = false;
                            }
                            if (data[0].otH_DefaultSendIMS) {
                                $('#ViewModel_Checkbox2')[0].checked = true;
                            }
                            else {
                                $('#ViewModel_Checkbox2')[0].checked = false;
                            }
                        }
                        manageDirectory();
                    });
            } else {
                defaultSwift = null;
            }
        }
    });

    loadFileManager();

    setTimeout(function () {
        refreshFile();
    }, 1000)

    function loadFileManager() {
        var hostUrl = '/';
        var fileObject = new ej.filemanager.FileManager({
            ajaxSettings: {
                url: hostUrl + 'api/FileManagerFees/FileOperations',
                getImageUrl: hostUrl + 'api/FileManagerFees/GetImage',
                uploadUrl: hostUrl + 'api/FileManagerFees/Upload',
                downloadUrl: hostUrl + 'api/FileManagerFees/Download?folder=' + globalFolderName
            },
            toolbarSettings: { items: ['Upload', 'Delete', 'Refresh'], visible: true },
            beforeSend: (beforeSendEvent) => {
                var request = JSON.parse(beforeSendEvent.ajaxSettings.data);
                if (request.action == "read") {

                    request.path = globalPath
                    request.data = []
                    var updatedRequest = JSON.stringify(request);
                    beforeSendEvent.ajaxSettings.data = updatedRequest
                }
                if (beforeSendEvent.action == "Upload") {
                    request[0].path = globalPath
                    var updatedRequest = JSON.stringify(request);
                    beforeSendEvent.ajaxSettings.data = updatedRequest
                }
                if (request.action == "delete") {
                    request.path = globalPath
                    var updatedRequest = JSON.stringify(request);
                    beforeSendEvent.ajaxSettings.data = updatedRequest
                }
                beforeSendEvent.ajaxSettings.beforeSend = function (beforeSendEvent) {
                    beforeSendEvent.httpRequest.setRequestHeader('folder', globalFolderName);
                }
            },
            //beforeImageLoad: (beforeLoadImageEvent) => {
            //    beforeLoadImageEvent.imageUrl = beforeLoadImageEvent.imageUrl + '&folder=' + globalFolderName;
            //},
            beforeImageLoad: (beforeImageLoadEvent) => {
                beforeImageLoadEvent.imageUrl = beforeImageLoadEvent.imageUrl + '&folder=' + globalFolderName;
            },
            created: () => {
                var fileManagerHeight = ($(".pcoded-main-container").height()) - 8;
                fileObject.height = fileManagerHeight;
            },
            beforePopupOpen(args) {
                if (args.popupName == "Error") {
                    args.cancel = true;
                }
            },
            view: 'Details',
            failure: (event) => {
                if (event.action == 'create' && event.action == 'delete' && event.error.code == '400') {
                    globalFile.refresh();
                }
            }
        });
        globalFile = fileObject;
        fileObject.appendTo('#filemanager');
    }

    function refreshFile() {
        try {
            var folderNames = [];
            globalFile.selectAll();
            var allDir = globalFile.getSelectedFiles();
            for (var dir = 0; dir < allDir.length; dir++) {
                folderNames.push(allDir[dir].name);
            }
            //if (PaymentId == null || PaymentId == undefined) {
            //    globalFile.deleteFiles(folderNames);
            //}
            globalFile.refresh();
        } catch (e) {
        }
    }

    $('li.nav-item').click(function (e) {
        if (e.target != null && e.target.text == 'Preview') {
            $('#previewTab').html("");
            if ($('#MoveType option:selected').text() != 'Select') {
                if ($('#ViewModel_Amount').val().replaceAll(',', "") > 0) {
                    if (defaultSwift == 'MT200') {
                        const swiftBackOfficePaymentDto = {
                            clientSwiftCode: ClientSwiftCode,
                            orderSwiftCode: paymentHomeAddressData.swiftCode,
                            orderRef: "REFER0000000",
                            settleDate: $('#ViewModel_SettleDate').val(),
                            amount: parseFloat($('#ViewModel_Amount').val().replaceAll(',', "")),
                            ccy: $('#ViewModel_CCY option:selected').text(),
                            benIBAN: $('#ViewModel_BenDetailSendSWIFT_IBAN59').val(),
                            orderIBAN: $('#ViewModel_Order_IBAN').val(),
                            orderOther: $('#ViewModel_Order_Other').val(),
                            swiftUrgent: false,
                            email: "",
                            ftp: false
                        };
                        if ($('#MoveType option:selected').text() != 'Other Movements') {
                            if ($('#ViewModel_CCY option:selected').text() != 'Select' && $('#ViewModel_BenDetailSendSWIFT_IBAN59').val() != '' && $('#ViewModel_Order_IBAN').val() != '') {
                                indo.backOfficePayment.backOfficePayment.fetchSwiftBackOfficePaymentmt200(swiftBackOfficePaymentDto)
                                    .then(function (data) {
                                        if (data != null) {
                                            var swiftRec = data.split("\n").join("<br />");
                                            $('#previewTab').html(swiftRec);
                                        }
                                    });
                            }
                            else {
                                abp.notify.warn("Please fill required fields <br> 1.Move Type<br> 2.CCY <br> 3.Order > IBAN<br> 4.Ben > Bank Details > IBAN");
                            }
                        }
                        else {
                            if ($('#ViewModel_CCY option:selected').text() != 'Select') {
                                if (ShowMessageDetails == true || (orderTabShow == true && BenTabShow == true)) {
                                    indo.backOfficePayment.backOfficePayment.fetchSwiftBackOfficePaymentmt200(swiftBackOfficePaymentDto)
                                        .then(function (data) {
                                            if (data != null) {
                                                var swiftRec = data.split("\n").join("<br />");
                                                $('#previewTab').html(swiftRec);
                                            }
                                        });
                                }
                                else {
                                    abp.notify.warn("No Preview");
                                }
                            }
                            else {
                                abp.notify.warn("Please fill required fields <br> 1.Move Type<br> 2.CCY");
                            }
                        }
                    }
                    else if (defaultSwift == 'MT103' || defaultSwift == 'MT101' || defaultSwift == 'MT202') {
                        const swiftBackOfficePaymentDto = {
                            clientSwiftCode: ClientSwiftCode,
                            settleDate: $('#ViewModel_SettleDate').val(),
                            amount: parseFloat($('#ViewModel_Amount').val().replaceAll(',', "")),
                            ccy: $('#ViewModel_CCY option:selected').text(),
                            orderRef: "REFER0000000",
                            orderName: paymentHomeAddressData.name,
                            orderAddress: paymentHomeAddressData.address,
                            orderCity: paymentHomeAddressData.city,
                            orderPostCode: paymentHomeAddressData.postCode,
                            orderSwift: paymentHomeAddressData.swiftCode,
                            orderIbanAccountNumber: $('#ViewModel_Order_IBAN').val(),
                            orderOther: $('#ViewModel_Order_Other').val(),
                            orderVOCode: "string",
                            beneficiaryName: $('#ViewModel_BenMain_Name').val() ? $('#ViewModel_BenMain_Name').val() : $('#ViewModel_BenMain_BusinessName').val(),
                            beneficiaryAddress1: $('#ViewModel_BenMain_Address').val() ? $('#ViewModel_BenMain_Address').val() : $('#ViewModel_BenDetailAcc_Address').val(),
                            beneficiaryAddress2: $('#ViewModel_BenMain_City').val() ? $('#ViewModel_BenMain_City').val() : $('#ViewModel_BenDetailAcc_City').val(),
                            beneficiaryPostCode: $('#ViewModel_BenMain_PostZip').val() ? $('#ViewModel_BenMain_PostZip').val() : $('#ViewModel_BenDetailAcc_Zip').val(),
                            beneficiarySwift: $('#ViewModel_BenDetailSendSWIFT_Swift57A').val(),
                            beneficiaryIBAN: $('#ViewModel_BenDetailSendSWIFT_IBAN59').val(),
                            beneficiaryCcy: $('#ViewModel_CCY option:selected').text() != 'Select' ? $('#ViewModel_CCY option:selected').text() : '',
                            beneficiarySubBankName: $('#ViewModel_BenDetail_IntermediaryBankSwiftCode').val(),
                            beneficiarySubBankAddress1: $('#ViewModel.BenDetail_Address').val(),
                            beneficiarySubBankAddress2: $('#ViewModel_BenDetail_City').val(),
                            beneficiarySubBankPostCode: $('#ViewModel_BenDetail_Zip').val(),
                            beneficiarySubBankSwift: $('#ViewModel_BenDetail_Swift56A').val(),
                            beneficiarySubBankIBAN: $('#ViewModel_BenDetail_IBAN57A').val(),
                            beneficiarySubBankBik: paymentSelectAccountData[0].bik ? paymentSelectAccountData[0].bik : "",
                            beneficiarySubBankINN: paymentSelectAccountData[0].inn ? paymentSelectAccountData[0].inn : "",
                            beneficiaryBankName: $('#ViewModel_BenDetailAcc_AccountBankNameSwiftCode').val(),
                            beneficiaryBankAddress1: $('#ViewModel_BenDetailAcc_Address').val(),
                            beneficiaryBankAddress2: $('#ViewModel_BenDetailAcc_City').val(),
                            beneficiaryBankPostCode: $('#ViewModel_BenDetailAcc_Zip').val(),
                            beneficiaryBankSwift: $('#ViewModel_BenDetailAcc_Swift56A').val(),
                            beneficiaryBankIBAN: $('#ViewModel_BenDetailAcc_IBAN57A').val(),
                            swiftUrgent: false,
                            swiftSendRef: false,
                            exchangeRate: 0,
                            email: "",
                            ftp: false
                        };
                        if ($('#MoveType option:selected').text() != 'Other Movements') {
                            if ($('#ViewModel_CCY option:selected').text() != 'Select' && $('#ViewModel_Order_IBAN').val() != '') {
                                indo.backOfficePayment.backOfficePayment.fetchSwiftBackOfficePayment(swiftBackOfficePaymentDto)
                                    .then(function (data) {
                                        if (data != null) {
                                            var swiftRec = data.split("\n").join("<br />");
                                            $('#previewTab').html(swiftRec);
                                        }
                                    });
                            }
                            else {
                                abp.notify.warn("Please fill required fields <br> 1.Move Type <br> 2.CCY <br> 3.Order > IBAN");
                            }
                        }
                        else {
                            if ($('#ViewModel_CCY option:selected').text() != 'Select') {
                                if (ShowMessageDetails == true || (orderTabShow == true && BenTabShow == true)) {
                                    indo.backOfficePayment.backOfficePayment.fetchSwiftBackOfficePayment(swiftBackOfficePaymentDto)
                                        .then(function (data) {
                                            if (data != null) {
                                                var swiftRec = data.split("\n").join("<br />");
                                                $('#previewTab').html(swiftRec);
                                            }
                                        });
                                }
                                else {
                                    abp.notify.warn("No Preview");
                                }
                            }
                            else {
                                abp.notify.warn("Please fill required fields <br> 1.Move Type <br> 2.CCY");
                            }
                        }
                    }
                    else if (defaultSwift == 'MT210') {
                        const swiftBackOfficePaymentDto = {
                            clientSwiftCode: ClientSwiftCode,
                            benSwiftCode: $('#ViewModel_BenDetailSendSWIFT_Swift57A').val(),
                            orderRef: "REFER0000000",
                            brokerAccountNo: "string",
                            relatedOrderRef: $('#ViewModel_BenMain_Order_Ref').val(),
                            settleDate: $('#ViewModel_SettleDate').val(),
                            amount: parseFloat($('#ViewModel_Amount').val().replaceAll(',', "")),
                            ccy: $('#ViewModel_CCY option:selected').text(),
                            orderSwiftCode: $('#ViewModel_Order_Swift').val() ? $('#ViewModel_Order_Swift').val() : '',
                            benSubBankSwift: $('#ViewModel_BenDetail_Swift56A').val() ? $('#ViewModel_BenDetail_Swift56A').val() : '',
                            swiftUrgent: false,
                            email: "",
                            ftp: false
                        };
                        if ($('#MoveType option:selected').text() != 'Other Movements') {
                            if ($('#ViewModel_CCY option:selected').text() != 'Select' && $('#ViewModel_BenDetailSendSWIFT_Swift57A').val() != '') {
                                indo.backOfficePayment.backOfficePayment.fetchSwiftBackOfficePaymentMt210(swiftBackOfficePaymentDto)
                                    .then(function (data) {
                                        if (data != null) {
                                            var swiftRec = data.split("\n").join("<br />");
                                            $('#previewTab').html(swiftRec);
                                        }
                                    });
                            }
                            else {
                                abp.notify.warn("Please fill required fields <br> 1.Move Type <br> 2.CCY <br> 3.Ben > Bank > Swift");
                            }
                        }
                        else {
                            if ($('#ViewModel_CCY option:selected').text() != 'Select') {
                                swiftBackOfficePaymentDto.benSwiftCode = $("#ViewModel_sendTo").val();
                                swiftBackOfficePaymentDto.relatedOrderRef = $("#ViewModel_sendTo").val();
                                if (ShowMessageDetails == true || (orderTabShow == true && BenTabShow == true)) {
                                    indo.backOfficePayment.backOfficePayment.fetchSwiftBackOfficePaymentMt210(swiftBackOfficePaymentDto)
                                        .then(function (data) {
                                            if (data != null) {
                                                var swiftRec = data.split("\n").join("<br />");
                                                $('#previewTab').html(swiftRec);
                                            }
                                        });
                                }
                                else {
                                    abp.notify.warn("No Preview");
                                }
                            }
                            else {
                                abp.notify.warn("Please fill required fields <br> 1.Move Type <br> 2.CCY <br> 3.Request > SendTo: (SwiftCode)");
                            }
                        }
                    }
                    else if (defaultSwift == 'MT99' || defaultSwift == 'MT199' || defaultSwift == 'MT299' || defaultSwift == 'MT399' || defaultSwift == 'MT499' || defaultSwift == 'MT599' || defaultSwift == 'MT699' || defaultSwift == 'MT799' || defaultSwift == 'MT899' || defaultSwift == 'MT999') {
                        const swiftBackOfficePaymentDto = {
                            clientSwiftCode: ClientSwiftCode,
                            orderSwiftCode: $('#ViewModel_Order_Swift').val(),
                            orderRef: "REFER0000000",
                            orderRelatedRef: $('#ViewModel_BenMain_Order_Ref').val() ? $('#ViewModel_BenMain_Order_Ref').val() : '',
                            message: $('#ViewModel_Comments').val(),
                            swiftUrgent: false,
                            email: "",
                            ftp: false
                        };
                        if ($('#MoveType option:selected').text() != 'Other Movements') {
                            if ($('#ViewModel_CCY option:selected').text() != 'Select' && $('#ViewModel_Order_Swift').val() != '') {
                                indo.backOfficePayment.backOfficePayment.fetchSwiftBackOfficePaymentMt299(swiftBackOfficePaymentDto)
                                    .then(function (data) {
                                        if (data != null) {
                                            var swiftRec = data.split("\n").join("<br />");
                                            $('#previewTab').html(swiftRec);
                                        }
                                    });
                            }
                            else {
                                abp.notify.warn("Please fill required fields <br> 1.Move Type <br> 2.CCY <br> 3.Order > Swift");
                            }
                        }
                        else {
                            if ($('#ViewModel_CCY option:selected').text() != 'Select') {
                                swiftBackOfficePaymentDto.orderSwiftCode = $("#ViewModel_sendTo").val();
                                swiftBackOfficePaymentDto.orderRelatedRef = $("#ViewModel_sendTo").val();
                                if (ShowMessageDetails == true || (orderTabShow == true && BenTabShow == true)) {
                                    indo.backOfficePayment.backOfficePayment.fetchSwiftBackOfficePaymentMt299(swiftBackOfficePaymentDto)
                                        .then(function (data) {
                                            if (data != null) {
                                                var swiftRec = data.split("\n").join("<br />");
                                                $('#previewTab').html(swiftRec);
                                            }
                                        });
                                }
                                else {
                                    abp.notify.warn("No Preview");
                                }
                            }
                            else {
                                abp.notify.warn("Please fill required fields <br> 1.Move Type <br> 2.CCY <br> 3.Request > SendTo: (SwiftCode)");
                            }
                        }
                    }
                    else if ((defaultSwift == null || defaultSwift == '') && ($('#MoveType option:selected').text() != 'Other Movements')) {
                        abp.notify.warn("m_MessageType is null for selected Move Type");
                    }
                    else if ((defaultSwift == null || defaultSwift == '') && ($('#MoveType option:selected').text() == 'Other Movements')) {
                        abp.notify.warn("m_MessageType is null for selected Fee Type");
                    }
                    else {
                        abp.notify.warn(defaultSwift + " Preview is not implemented");
                    }
                }
                else {
                    abp.notify.warn("Amount should be greater than 0");
                }
            } else {
                abp.notify.warn(defaultSwift + "Please select Move Type");
            }
        }
    });

    $('#ViewModel_BenMain_Type').on('change', function () {
        showHideBenBusiness();
    });

    function setTwoNumberDecimal(event) {
        this.value = parseFloat(this.value).toFixed(2);
    }

    $("#ViewModel_BenMain_BusinessName").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger error' id='error'>Business Name: is required<span>")
    $("#ViewModel_BenMain_Surname").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger error' id='surNameError'>Surname: is required<span>")

    $("form").submit(function (e) {
        if (validateDate($("#ViewModel_TransDate").val()) < validateDate(new Date(new Date().setHours(0, 0, 0, 0)))) {
            abp.notify.warn("Trans date should be from today onwards");
            e.preventDefault();
            return false;
        }
        if ($("#ViewModel_SettleDate").val() < $("#ViewModel_TransDate").val()) {
            e.preventDefault();
            abp.notify.warn("Settle Date should be greater or equal to Trans Date");
            return false;
        }
        $("#error").addClass("error").removeClass("error_show");
        $("#surNameError").addClass("error").removeClass("error_show");
        if ($('#ViewModel_BenMain_Type option:selected').text() == 'Business') {
            if ($("#ViewModel_BenMain_BusinessName").val() == '') {
                $("#error").removeClass("error").addClass("error_show");
                e.preventDefault();
                return false;
            }
            else {
                $("#error").addClass("error").removeClass("error_show");
            }
        }
        var settleDate = $('#ViewModel_SettleDate').val();
        if (($("#ViewModel_Checkbox1").prop('checked') == true) && settleDate == new Date().toISOString().slice(0, 10)) {

            if (($('#ViewModel_Order_Account').val() != null) && ($('#ViewModel_CCY').val() != null) && ($('#ViewModel_Portfolio').val() != null)) {
                var ccyCode = $('#ViewModel_CCY').val();
                var accountCode = $('#ViewModel_Order_Account').val();
                var accountName = $('#ViewModel_Order_Account option:selected').text()

                if (ccyCode != null && accountCode != null) {
                    accountCode = paymentSelectAccountData.filter(x => x.t_Name1 == accountName);
                    if (accountCode && accountCode.length != 0) {
                        indo.backOfficePayment.backOfficePayment.fetchCutOffPayments(accountCode[0].b_Code, parseInt(ccyCode))
                            .then(function (data) {
                                if (data[0].cO_CutOffTime != null && data[0].cO_CutOffTimeZone != null) {
                                    var convertedTimeZone = new Date().toLocaleString("sv-SE").slice(0, -3);
                                    if ((settleDate + (" " + data[0].cO_CutOffTime)) < convertedTimeZone) {
                                        var cutOffmessage = "The cut off time from the broker is at " + data[0].cO_CutOffTime + " " + data[0].cO_CutOffTimeZone;
                                        $('.WARNINGColor').text(cutOffmessage);
                                        $(".WARNINGColor").css("color", "Green");
                                    }
                                    else {
                                        var cutOffmessage1 = "WARNING: You have now passed the cut off time. The cut off time for this broker was at " + data[0].cO_CutOffTime + " " + data[0].cO_CutOffTimeZone;
                                        $('.WARNINGColor').text(cutOffmessage1);
                                        $(".WARNINGColor").css("color", "Red");
                                    }
                                }
                            });
                    }
                }
            }
        }
        validateFormIndi = true;
        if ($('#ViewModel_BenMain_Type option:selected').text() == 'Individual') {
            if ($("#ViewModel_BenMain_Surname").val() == '') {
                $("#surNameError").removeClass("error").addClass("error_show");
                validateFormIndi = false;
            }
            else {
                $("#surNameError").addClass("error").removeClass("error_show");
                validateFormIndi = true;
            }
        }
        checkValidationFeeType();
        checkValidationType();
        checkValidationRelationship()
        checkValidationBenAcc();
        checkValidationAcc();
        checkValidationBenName();
        checkValidationBenCountry();
        checkValidationPortfolio();
        checkValidationOrderPortfolio();
        checkValidationBenPortfolio();
        checkValidationBenSwift();
        checkValidationBenIBAN();
        checkValidationBenAccInsSwift();
        checkValidationBenAccInsIBAN();
        $("#ViewModel_BackOfficePaymentFilePath").val("wwwroot/FeesFiles/BackOfficePaymentForm" + globalPath);
        $("#ViewModel_BenCCY").val(benCCY);
        $("#ViewModel_rate").val(benRate);
        $("#ViewModel_Portfolioval").val($("#ViewModel_Portfolio option:selected").text() && $("#ViewModel_Portfolio option:selected").text() != 'Select' ? $("#ViewModel_Portfolio option:selected").text() : $("#portfoliolist option:selected").text());
        $("#ViewModel_CCYVal").val($("#ViewModel_CCY option:selected").text());
        $("#ViewModel_Order_AccountVal").val($("#ViewModel_Order_Account option:selected").text());
        $("#ViewModel_BenMain_AccountVal").val($("#ViewModel_BenMain_Account option:selected").text());
        $("#ViewModel_BenMain_PortfolioVal").val($("#ViewModel_BenMain_Portfolio option:selected").text());
        $("#ViewModel_MoveTypeVal").val($("#MoveType option:selected").text());
        $("#ViewModel_HiddenCCY").val($("#ViewModel_CCY").val());
        $("#ViewModel_MessageId").val(messageId);

        if (PaymentId) {
            $("#ViewModel_PaymentId").val(PaymentId);
        }
        if ($('#ViewModel_Amount').val() == '0') {
            $('#ViewModel_Amount').keyup();
            e.preventDefault();
            return false;
        }

        if (($("#ViewModel_Checkbox1").prop('checked') == true) && ($("#ViewModel_Order_Swift").val().trim() == '' || $("#ViewModel_Order_IBAN").val().trim() == '')) {
            abp.notify.warn("You cannot send a swift without the swift code or iban");
        }
       
        if (validateForm === false || validateFormPortfolio === false || validateFormOrderPortfolio === false || validateFormBenPortfolio === false || validateFormBenSwift === false || validateFormBenAccInsSwift === false || validateFormBenAccInsIBAN === false || validateFormBenIBAN === false || validateFormBenAcc === false || validateFormAcc === false || validateFormBenCountry === false || validateFormBenName === false || validateFormFeeType === false || validateFormRel === false || validateFormBenType === false || validateFormIndi === false) {
            e.preventDefault();
            return false;
        }

    });
    $("#ViewModel_BenMain_Type").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorBenType'>Type: is required<span>")
    $(".errorBenType").hide();
    function checkValidationType() {
        $(".errorBenType").hide();
        if (check3rdPartyPayment == true && ($("#ViewModel_BenMain_Type").val() == '-1' || $("#ViewModel_BenMain_Type").val() == null)) {
            $(".errorBenType").show();
            validateFormBenType = false;
        }
        else {
            $(".errorBenType").hide();
            validateFormBenType = true;
        }
    }
    $("#ViewModel_BenMain_Relationship").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorBenrel'>Relationship: is required<span>")
    $(".errorBenrel").hide();
    function checkValidationRelationship() {
        $(".errorBenrel").hide();
        if (check3rdPartyPayment == true && ($("#ViewModel_BenMain_Relationship").val() == '-1' || $("#ViewModel_BenMain_Relationship").val() == null)) {
            $(".errorBenrel").show();
            validateFormRel = false;
        }
        else {
            $(".errorBenrel").hide();
            validateFormRel = true;
        }
    }
    $("#ViewModel_Portfolio").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorPortfolio'>Portfolio: is required<span>")
    $(".errorPortfolio").hide();
    function checkValidationPortfolio() {
        $(".errorPortfolio").hide();
        validateFormPortfolio = true;
        if ($('#MoveType option:selected').text() != 'Other Movements') {
            if ($("#ViewModel_Portfolio").val() == '-1') {
                $(".errorPortfolio").show();
                validateFormPortfolio = false;
            }
            else {
                $(".errorPortfolio").hide();
                validateFormPortfolio = true;
            }
        }
    }
    $("#portfoliolist").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorOrderPortfolio'>Portfolio: is required<span>")
    $(".errorOrderPortfolio").hide();
    function checkValidationOrderPortfolio() {
        $(".errorOrderPortfolio").hide();
        validateFormOrderPortfolio = true;
        if ($('#MoveType option:selected').text() == 'Other Movements' && ShowMessageDetails == false && orderTabShow == true) {
            if ($("#portfoliolist").val() == '-1') {
                $(".errorOrderPortfolio").show();
                validateFormOrderPortfolio = false;
            }
            else {
                $(".errorOrderPortfolio").hide();
                validateFormOrderPortfolio = true;
            }
        }
    }
    $("#ViewModel_BenMain_Portfolio").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorBenPortfolio'>Portfolio: is required<span>")
    $(".errorBenPortfolio").hide();
    function checkValidationBenPortfolio() {
        $(".errorBenPortfolio").hide();
        validateFormBenPortfolio = true;
        if ($('#MoveType option:selected').text() == 'Other Movements' && ShowMessageDetails == false && BenTabShow == true) {
            if ($("#ViewModel_BenMain_Portfolio").val() == '-1') {
                $(".errorBenPortfolio").show();
                validateFormBenPortfolio = false;
            }
            else {
                $(".errorBenPortfolio").hide();
                validateFormBenPortfolio = true;
            }
        }
    }
    $("#ViewModel_PaymentOthers").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorFeetype'>Fee Type: is required<span>")
    $(".errorFeetype").hide();
    function checkValidationFeeType() {
        $(".errorFeetype").hide();
        validateFormFeeType = true;
        if ($('#MoveType option:selected').text() == 'Other Movements') {
            if ($("#ViewModel_PaymentOthers").val() == '-1' || $("#ViewModel_PaymentOthers").val() == null) {
                $(".errorFeetype").show();
                validateFormFeeType = false;
            }
            else {
                $(".errorFeetype").hide();
                validateFormFeeType = true;
            }
        }
    }
    $("#ViewModel_BenMain_Account").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorBenAcc'>Account: is required<span>")
    $(".errorBenAcc").hide();
    function checkValidationBenAcc() {
        $(".errorBenAcc").hide();
        if (check3rdPartyPayment == false && $('#MoveType option:selected').text() != 'Other Movements') {
            if ($('#ViewModel_BenMain_Account option:selected').val() == null || $('#ViewModel_BenMain_Account option:selected').val() == '-1') {
                $(".errorBenAcc").show();
                validateFormBenAcc = false;
            }
            else {
                $(".errorBenAcc").hide();
                validateFormBenAcc = true;
            }
        } else {
            validateFormBenAcc = true;
        }
    }

    $("#ViewModel_Order_Account").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorAcc'>Account: is required<span>")
    $(".errorAcc").hide();
    function checkValidationAcc() {
        $(".errorAcc").hide();
        if ($('#MoveType option:selected').text() != 'Other Movements' && $('#MoveType option:selected').text() != 'External Receipt') {
            if ($('#ViewModel_Order_Account option:selected').val() == null || $('#ViewModel_Order_Account option:selected').val() == '-1') {
                $(".errorAcc").show();
                validateFormAcc = false;
            }
            else {
                $(".errorAcc").hide();
                validateFormAcc = true;
            }
        } else {
            validateFormAcc = true;
        }
    }

    $("#ViewModel_BenMain_Country").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorCountry'>Country: is required<span>")
    $(".errorCountry").hide();
    function checkValidationBenCountry() {
        $(".errorCountry").hide();
        if ($('#MoveType option:selected').text() != 'Other Movements') {
            if ($('#ViewModel_BenMain_Country option:selected').val() == null || $('#ViewModel_BenMain_Country option:selected').val() == '-1') {
                $(".errorCountry").show();
                validateFormBenCountry = false;
            }
            else {
                $(".errorCountry").hide();
                validateFormBenCountry = true;
            }
        } else {
            validateFormBenCountry = true;
        }
    }

    $("#ViewModel_BenMain_Name").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorBenName'>Name: is required<span>")
    $(".errorBenName").hide();
    function checkValidationBenName() {
        $(".errorBenName").hide();
        if (check3rdPartyPayment == false && $('#MoveType option:selected').text() != 'Other Movements') {
            if ($('#ViewModel_BenMain_Name').val() == "") {
                $(".errorBenName").show();
                validateFormBenName = false;
            }
            else {
                $(".errorBenName").hide();
                validateFormBenName = true;
            }
        } else {
            validateFormBenName = true;
        }
    }

    $("#ViewModel_BenDetailSendSWIFT_Swift57A").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorBenSwift'>Swift (57A): is required<span>")
    $(".errorBenSwift").hide();
    function checkValidationBenSwift() {
        $(".errorBenSwift").hide();
        validateFormBenSwift = true;
        if (ShowMessageDetails == false && $("#ViewModel_BenDetailAcc_Swift56A").val() == "" && BenTabShow == true) {
            if ($("#ViewModel_BenDetailSendSWIFT_Swift57A").val() == '') {
                $(".errorBenSwift").show();
                validateFormBenSwift = false;
            }
            else {
                $(".errorBenSwift").hide();
                validateFormBenSwift = true;
            }
        }
    }

    $("#ViewModel_BenDetailSendSWIFT_IBAN59").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorBenIBAN'>IBAN (59): is required<span>")
    $(".errorBenIBAN").hide();
    function checkValidationBenIBAN() {

        $(".errorBenIBAN").hide();
        validateFormBenIBAN = true;
        if (ShowMessageDetails == false && $("#ViewModel_BenDetailAcc_IBAN57A").val() == "" && BenTabShow == true) {

            if ($("#ViewModel_BenDetailSendSWIFT_IBAN59").val() == '') {
                $(".errorBenIBAN").show();
                validateFormBenIBAN = false;
            }
            else {
                $(".errorBenIBAN").hide();
                validateFormBenIBAN = true;
            }
        }
    }

    $("#ViewModel_BenDetailAcc_Swift56A").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorBenAccInsSwift'>Swift (57A): is required<span>")
    $(".errorBenAccInsSwift").hide();
    function checkValidationBenAccInsSwift() {
        $(".errorBenAccInsSwift").hide();
        validateFormBenAccInsSwift = true;
        if (ShowMessageDetails == false && $("#ViewModel_BenDetailSendSWIFT_Swift57A").val() == "" && BenTabShow == true) {
            if ($("#ViewModel_BenDetailAcc_Swift56A").val() == '') {
                $(".errorBenAccInsSwift").show();
                validateFormBenAccInsSwift = false;
            }
            else {
                $(".errorBenAccInsSwift").hide();
                validateFormBenAccInsSwift = true;
            }
        }
    }

    $("#ViewModel_BenDetailAcc_IBAN57A").parent()[0].insertAdjacentHTML("beforeend", "<span class = 'text-danger errorBenAccInsIBAN'>Swift (57A): is required<span>")
    $(".errorBenAccInsIBAN").hide();
    function checkValidationBenAccInsIBAN() {
        $(".errorBenAccInsIBAN").hide();
        validateFormBenAccInsIBAN = true;
        if (ShowMessageDetails == false && $("#ViewModel_BenDetailSendSWIFT_IBAN59").val() == "" && BenTabShow == true) {
            if ($("#ViewModel_BenDetailAcc_IBAN57A").val() == '') {
                $(".errorBenAccInsIBAN").show();
                validateFormBenAccInsIBAN = false;
            }
            else {
                $(".errorBenAccInsIBAN").hide();
                validateFormBenAccInsIBAN = true;
            }
        }
    }

    $('#ViewModel_Order_IBAN').keyup(function (e) {
        if (e.target.value.trim() != '') {
            if (ValidIBANConvertAndConfirm(e.target.value) == true) {
                $('.symboleOk').show();
                $('.symboleRemove').hide();
            }
            else {
                $('.symboleOk').hide();
                $('.symboleRemove').show();
            }
        }
        else {
            $('.symboleOk').hide();
            $('.symboleRemove').hide();
        }
    });

    $("#ViewModel_BenDetailSendSWIFT_IBAN59").keyup(function (e) {
        $('.symboleRemoveBenDetail').hide();
        if (e.target.value.trim() != "") {
            if (ValidIBANConvertAndConfirm(e.target.value) == true) {
                $('.symboleOkBenDetail').show();
                $('.symboleRemoveBenDetail').hide();
            }
            else {
                $('.symboleOkBenDetail').hide();
                $('.symboleRemoveBenDetail').show();
            }
        } else {
            $('.symboleOkBenDetail').hide();
            $('.symboleRemoveBenDetail').hide();
        }
    });

    $("#ViewModel_BenDetail_IBAN57A").keyup(function (e) {
        if (e.target.value.trim() != '') {
            if (ValidIBANConvertAndConfirm(e.target.value) == true) {
                $('.symboleOkBenDetailInter').show();
                $('.symboleRemoveBenDetailInter').hide();
            }
            else {
                $('.symboleOkBenDetailInter').hide();
                $('.symboleRemoveBenDetailInter').show();
            }
        }
        else {
            $('.symboleOkBenDetailInter').hide();
            $('.symboleRemoveBenDetailInter').hide();
        }
    });

    $("#ViewModel_BenDetailAcc_IBAN57A").keyup(function (e) {
        if (e.target.value.trim() != '') {
            if (ValidIBANConvertAndConfirm(e.target.value) == true) {
                $('.symboleOkBenDetailAcc').show();
                $('.symboleRemoveBenDetailAcc').hide();
            }
            else {
                $('.symboleOkBenDetailAcc').hide();
                $('.symboleRemoveBenDetailAcc').show();
            }
        }
        else {
            $('.symboleOkBenDetailAcc').hide();
            $('.symboleRemoveBenDetailAcc').hide();
        }
    });

    function ValidIBANConvertAndConfirm(varIBAN) {
        var ValidIBAN = true;
        if (varIBAN != null && varIBAN.trim().length != 0) {
            {
                try {
                    var varFirstLetter = varIBAN.substring(0, 1).toUpperCase();
                    var varSecondLetter = varIBAN.substring(1, 2).toUpperCase();
                    var JoinLetter = (varFirstLetter + varSecondLetter);

                    if (varIBAN.indexOf(' ') >= 0) {
                        ValidIBAN = false;
                    }
                    else if (((varIBAN.length < 15) || (varIBAN.length > 32))) {
                        ValidIBAN = false;
                    }
                    else if (((varFirstLetter.charCodeAt(0) < 65)
                        || (varFirstLetter.charCodeAt(0) > 90))) {
                        ValidIBAN = false;
                    }
                    else if (((varSecondLetter.charCodeAt(0) < 65)
                        || (varSecondLetter.charCodeAt(0) > 90))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "AX")
                        && (varIBAN.length != 18))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "AL")
                        && (varIBAN.length != 28))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "AD")
                        && (varIBAN.length != 24))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "AT")
                        && (varIBAN.length != 20))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "BE")
                        && (varIBAN.length != 16))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "BA")
                        && (varIBAN.length != 20))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "BG")
                        && (varIBAN.length != 22))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "HR")
                        && (varIBAN.length != 21))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "CY")
                        && (varIBAN.length != 28))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "CZ")
                        && (varIBAN.length != 24))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "DK")
                        && (varIBAN.length != 18))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "EE")
                        && (varIBAN.length != 20))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "FO")
                        && (varIBAN.length != 18))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "FI")
                        && (varIBAN.length != 18))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "FR")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "DE")
                        && (varIBAN.length != 22))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "GI")
                        && (varIBAN.length != 23))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "GR")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "GL")
                        && (varIBAN.length != 18))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "HU")
                        && (varIBAN.length != 28))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "IS")
                        && (varIBAN.length != 26))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "IE")
                        && (varIBAN.length != 22))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "IT")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "XK")
                        && (varIBAN.length != 20))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "LV")
                        && (varIBAN.length != 21))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "LI")
                        && (varIBAN.length != 21))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "LT")
                        && (varIBAN.length != 20))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "LU")
                        && (varIBAN.length != 20))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "MK")
                        && (varIBAN.length != 19))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "MT")
                        && (varIBAN.length != 31))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "MD")
                        && (varIBAN.length != 24))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "MC")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "ME")
                        && (varIBAN.length != 22))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "NL")
                        && (varIBAN.length != 18))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "NO")
                        && (varIBAN.length != 15))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "PL")
                        && (varIBAN.length != 28))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "PT")
                        && (varIBAN.length != 25))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "RO")
                        && (varIBAN.length != 24))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "SM")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "RS")
                        && (varIBAN.length != 22))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "SK")
                        && (varIBAN.length != 24))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "SI")
                        && (varIBAN.length != 19))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "ES")
                        && (varIBAN.length != 24))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "SE")
                        && (varIBAN.length != 24))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "CH")
                        && (varIBAN.length != 21))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "GB")
                        && (varIBAN.length != 22))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "AZ")
                        && (varIBAN.length != 28))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "BH")
                        && (varIBAN.length != 22))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "BR")
                        && (varIBAN.length != 29))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "VG")
                        && (varIBAN.length != 24))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "CR")
                        && (varIBAN.length != 21))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "DO")
                        && (varIBAN.length != 28))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "GF")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "PF")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "TF")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "GE")
                        && (varIBAN.length != 22))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "GP")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "GU")
                        && (varIBAN.length != 28))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "IL")
                        && (varIBAN.length != 23))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "JO")
                        && (varIBAN.length != 30))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "KZ")
                        && (varIBAN.length != 20))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "KW")
                        && (varIBAN.length != 30))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "LB")
                        && (varIBAN.length != 28))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "MQ")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "MR")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "MU")
                        && (varIBAN.length != 30))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "YT")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "NC")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "PK")
                        && (varIBAN.length != 24))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "PS")
                        && (varIBAN.length != 29))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "QA")
                        && (varIBAN.length != 29))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "RE")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "BL")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "LC")
                        && (varIBAN.length != 32))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "MF")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "PM")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "ST")
                        && (varIBAN.length != 25))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "SA")
                        && (varIBAN.length != 24))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "TL")
                        && (varIBAN.length != 23))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "TN")
                        && (varIBAN.length != 24))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "TR")
                        && (varIBAN.length != 26))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "UA")
                        && (varIBAN.length != 29))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "AE")
                        && (varIBAN.length != 23))) {
                        ValidIBAN = false;
                    }
                    else if (((JoinLetter == "WF")
                        && (varIBAN.length != 27))) {
                        ValidIBAN = false;
                    }
                }
                catch (ex) {
                    ValidIBAN = false;
                }
            }
        }
        else {
            ValidIBAN = false;
            $('.symboleOk').hide();
            $('.symboleRemove').hide();
            $('.symboleOkBenDetail').hide();
            $('.symboleRemoveBenDetail').hide();
        }
        return ValidIBAN;
    }

    function formatBalance(x) {
        return parseFloat(x).toLocaleString()
    }

    $('#ViewModel_sfiwtcodeIntermediaryBank').keyup(function (e) {
        if (e.target.value.length) {
            if (e.target.value.length == 8 || e.target.value.length == 11) {
                $('.symboleOkInterm').show();
                $('.symboleRemoveInterm').hide();
                $('#validatSwiftIntermediaryBank').attr('disabled', false);

            } else {
                $('.symboleOkInterm').hide();
                $('.symboleRemoveInterm').show();
                $('#validatSwiftIntermediaryBank').attr('disabled', true);
                $('.symboleOkInterm').hide();
                $('.symboleRemove').hide();
                $('#ViewModel_BenDetail_IntermediaryBankSwiftCode').val("");
                $('#ViewModel_BenDetail_IntermediaryBank').val("");
                $('#ViewModel_BenDetail_IntermediaryBankSwiftCode').val("");
                $('#ViewModel_BenDetail_Address').val("");
                $('#ViewModel_BenDetail_Swift56A').val("");
                $('#ViewModel_BenDetail_City').val("");
                $('#ViewModel_BenDetail_Zip').val("");
                $('#ViewModel_BenDetail_Other').val("");
            }
        }
        else {
            $('.symboleOkInterm').hide();
            $('.symboleRemoveInterm').hide();
        }
    });

    $('#validatSwiftIntermediaryBank').on('click', function () {
        $('.symboleOkInterm').hide();
        $('.symboleRemoveInterm').hide();
        var swiftCode = $('#ViewModel_sfiwtcodeIntermediaryBank').val()
        indo.backOfficeFOPs.backOfficeFOP.fetchSwiftCodes(-1, swiftCode)
            .then(function (data) {
                if (data.length != 0) {
                    $('.symboleOkInterm').show();
                    $('.symboleRemoveInterm').hide();
                    $('#ViewModel_BenDetail_IntermediaryBankSwiftCode').val(data[0].sC_BankName);
                    $('#ViewModel_BenDetail_IntermediaryBank').val(data[0].sC_SwiftCode);
                    $('#ViewModel_BenDetail_Address').val(data[0].sC_Address1);
                    $('#ViewModel_BenDetail_Swift56A').val(data[0].sC_SwiftCode);
                    $('#ViewModel_BenDetail_City').val(data[0].sC_City);
                    $('#ViewModel_BenDetail_Zip').val(data[0].sC_PostCode);
                    $('#ViewModel_BenDetail_Other').val(data[0].swiftOtherMsg);
                }
                else {
                    $('.symboleOkInterm').hide();
                    $('.symboleRemoveInterm').show();
                    $('#ViewModel_BenDetail_IntermediaryBankSwiftCode').val("");
                    $('#ViewModel_BenDetail_Address').val("");
                    $('#ViewModel_BenDetail_Swift56A').val("");
                    $('#ViewModel_BenDetail_City').val("");
                    $('#ViewModel_BenDetail_Zip').val("");
                    $('#ViewModel_BenDetail_Other').val("");
                }
            })
    });

    $('#ViewModel_BenDetailAcc_AccountBankNameSwiftCodes').keyup(function (e) {
        if (e.target.value.length) {
            if (e.target.value.length == 8 || e.target.value.length == 11) {
                $('.symboleOk_').show();
                $('.symboleRemove_').hide();
                $('#validatSwiftInstitution').attr('disabled', false);

            }
            else {
                $('#validatSwiftInstitution').attr('disabled', true);
                $('.symboleOk_').hide();
                $('.symboleRemove_  ').show();
                $('#ViewModel_BenDetailAcc_AccountBankNameSwiftCode').val("");
                $('#ViewModel_BenDetailAcc_AccountBankName').val("");
                $('#ViewModel_BenDetailAcc_Address').val("");
                $('#ViewModel_BenDetailAcc_Swift56A').val("");
                $('#ViewModel_BenDetailAcc_City').val("");
                $('#ViewModel_BenDetailAcc_Zip').val("");
                $('#ViewModel_BenDetailAcc_Other').val("");
            }
        }
        else {
            $('.symboleOk_').hide();
            $('.symboleRemove_').hide();
        }
    });

    $('#validatSwiftInstitution').on('click', function () {
        $('.symboleOk_').hide();
        $('.symboleRemove_  ').hide();
        var swiftCode = $('#ViewModel_BenDetailAcc_AccountBankNameSwiftCodes').val()
        indo.backOfficeFOPs.backOfficeFOP.fetchSwiftCodes(-1, swiftCode)
            .then(function (data) {
                if (data.length != 0) {
                    $('.symboleOk_').show();
                    $('.symboleRemove_').hide();
                    $('#ViewModel_BenDetailAcc_AccountBankNameSwiftCode').val(data[0].sC_BankName);
                    $('#ViewModel_BenDetailAcc_AccountBankName').val(data[0].sC_SwiftCode);

                    $('#ViewModel_BenDetailAcc_Address').val(data[0].sC_Address1);
                    $('#ViewModel_BenDetailAcc_Swift56A').val(data[0].sC_SwiftCode);
                    $('#ViewModel_BenDetailAcc_City').val(data[0].sC_City);
                    $('#ViewModel_BenDetailAcc_Zip').val(data[0].sC_PostCode);
                    $('#ViewModel_BenDetailAcc_Other').val(data[0].swiftOtherMsg);
                }
                else {
                    $('.symboleOk_').hide();
                    $('.symboleRemove_  ').show();
                    $('#ViewModel_BenDetailAcc_AccountBankNameSwiftCode').val("");
                    $('#ViewModel_BenDetailAcc_Address').val("");
                    $('#ViewModel_BenDetailAcc_Swift56A').val("");
                    $('#ViewModel_BenDetailAcc_City').val("");
                    $('#ViewModel_BenDetailAcc_Zip').val("");
                    $('#ViewModel_BenDetailAcc_Other').val("");
                }
            })
    });

    function loadDropDowns() {
        indo.backOfficeFOPs.backOfficeFOP.fetchPaymentHomeAddress("")
            .then(function (data) {
                if (data != null) {
                    ClientSwiftCode = data.swiftCode;
                    paymentHomeAddressData = data;
                }
            });
    }

    function triggerCashAcc() {
        var ccyVal = $('#ViewModel_CCY option:selected').text();
        $('#ViewModel_Order_Account option').remove();
        var portfolioVal
        if ($('#MoveType option:selected').text() == 'Other Movements') {
            portfolioVal = $('#portfoliolist').val();
        } else {
            portfolioVal = $('#ViewModel_Portfolio').val();
        }

        indo.backOfficeFOPs.backOfficeFOP.fetchPaymentSelectAccount(portfolioVal)
            .then(function (data) {
                if (data != null) {
                    paymentSelectAccountData = data;
                    var cashAccData = data.filter(x => x.cR_Name1 == ccyVal).map(({ sA_ID, t_Name1 }) => ({ id: sA_ID, text: t_Name1 }));
                    cashAccData.unshift({ id: -1, text: 'Select' });
                    $("#ViewModel_Order_Account").select2({
                        data: cashAccData,
                        placeholder: 'Select',
                    });
                    populateAcc();
                }
            })
    }

    function triggerCashAccBen() {
        var ccyVal = $('#ViewModel_CCY option:selected').text();
        $('#ViewModel_BenMain_Account option').remove();
        var portfolioVal
        if ($('#MoveType option:selected').text() == 'Other Movements') {
            portfolioVal = $('#ViewModel_BenMain_Portfolio').val();
        } else {
            portfolioVal = $('#ViewModel_Portfolio').val();
        }

        indo.backOfficeFOPs.backOfficeFOP.fetchPaymentSelectAccount(portfolioVal)
            .then(function (data) {
                if (data != null) {
                    paymentSelectAccountData = data;
                    var cashAccData = data.filter(x => x.cR_Name1 == ccyVal).map(({ sA_ID, t_Name1 }) => ({ id: sA_ID, text: t_Name1 }));
                    cashAccData.unshift({ id: -1, text: 'Select' });
                    $("#ViewModel_BenMain_Account").select2({
                        data: cashAccData,
                        placeholder: 'Select',
                    });
                    populateAcc();
                }
            })
    }

    var selectedGridData;

    function populateAcc() {
        if (PaymentId != null) {
            indo.backOfficePayment.backOfficePayment.fetchPaymentGridView(PaymentId)
                .then(function (data) {
                    if (data != null) {
                        var selectedGridData = data;
                        $("#ViewModel_Order_Account").val(selectedGridData[0].p_OrderAccountID).trigger("change");
                        if ($('#MoveType option:selected').text() != 'External Client Payment' && $('#MoveType option:selected').text() != 'External Payment') {
                            $("#ViewModel_BenMain_Account").val(selectedGridData[0].p_BenAccountID).trigger("change");
                        }
                        $("#loader").hide();
                        $(".card-body").removeClass("blurContent");
                        showHideBenBusiness();
                    }
                });
        }
    }

    //function populateTemplate() {        
    //    if (PaymentId != null) {
    //        indo.backOfficePayment.backOfficePayment.fetchPaymentGridView(PaymentId)
    //            .then(function (data) {
    //                if (data[0].p_BenTemplateName) {
    //                    var selectedTempVal = templateList.filter(x => x.pa_TemplateName == data[0].p_BenTemplateName);
    //                    if (selectedTempVal != null && selectedTempVal != "") {
    //                        $("#ViewModel_BenMain_Template").val(selectedTempVal[0].pa_ID).trigger("change");
    //                    }
    //                }
    //            });
    //    }
    //}

    function validateDate(userDate) {
        var userFormattedDate = new Date(userDate);
        var dateString =
            userFormattedDate.getUTCFullYear() + "/" +
            ("0" + (userFormattedDate.getUTCMonth() + 1)).slice(-2) + "/" +
            ("0" + userFormattedDate.getUTCDate()).slice(-2) + " " +
            ("0" + userFormattedDate.getUTCHours()).slice(-2) + ":" +
            ("0" + userFormattedDate.getUTCMinutes()).slice(-2) + ":" +
            ("0" + userFormattedDate.getUTCSeconds()).slice(-2);
        return dateString;
    }

    $('#files-tab').click(function (e) {
        manageDirectory();
        manageDirectory();
    });

    function clearAccountSelection() {
        $(".accounntSelectionShowHide").removeClass("disabledbutton");
        $("#ViewModel_Order_Name").val("");
        $('#ViewModel_Order_Address1').val("");
        $('#ViewModel_Order_City').val("");
        $('#ViewModel_Order_PostZip').val("");
        $('#ViewModel_Order_Country option:selected').text("");
        $('#ViewModel_Order_Swift').val("");
        $('#ViewModel_Order_Swift').keyup();
        $('#ViewModel_Order_IBAN').val("");
        $('#ViewModel_Order_IBAN').keyup();
        $("#ViewModel_Order_Country").val("").trigger("change");
        $("#ViewModel_Order_Account").val("").trigger("change");
    }

    function clearBenAccSelection() {
        $("#ViewModel_BenMain_Name").val("");
        $('#ViewModel_BenMain_Address').val("");
        $('#ViewModel_BenMain_City').val("");
        $('#ViewModel_BenMain_PostZip').val("");
        $('#ViewModel_BenMain_Country').val("").trigger("change");
        //$('#ViewModel_BenDetailSendSWIFT_Swift57A').val("");
        //$('#ViewModel_BenDetailSendSWIFT_IBAN59').val("");
        $("#ViewModel_BenMain_Account").val("").trigger("change");
        $(".FieldsBenAccShowHide").removeClass("disabledbutton");
    }

    function clearBenTempSelection() {
        $(".FieldsTempShowHide").removeClass("disabledbutton");
        $(".FieldsBenAccShowHide").removeClass("disabledbutton");
        $("#ViewModel_BenMain_Type").val("").trigger("change");
        $("#ViewModel_BenMain_Relationship").val("").trigger("change");
        $('#ViewModel_BenMain_FirstName').val("");
        $('#ViewModel_BenMain_MiddleName').val("");
        $('#ViewModel_BenMain_Surname').val("");
        $('#ViewModel_BenMain_Address').val("");
        $('#ViewModel_BenMain_City').val("");
        $('#ViewModel_BenMain_County').val("");
        $('#ViewModel_BenMain_PostZip').val("");
        $('#ViewModel_BenMain_Country').val("").trigger("change");
        $("#ViewModel_BenDetailSendSWIFT_Swift57A").val("");
        $('#ViewModel_BenDetailSendSWIFT_Swift57A').keyup();
        $("#ViewModel_BenDetailSendSWIFT_IBAN59").val("");
        $("#ViewModel_BenDetailSendSWIFT_IBAN59").keyup();
        $("#ViewModel_BenDetail_Address").val("");
        $("#ViewModel_BenDetail_Swift56A").val("");
        $("#ViewModel_BenDetail_City").val("");
        $("#ViewModel_BenDetail_IBAN57A").val("");
        $("#ViewModel_BenDetail_IBAN57A").keyup();
        $("#ViewModel_BenDetail_Zip").val("");
        $("#ViewModel_BenDetail_Other").val("");
        $("#ViewModel_BenDetailAcc_Address").val("");
        $("#ViewModel_BenDetailAcc_Swift56A").val("");
        $("#ViewModel_BenDetailAcc_City").val("");
        $("#ViewModel_BenDetailAcc_IBAN57A").val("");
        $("#ViewModel_BenDetailAcc_IBAN57A").keyup();
        $("#ViewModel_BenDetailAcc_Zip").val("");
        $("#ViewModel_BenDetailAcc_Other").val("");
        clearBenBankDetailData();
        clearBenIntermediaryData();

    }
    $('#btnNewTrans').on('click', function () {
        if (PaymentId) { window.location.href = '/BackOfficePayment'; }
        else { clearFields(); }
    });

    function clearFields() {
        $('#ViewModel_BrkrAcc').val("");
        $('#ViewModel_Clearer').val("");
        $('#ViewModel_PlaceOfSettle').val("");
        $('#ViewModel_DelivAgent').val("");
        $('#ViewModel_sfiwtcodeClearer').val("");
        $('#ViewModel_sfiwtcodeBrkrAcc').val("");
        $('#ViewModel_sfiwtcodePlaceOfSettle').val("");
        $('#ViewModel_sfiwtcode').val("");
        $('#ViewModel_PlaceOfSettleSwiftCode').val("");
        $('#ViewModel_DelivAgentSwiftCode').val("");
        $('#ViewModel_ClearerSwiftCode').val("");
        $('#ViewModel_BrkrAccSwiftCode').val("");
        $('#ViewModel_Amount').val("0");
        $('#ViewModel_DelAgtAccNo').val("");
        $('#ViewModel_InstructionsId').val("");
        $("#ViewModel_TransDate").val(getTodate());
        $("#ViewModel_SettleDate").val(getTodate());
        $('#MoveType').val('-1').trigger("change");
        $('#ViewModel_CCY').val('-1').trigger("change");
        $('#ViewModel_Portfolio').val('-1').trigger("change");
        $('#ViewModel_Instrument').val('-1').trigger("change");
        $('#ViewModel_CashAcc').val('-1').trigger("change");
        $("#ViewModel_Comments").val("");
        $("#ViewModel_BenMain_Order_Ref").val("");
        $('#ViewModel_BenMain_Template').val('-1').trigger("change");
        $("#ordertTab-tab").click();
        $("#ViewModel_sendTo").val("");
        $("#ViewModel_relatedRef").val("");
        $('#ViewModel_sendTo').keyup();
    };

    function getTodate() {
        var today = new Date();
        var yyyy = today.getFullYear();
        let mm = today.getMonth() + 1;
        let dd = today.getDate();
        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;
        return today = yyyy + '-' + mm + '-' + dd;
    }

    function addDecimal(amt) {
        var formatAmt;
        if (!amt.includes(',')) {
            formatAmt = parseFloat(amt).toLocaleString();
        }
        else {
            formatAmt = parseFloat(amt.replaceAll(',', "")).toLocaleString();
        }
        if (!amt.includes('.')) {
            $('#balance').text(formatAmt + '.00');
        } else {
            var modifyAmt = parseFloat(formatAmt.replaceAll(',', "")).toFixed(2)
            $('#balance').text(parseFloat(modifyAmt).toLocaleString());
        }
    }

    function addDecimalBen(amt) {
        var formatAmt;
        if (!amt.includes(',')) {
            formatAmt = parseFloat(amt).toLocaleString();
        }
        else {
            formatAmt = parseFloat(amt.replaceAll(',', "")).toLocaleString();
        }
        if (!amt.includes('.')) {
            $('#balanceBen').text(formatAmt + '.00');
        } else {
            var modifyAmt = parseFloat(formatAmt.replaceAll(',', "")).toFixed(2)
            $('#balanceBen').text(parseFloat(modifyAmt).toLocaleString());
        }
    }

    $('#requestTab-tab').on('click', function () {
        populateReqTab();
    });

    function populateReqTab() {
        if (PaymentId != null) {
            $('#requestTabPREmail').text("");
            indo.backOfficePayment.backOfficePayment.fetchPaymentGridView(PaymentId)
                .then(function (data) {
                    if (data != null) {
                        if (ShowMessageDetails == false) {
                            $('#reqTabContent').hide();
                            $('#requestTabPREmail').text(data[0].pR_Email);
                        } else {
                            $('#requestTabPREmail').text("");
                            $('#reqTabContent').show();
                            $('#ViewModel_sendTo').val(data[0].p_OrderOther);
                            $('#ViewModel_relatedRef').val(data[0].p_IMSRef);
                            $('#ViewModel_sendTo').keyup();
                        }
                    }
                });
        }
    }

    $('#ViewModel_sendTo').keyup(function (e) {
        if (e.target.value.trim() != "") {
            if (e.target.value.length == 8 || e.target.value.length == 11) {
                $('.symboleOkSendTo').show();
                $('.symboleRemoveSendTo').hide();

            } else {
                $('.symboleOkSendTo').hide();
                $('.symboleRemoveSendTo').show();
            }
        } else {
            $('.symboleOkSendTo').hide();
            $('.symboleRemoveSendTo').hide();
        }
    });

    prepopulateFields();

    function prepopulateFields() {
        if (PaymentId != null) {
            indo.backOfficePayment.backOfficePayment.fetchPaymentGridView(PaymentId)
                .then(function (data) {
                    if (data != null) {
                        selectedGridData = data;
                        $("#MoveType").val(selectedGridData[0].p_PaymentTypeID).trigger("change");
                        if ($("#ViewModel_Portfolio").val() == '-1' || $("#ViewModel_Portfolio").val() == null) {
                            $("#ViewModel_Portfolio, #portfoliolist").val(selectedGridData[0].p_PortfolioID).trigger("change");
                        }
                        $("#ViewModel_BenMain_Portfolio").val(selectedGridData[0].p_BenPortfolioID).trigger("change");
                        if ($("#ViewModel_CCY").val() == '-1') {
                            $("#ViewModel_CCY").val(selectedGridData[0].p_CCYID).trigger("change");
                        }
                        if ($("#ViewModel_Amount").val() == '0') {
                            $("#ViewModel_Amount").val(selectedGridData[0].p_Amount);
                        }

                        $('#ViewModel_Checkbox1').prop('checked', selectedGridData[0].p_SendSwift);
                        $('#ViewModel_Checkbox2').prop('checked', selectedGridData[0].p_SendIMS);
                        $("#ViewModel_Comments").val(selectedGridData[0].p_Comments);
                        $("#ViewModel_Order_Account").val(selectedGridData[0].p_OrderAccountID).trigger("change");
                        $("#ViewModel_Order_Name").val(selectedGridData[0].p_OrderName);
                        $("#ViewModel_Order_Address1").val(selectedGridData[0].p_OrderAddress1);
                        $("#ViewModel_Order_City").val(selectedGridData[0].p_OrderAddress2);
                        $("#ViewModel_Order_PostZip").val(selectedGridData[0].p_OrderZip);
                        $("#ViewModel_Order_Country").val(selectedGridData[0].p_OrderCountryID).trigger("change");
                        $("#ViewModel_Order_Ref").val(selectedGridData[0].p_OrderRef);
                        $("#ViewModel_Order_Swift").val(selectedGridData[0].p_OrderSwift);
                        $('#ViewModel_Order_Swift').keyup();
                        $("#ViewModel.Order_IBAN").val(selectedGridData[0].p_OrderIBAN);
                        $('#ViewModel_Order_IBAN').keyup();
                        $("#ViewModel_Order_Other").val(selectedGridData[0].p_OrderOther);
                        $("#ViewModel_BenMain_Account").val(selectedGridData[0].p_BenAccountID).trigger("change");
                        $("#ViewModel_BenMain_Name").val(selectedGridData[0].p_BenName);
                        $("#ViewModel_BenMain_Type").val(selectedGridData[0].p_BenTypeID).trigger("change");
                        $("#ViewModel_BenMain_Address").val(selectedGridData[0].p_BenAddress1);
                        $("#ViewModel_BenMain_County").val(selectedGridData[0].p_BenCounty);
                        $("#ViewModel_BenMain_Order_Ref").val(selectedGridData[0].p_BenRef);
                        $("#ViewModel_BenMain_Relationship").val(selectedGridData[0].p_BenRelationshipID).trigger("change");
                        $("#ViewModel_BenMain_City").val(selectedGridData[0].p_BenAddress2);
                        $("#ViewModel_BenMain_PostZip").val(selectedGridData[0].p_BenZip);
                        $("#ViewModel_BenMain_Country").val(selectedGridData[0].p_BenCountryID).trigger("change");
                        $("#ViewModel_BenDetailSendSWIFT_Swift57A").val(selectedGridData[0].p_BenSwift);
                        $('#ViewModel_BenDetailSendSWIFT_Swift57A').keyup();
                        $("#ViewModel_BenDetailSendSWIFT_IBAN59").val(selectedGridData[0].p_BenIBAN);
                        $("#ViewModel_BenDetailSendSWIFT_IBAN59").keyup();
                        $("#ViewModel_BenDetailSendSWIFT_Other").val(selectedGridData[0].p_BenOther);
                        $("#ViewModel_PaymentOthers").val(selectedGridData[0].p_Other_ID).trigger("change");
                        $("#ViewModel_TransDate").val(selectedGridData[0].p_TransDate.substr(0, 10).split("/").reverse().join("-"));
                        $("#ViewModel_SettleDate").val(selectedGridData[0].p_SettleDate.substr(0, 10).split("/").reverse().join("-"));
                        $("#ViewModel_BenMain_BusinessName").val(selectedGridData[0].p_BenName);
                        $("#ViewModel_BenMain_FirstName").val(selectedGridData[0].p_BenFirstname);
                        $("#ViewModel_BenMain_MiddleName").val(selectedGridData[0].p_BenMiddlename);
                        $("#ViewModel_BenMain_Surname").val(selectedGridData[0].p_BenSurname);
                        $("#ViewModel_Amount").focusout();
                        $('#ViewModel_Amount').keyup();
                        $("#ViewModel_BenDetailAcc_IntermediaryBankSwiftCode").val(selectedGridData[0].p_BenBankName);
                        $("#ViewModel_BenDetailAcc_Address").val(selectedGridData[0].p_BenBankAddress1);
                        $("#ViewModel_BenDetailAcc_City").val(selectedGridData[0].p_BenBankAddress2);
                        $("#ViewModel_BenDetailAcc_Zip").val(selectedGridData[0].p_BenBankZip);
                        $("#ViewModel_BenDetailAcc_Swift56A").val(selectedGridData[0].p_BenBankSwift);
                        $("#ViewModel_BenDetailAcc_IBAN57A").val(selectedGridData[0].p_BenBankIBAN);
                        $("#ViewModel_BenDetailAcc_IBAN57A").keyup();
                        $("#ViewModel_BenDetailAcc_Other").val(selectedGridData[0].p_BenBankOther);
                        $("#ViewModel_BenDetail_IntermediaryBankSwiftCode").val(selectedGridData[0].p_BenSubBankName);
                        $("#ViewModel_BenDetail_Address").val(selectedGridData[0].p_BenSubBankAddress1);
                        $("#ViewModel_BenDetail_City").val(selectedGridData[0].p_BenSubBankAddress2);
                        $("#ViewModel_BenDetail_Zip").val(selectedGridData[0].p_BenSubBankZip);
                        $("#ViewModel_BenDetail_Swift56A").val(selectedGridData[0].p_BenSubBankSwift);
                        $("#ViewModel_BenDetail_IBAN57A").val(selectedGridData[0].p_BenSubBankIBAN);
                        $("#ViewModel_BenDetail_IBAN57A").keyup();
                        $("#ViewModel_BenDetail_Other").val(selectedGridData[0].p_BenSubBankOther);
                        $("#ViewModel_BenInter_Country").val(selectedGridData[0].p_BenSubBankCountryID).trigger("change");
                        $("#ViewModel_BenAcc_Country").val(selectedGridData[0].p_BenBankCountryID).trigger("change");
                        $("#ViewModel_BenDetailAcc_AccountBankNameSwiftCode").val(selectedGridData[0].p_BenBankName);
                    }
                });
        }
    }

    function showHideBenBusiness() {
        if ($('#ViewModel_BenMain_Type option:selected').text() == 'Business') {
            $('#businessName').show();
            $('#individualName').hide();
        }
        else if ($('#ViewModel_BenMain_Type option:selected').text() == 'Select') {
            $('#businessName').show();
            $('#individualName').show();
        }
        else {
            $('#businessName').hide();
            $('#individualName').show();
        }
    }
})

