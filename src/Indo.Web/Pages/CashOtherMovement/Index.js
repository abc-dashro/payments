﻿$(function () {
    /* Localization */
    var l = abp.localization.getResource('Indo');

    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes();
    var dateTime = date + '_' + time;

    /* load grid once */
    loadGrid();

    /* Syncfusion */
    ej.base.enableRipple(window.ripple);
    var grid = new ej.grids.Grid({
        enableInfiniteScrolling: false,
        allowPaging: true,
        pageSettings: { currentPage: 1, pageSize: 100, pageSizes: ["10", "20", "100", "200", "All"] },
        allowGrouping: true,
        groupSettings: { columns: ['p_Status'] },
        allowSorting: true,
        allowFiltering: true,
        filterSettings: { type: 'Excel' },
        allowSelection: true,
        selectionSettings: { persistSelection: true, type: 'Single' },
        enableHover: true,
        allowExcelExport: true,
        allowPdfExport: true,
        allowTextWrap: false,
        toolbar: [
            'ExcelExport', 'Search', { type: 'Separator' }, 'PdfExport',
            { type: 'Separator' },
            { text: 'Help', tooltipText: 'Help', id: 'HelpCustom' },
        ],
        columns: [
            { type: 'checkbox', width: 10 },
            {
                field: 'p_ID', isPrimaryKey: true, headerText: 'ID', textAlign: 'right', width: 10
            },
            {
                field: 'p_PaymentTypeID', headerText: 'Payment Type Id', visible: false
            },
            {
                field: 'p_Status', headerText: 'Status', textAlign: 'Left', width: 30
            },
            {
                field: 'p_SentApproveEmail', headerText: 'Email Sent', textAlign: 'center', width: 20
            },
            {
                field: 'p_SendSwift', headerText: 'Send Swift', textAlign: 'center', width: 20
            },
            {
                field: 'p_SwiftAcknowledged', headerText: 'Swift Ack', textAlign: 'center', width: 20
            },
            {
                field: 'p_SendIMS', headerText: 'Send IMS', textAlign: 'center', width: 170
            },
            {
                field: 'p_IMSAcknowledged', headerText: 'IMS Ack', textAlign: 'center', width: 20
            },
            {
                field: 'paymentType', headerText: 'Payment Type', textAlign: 'Left', width: 20
            },
            {
                field: 'branchLocation', headerText: 'Branch', textAlign: 'Left', width: 20
            },
            {
                field: 'p_Portfolio', headerText: 'Portfolio', textAlign: 'Left', width: 20
            },
            {
                field: 'p_Amount', headerText: 'Amount', textAlign: 'right', width: 20
            },
            {
                field: 'p_CCY', headerText: 'CCY', textAlign: 'Left', width: 20
            },
            {
                field: 'p_TransDate', headerText: 'Trans Date', textAlign: 'center', width: 20
            },
            {
                field: 'p_SettleDate', headerText: 'Settle Date', textAlign: 'center', width: 20
            },
            {
                field: 'p_OrderRef', headerText: 'Order Ref', textAlign: 'Left', width: 20
            },
            {
                field: 'p_OrderAccount', headerText: 'Order Account', textAlign: 'Left', width: 20
            },
            {
                field: 'orderBalanceUnblockedIncPending', headerText: 'Order Balance', textAlign: 'right', width: 20, format:"N2"
            },
            {
                field: 'p_BenPortfolio', headerText: 'Ben Portfolio', textAlign: 'Left', width: 20
            },
            {
                field: 'benAccount', headerText: 'Ben Account', textAlign: 'Left', width: 20
            },
            {
                field: 'p_BenRef', headerText: 'Ben Ref', textAlign: 'Left', width: 20
            },
            {
                field: 'p_SwiftFile', headerText: 'Swift File', textAlign: 'Left', width: 20
            },
            {
                field: 'p_SwiftReturned', headerText: 'Swift Returned', textAlign: 'Left', width: 20
            },
            {
                field: 'p_SwiftRef', headerText: 'Swift Sent Ref', textAlign: 'Left', width: 20
            },
            {
                field: 'p_IMSFile', headerText: 'IMS File', textAlign: 'Left', width: 20
            },
            {
                field: 'p_IMSReturned', headerText: 'IMS Returned', textAlign: 'Left', width: 20
            },
            {
                field: 'p_IMSDocNoIn', headerText: 'Doc No In', textAlign: 'Left', width: 20
            },
            {
                field: 'p_IMSDocNoOut', headerText: 'Doc No Out', textAlign: 'Left', width: 20
            },
            {
                field: 'p_PaidAcknowledged', headerText: 'Paid Ack', textAlign: 'Left', width: 20
            },
            {
                field: 'p_Comments', headerText: 'Comments', textAlign: 'Left', width: 20
            },
            {
                field: 'p_OrderOther', headerText: 'Swift Ref', textAlign: 'Left', width: 20
            },
            {
                field: 'p_CreatedTime', headerText: 'Created Time', textAlign: 'center', width: 20
            },
            {
                field: 'p_ModifiedTime', headerText: 'Modified Time', textAlign: 'center', width: 20
            },
            {
                field: 'p_SwiftSentTime', headerText: 'Swift Sent Time', textAlign: 'center', width: 20
            },
            {
                field: 'p_IMSReceivedTime', headerText: 'IMS Received Time', textAlign: 'center', width: 20
            },
            //{
            //    field: 'p_AuthorisedTime', headerText: 'Authorised Time', textAlign: 'center', width: 20
            //},
            //{
            //    field: 'orderval', headerText: 'Order Val', textAlign: 'Left', width: 20
            //},
            {
                field: 'p_PaidReceivedTime', headerText: 'Paid Received Time', textAlign: 'center', width: 20
            },
            {
                field: 'p_PaidReceivedInPayFile', headerText: 'Paid Received InPay File', textAlign: 'center', width: 20
            },
            {
                field: 'p_PaidReceivedOutPayFile', headerText: 'Paid Received OutPay File', textAlign: 'center', width: 10
            },
            {
                field: 'p_IMSRef', headerText: 'IMS Ref', textAlign: 'Left', width: 10
            },
            {
                field: 'paymentRequested', headerText: 'Pay Request', textAlign: 'Left', width: 10
            },
            {
                field: 'p_PaymentIDOther', headerText: 'PayID Other', textAlign: 'Left', width: 10
            },
            {
                field: 'branchCode', headerText: 'Branch Code', textAlign: 'Left', width: 10
            },
            //{
            //    field: 'p_PortfolioCode', headerText: 'Portfolio Code', textAlign: 'Left', width: 10
            //},
            //{
            //    field: 'p_CCYCode', headerText: 'CCY Code', textAlign: 'Left', width: 20
            //},
            {
                field: 'p_MessageType', headerText: 'Message Type', textAlign: 'Left', width: 10
            }
        ],
        rowHeight: 32,
        aggregates: [{
            columns: [
                {
                    type: 'Sum',
                    field: 'p_Amount',
                    format: 'N2',
                    groupFooterTemplate: 'Total Amount: ${Sum}'
                },
                {
                    type: 'Sum',
                    field: 'orderBalanceUnblockedIncPending',
                    format: 'N2',
                    groupFooterTemplate: 'Total Order Balance: ${Sum}'
                }
            ]
        }],
        beforeDataBound: () => {
            grid.showSpinner();
        },
        dataBound: () => {
            grid.element.querySelector('.e-toolbar-left').style.left = grid.element.querySelector('.e-toolbar-right').getBoundingClientRect().width + 'px';
            grid.autoFitColumns();
            grid.hideSpinner();
        },
        excelExportComplete: () => {
            grid.hideSpinner();
        },
        pdfExportComplete: () => {
            grid.hideSpinner();
        },
        created: () => {
            var gridHeight = ($(".pcoded-main-container").height()) - ($(".e-gridheader").outerHeight()) - ($(".e-toolbar").outerHeight()) - ($(".e-gridpager").outerHeight());
            grid.height = gridHeight;
        },
        rowSelecting: () => {
            if (grid.getSelectedRecords().length) {
                grid.clearSelection();
            }
        },
        rowSelected: () => {
            if (grid.getSelectedRecords().length) {
                var id = grid.getSelectedRecords()[0].p_ID;
                window.location.href = '/BackOfficePayment?id=' + id;
            }
        },
        toolbarClick: (args) => {
            if (args.item.id === 'Grid_excelexport') {
                var exportProperties = {
                    fileName: "CashMovementsOther_" + dateTime + ".xlsx"
                };
                grid.showSpinner();
                grid.excelExport(exportProperties);
            }
            if (args.item.id === 'Grid_pdfexport') {
                var exportProperties = {
                    pageOrientation: 'Landscape',
                    pageSize: 'A4',
                    header: {
                        fromTop: 0,
                        height: 50,
                        contents: [
                            {
                                type: 'PageNumber',
                                pageNumberType: 'Arabic',
                                format: 'Page {$current} of {$total}',
                                position: { x: 0, y: 25 },
                                style: { textBrushColor: '#0008ff', fontSize: 12, hAlign: 'Center' }
                            }
                        ]
                    },
                    theme: {
                        header: { fontColor: '#000000', fontName: 'Calibri', fontSize: 8, bold: true, borders: { color: '#000000', lineStyle: 'Thin' } },
                        caption: { fontColor: '#0e1230', fontName: 'Calibri', fontSize: 9, bold: false },
                        record: { fontColor: '#1E377F', fontName: 'Calibri', fontSize: 8, bold: false }
                    },
                    fileName: "CashMovementsOther_" + dateTime + ".pdf"
                };
                grid.showSpinner();
                grid.pdfExport(exportProperties);
            }
            if (args.item.id === 'HelpCustom') {
                helpModal.open();
            }
        },
    });
    function reloadGrid() {
        indo.cashOtherMovements.cashOtherMovement.getList()
            .then(function (data) {
                grid.dataSource = data;
            });
    }
    function loadGrid() {
        indo.cashOtherMovements.cashOtherMovement.getList()
            .then(function (data) {
                grid.dataSource = data;
                grid.appendTo('#Grid');
            });
    }

    /* Popup Modal */
    var helpModal = new abp.ModalManager(abp.appPath + 'CashOtherMovement/Help');
});
