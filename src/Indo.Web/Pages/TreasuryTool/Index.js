﻿$(function () {
    var l = abp.localization.getResource('Indo');
    var movementB;
    var summaryColumnName = "Total";
    $("select").removeClass("custom-select");

    var grid = new ej.grids.Grid({

        editSettings: { allowEditing: true },
        isResponsive: true,
        enableResponsiveRow: true,
        allowExcelExport: true,
        toolbar: ['Edit', 'Update'],
        actionBegin: (action) => {
            if (action.requestType === 'beginEdit') {
                if (action.rowData.portfolio == summaryColumnName) {
                    action.cancel = true;
                }
            }
        },
        columns: [
            {
                field: 'id', headerText: 'No.', width: 20, allowEditing: false
            },
            {
                field: 'portfolio', headerText: 'Portfolio', width: 170, allowEditing: false, isPrimaryKey: true
            },
            {
                field: 'bankA', headerText: 'Institution A', textAlign: 'Center',
                columns: [
                    {
                        field: 'currentBalanceA', headerText: 'Current Balance', textAlign: 'Left', width: 40, type: 'Number', allowEditing: false, format: 'N2'
                    },
                    {
                        field: 'movementA', headerText: 'Movement', textAlign: 'Right', width: 40, type: 'Number', editType: 'numericedit', format: 'N2',
                        edit: {
                            create: function () {
                                movementAElement = document.createElement('input');
                                return movementAElement;
                            },
                            read: function () {
                                return movementB.value;
                            },
                            destroy: function () {
                                movementB.destroy();
                            },
                            write: function (event) {
                                movementB = new ej.inputs.NumericTextBox({
                                    value: event.rowData[event.column.field],
                                    change: function (event) {
                                        if (event.value != null) {
                                            var selectedRowElement = grid.element.querySelector('form').ej2_instances[0];
                                            var currentBalanceAElement = selectedRowElement.getInputElement('currentBalanceA');
                                            var currentBalanceBElement = selectedRowElement.getInputElement('currentBalanceB');
                                            if ((parseFloat(currentBalanceAElement.value) + event.value >= 0) && (parseFloat(currentBalanceBElement.value) - event.value >= 0)) {
                                                var movementBElement = selectedRowElement.getInputElement('movementB');
                                                movementBElement.value = -1 * movementB.value;
                                                var newBalanceAElement = selectedRowElement.getInputElement('newBalanceA');
                                                newBalanceAElement.value = parseFloat(currentBalanceAElement.value) + movementB.value;

                                                var newBalanceBElement = selectedRowElement.getInputElement('newBalanceB');
                                                newBalanceBElement.value = parseFloat(currentBalanceBElement.value) + parseFloat(movementBElement.value);

                                                var totalMovementA = 0;
                                                var totalMovementB = 0;
                                                var totalNewBalanceA = 0;
                                                var totalNewBalanceB = 0;
                                                var currentViewRecords = grid.getCurrentViewRecords();
                                                var idElement = selectedRowElement.getInputElement('id');
                                                $.each(currentViewRecords, function (i, record) {
                                                    if (record.portfolio != summaryColumnName && idElement.value != record.id.toString()) {
                                                        if (record.movementA != "" && record.movementA != null) {
                                                            totalMovementA += parseFloat(record.movementA)
                                                            totalMovementB += -1 * parseFloat(record.movementA)
                                                        }
                                                    }
                                                    if (record.portfolio != summaryColumnName) {
                                                        if (record.newBalanceA != "" && record.newBalanceA != null) {
                                                            totalNewBalanceA += parseFloat(record.newBalanceA)
                                                        }
                                                        if (record.newBalanceB != "" && record.newBalanceB != null) {
                                                            totalNewBalanceB += parseFloat(record.newBalanceB)
                                                        }
                                                    }
                                                })
                                                totalMovementA += parseFloat(movementB.value)
                                                totalMovementB += -1 * parseFloat(movementB.value)
                                                totalNewBalanceA += parseFloat(movementB.value)
                                                totalNewBalanceB += -1 * movementB.value
                                                grid.setCellValue(summaryColumnName, "movementA", totalMovementA);
                                                grid.setCellValue(summaryColumnName, "movementB", totalMovementB);
                                                grid.setCellValue(summaryColumnName, "newBalanceA", totalNewBalanceA);
                                                grid.setCellValue(summaryColumnName, "newBalanceB", totalNewBalanceB);
                                            }
                                            else {
                                                abp.notify.warn("Movement should be between " + (-1 * parseFloat(currentBalanceAElement.value)) + " & " + (parseFloat(currentBalanceBElement.value)));
                                                movementAElement.value = event.previousValue
                                                event.value = event.previousValue
                                            }
                                        }
                                    }
                                });
                                movementB.appendTo(movementAElement);
                            }
                        },
                        customAttributes: { class: "e-attr" }
                    },
                    {
                        field: 'newBalanceA', headerText: 'New Balance', textAlign: 'Right', width: 40, type: 'Number', allowEditing: false, format: 'N2'
                    }]
            },
            {
                field: 'bankB',headerText: 'Institution B', textAlign: 'Center',
                columns: [
                    {
                        field: 'currentBalanceB', headerText: 'Current Balance', textAlign: 'Right', width: 40, type: 'Number', allowEditing: false, format: 'N2'
                    },
                    {
                        field: 'movementB', headerText: 'Movement', textAlign: 'Right', width: 40, type: 'Number', allowEditing: false, format: 'N2'
                    },
                    {
                        field: 'newBalanceB', headerText: 'New Balance', textAlign: 'Right', width: 40, type: 'Number', allowEditing: false, format: 'N2'
                    }
                ]
            }
        ],
        rowDataBound: onRowDataBound
    });

    function onRowDataBound(event) {
        if (event.data['portfolio'] === summaryColumnName) {
            event.row.classList.add('e-attr-last');
            event.row.childNodes.forEach(x => x.classList.remove("e-attr"))
        }
    }

    $('#btnResetFilter').click(function (e) {
        e.preventDefault();
        $("#ViewModel_CCY").val("-1")
        $("#ViewModel_InstitutionA").val("-1")
        $("#ViewModel_InstitutionB").val("-1")
    });

    $('#btnFilterGrid').click(function (e) {
        e.preventDefault();
        var ccyValue = $("#ViewModel_CCY").val();
        var institutionAValue = $("#ViewModel_InstitutionA").val();
        var institutionBValue = $("#ViewModel_InstitutionB").val();
        var transDateValue = $("#ViewModel_TransDate").val();

        if (ccyValue == "-1" || institutionAValue == "-1" || institutionBValue == "-1") {
            abp.notify.error("Please fill required values");
            return false;
        }
        
        $("#gridCard").hide();
        var bankAColumn = grid.getColumnByUid('grid-column5')
        var bankBColumn = grid.getColumnByUid('grid-column9')
        bankAColumn.headerText = $("#ViewModel_InstitutionA option:selected").text()
        bankBColumn.headerText = $("#ViewModel_InstitutionB option:selected").text()
        grid.refreshColumns()
        reloadGrid();
        $("#gridCard").show();
    });

    $('#btnTransaction').click(function (e) {
        e.preventDefault();
        var currentViewRecords = grid.getCurrentViewRecords();
        var summary = currentViewRecords.find(x => x.portfolio === summaryColumnName);
        if (parseFloat(summary.movementA) != NaN) {
            if (summary.movementA != 0) {
                abp.notify.error("Movement should be zero");
                return false;
            }
        }

        var exportData = currentViewRecords.filter(x => x.portfolio != summaryColumnName && x.movementA != "" && x.movementA != null && x.movementA != 0.0)
        let exportProperties = {
            dataSource: exportData,
        };
        grid.excelExport(exportProperties);
    });

    loadGrid()

    function reloadGrid() {
        debugger
        //Implemented for Testing
        //indo.treasuryTools.treasuryTool.getCurrencyList()
        //    .then(function (data) {
        //       console.log(data)
        //    });
        indo.treasuryTools.treasuryTool.getMovementsList()
            .then(function (data) {
                var totalCurrentBalanceA = 0;
                var totalCurrentBalanceB = 0;
                $.each(data, function (i, record) {
                    record.id = i + 1;
                    if (record.currentBalanceA != "" && record.currentBalanceA != null) {
                        totalCurrentBalanceA += parseFloat(record.currentBalanceA)
                    }
                    if (record.currentBalanceB != "" && record.currentBalanceB != null) {
                        totalCurrentBalanceB += parseFloat(record.currentBalanceB)
                    }
                })
                data.push({ "id": "", "portfolio": summaryColumnName, "currentBalanceA": totalCurrentBalanceA, "movementA": "", "newBalanceA": totalCurrentBalanceA, "currentBalanceB": totalCurrentBalanceB, "movementB": "", "newBalanceB": totalCurrentBalanceB })
                grid.dataSource = data;
            });
    }

    function loadGrid() {
        grid.appendTo('#gridDiv');
    }
});

