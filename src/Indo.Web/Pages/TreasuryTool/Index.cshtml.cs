using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Indo.Web.Pages.TreasuryTool
{
    public class IndexModel : PageModel
    {
        public IndexModel()
        {
            InstitutionList = new List<SelectListItem>
            {
                new SelectListItem { Value = "1", Text = "Bank A"},
                new SelectListItem { Value = "2", Text = "Bank B"},
                new SelectListItem { Value = "3", Text = "Bank C"}
            };
        }

        [BindProperty]
        public TreasuryViewModel ViewModel { get; set; }

        public List<SelectListItem> InstitutionList { get; set; }
        public List<SelectListItem> CCYList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "USD", Text = "USD"},
            new SelectListItem { Value = "GBP", Text = "GBP"},
            new SelectListItem { Value = "EUR", Text = "EUR"}
        };

        public void OnGet()
        {
            InstitutionList.Insert(0,new SelectListItem { Value = "-1", Text = "Select" });
            CCYList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });
            ViewModel = new TreasuryViewModel
            {
                TransDate = DateTime.Now
            };
        }

        public class TreasuryViewModel
        {
            [Required]
            public string CCY { get; set; }

            [Required]
            [Display(Name = "Institution A")]
            public string InstitutionA { get; set; }

            [Required]
            [Display(Name = "Institution B")]
            public string InstitutionB { get; set; }

            [DataType(DataType.Date)]
            [Display(Name = "Trans Date")]
            public DateTime TransDate { get; set; }
        }
    }
}
