﻿$(function () {
    /* Localization */
    var l = abp.localization.getResource('Indo');
    var maxLimitArray = [];
    var globalFile;
    var globalFolderName = 'PaymentForm';
    var globalPath = "/";
    var portfolioCode;
    $("select").removeClass("custom-select");
    onMinMaxChargeChange();
    $(function () {

        var setSelectBoxWidth = document.querySelectorAll('option')
        setSelectBoxWidth.forEach(x => {
            if (x.textContent.length > 180)
                x.textContent = x.textContent.substring(0, 180) + '...';
        })        

        $('#UKShortCode').keyup(function () {
            var splitCode = $(this).val().split("-").join(""); // remove hyphens
            if (splitCode.length > 0) {
                splitCode = splitCode.match(new RegExp('.{1,2}', 'g')).join("-");
            }
            $(this).val(splitCode);
        });
        jQuery.extend(jQuery.validator.messages, {
            minlength: jQuery.validator.format("Please enter at least 6 characters.."),
        });

        $("form").submit(function (e) {
            if ($("input[type=checkbox]:checked").length == 0) {
                abp.notify.warn("You have not ticked any KYC tick boxes, do you want to continue?")
            }
            checkValidationBusinessName();
            checkValidationsurName();
            $("#ViewModel_PaymentFilePath").val("wwwroot/FeesFiles/PaymentForm" +  globalPath);
        });
        function checkValidationBusinessName() {
            if ($("#businessNameVal").val() == '') {
                $("#error").removeClass("error").addClass("error_show");
            }
            else {
                $("#error").addClass("error").removeClass("error_show");
            }
        }
        function checkValidationsurName() {
            if ($("#surName").val() == '') {
                $("#surNameError").removeClass("error").addClass("error_show");
            }
            else {
                $("#surNameError").addClass("error").removeClass("error_show");
            }
        }

        $("#benPortfolio").hide();
        $('#PayType').on('change', function () {
            if (this.value == '1') { $("#benPortfolio").show(); }
            else { $("#benPortfolio").hide(); }
        });

        $('#CCY').on('change', function () {
            manageDirectory()
            if ($("#CCY option:selected").val() != "-1") {
                var ccyValue = $("#CCY option:selected").text();
                indo.payments.payment.getCurrencyList()
                    .then(function (data) {
                        if (data != null) {
                            var CurrencyList = data.find(x => x.fullname === ccyValue)
                            $("#postBalanceCCY").text(CurrencyList.shortcut);
                        }
                    });
            }
        });
        $('#feeCCY').on('change', function () {
            if ($("#feeCCY option:selected").val() != "-1") {
                var feeccyValue = $("#feeCCY option:selected").text();
                indo.payments.payment.getCurrencyList()
                    .then(function (data) {
                        if (data != null) {
                            var CurrencyList = data.find(x => x.fullname === feeccyValue)
                            $("#freeCCYBalanceCCY").text(CurrencyList.shortcut);
                        }
                    });
            }
        });

        $('#Portfolio').on('change', function () {
            manageDirectory()
            indo.payments.payment.fetchSourcePortfolios($("#Portfolio ").val())
                .then(function (data) {
                    if (data != null) {
                        portfolioCode = data[0].sP_Code;
                    }
                });
        });
        $(".hideOnBusinessSelection").css('display', 'none');
        $("#businessName").css('display', 'none');

        $('#benType').on('change', function () {
            var benTypeChar = $("#benType option:selected").val();
            if (benTypeChar == "1") {
                $("#businessName").css('display', 'none');
            }
            else {
                $("#businessName").css('display', 'initial');
            }
            if (benTypeChar == "2") {
                $(".hideOnBusinessSelection").css('display', 'none');
            }
            else {
                $(".hideOnBusinessSelection").css('display', 'initial');
            }
            if (benTypeChar == "-1") {
                $(".hideOnBusinessSelection").css('display', 'none');
                $("#businessName").css('display', 'none');
            }
        });

        function manageDirectory() {
            var today = new Date().toShortFormat()
            var portfolioText = $("#Portfolio option:selected").text();
            var CCYText = $("#CCY option:selected").text();
            var newFolderName = today + '-' + portfolioText + '-' + CCYText;
            globalPath = "/" + newFolderName + "/";
            if (portfolioText != 'Select' && CCYText != 'Select') {
                globalFile.createFolder(newFolderName)
                globalFile.refresh();
            }

        }
        Date.prototype.toShortFormat = function () {
            let monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            let day = this.getDate();
            let monthIndex = this.getMonth();
            let monthName = monthNames[monthIndex];
            let year = this.getFullYear();
            return `${year}-${monthName}-${day}`;
        }

        $("#TemplateName").change(function () {
            if (document.getElementById("TemplateName").value >= 1) {
                $("#tabs-167222").addClass("disable-div");
            }
            else {
                $("#tabs-167222").removeClass("disable-div");
            }
        });

        document.getElementById('ValidateBank').disabled = true;
        document.getElementById('SwiftCode').addEventListener('change', e => {
            if (e.target.value == -1) {
                document.getElementById('ValidateBank').disabled = true;
            }
            else { document.getElementById('ValidateBank').disabled = false; }
        });
        document.getElementById('UKShortCode').addEventListener('keyup', e => {
            var ukAccountNumber = $("#UKAccountNumber").val()
            if (e.target.value.length < 8 && ukAccountNumber.length < 8) {
                document.getElementById('ValidateBank').disabled = true;
            }
            else { document.getElementById('ValidateBank').disabled = false; }
        });

        document.getElementById('UKAccountNumber').addEventListener('keyup', e => {
            var ukShortCode = $("#UKShortCode").val()
            if (e.target.value.length < 8 && ukShortCode.length < 8) {
                document.getElementById('ValidateBank').disabled = true;
            }
            else { document.getElementById('ValidateBank').disabled = false; }
        });

        $("#ValidateBank").click(function (e) {
            var swiftCodeText = $("#SwiftCode option:selected").text();
            var ukSortCode = $("#UKShortCode").val();
            var ukAccountNumber = $("#UKAccountNumber").val();
            if ($("#SwiftCode").val() != "-1") {
                $.ajax({
                    type: "get",
                    url: 'https://bankcodesapi.com/swift/?format=json&api_key=6eeb5030334cd714cada24a7815fb3d1&swift=' + swiftCodeText,
                    success: function (response) {
                    },
                    error: function (request, status, error) {
                        abp.notify.error("Bank details have not been able to be retrieved and that the bank details should be entered manually");
                    }
                });
            }
            else if (ukSortCode != "" && ukSortCode != NaN && ukAccountNumber != "" && ukAccountNumber != NaN) {
                var url = "";
                if (ukSortCode != "" && ukSortCode != NaN) {
                    url += '&sortcode=' + ukSortCode
                }
                if (ukAccountNumber != "" && ukAccountNumber != NaN) {
                    url += '&bankaccount=' + ukAccountNumber
                }
                $.ajax({
                    type: "get",
                    url: 'https://tls.bankaccountchecker.com/listener.php?key=af12dced3a545d6205a115269a7d0f27&password=W0lverin£&output=json&type=uk' + url,
                    success: function (response) {
                    },
                    error: function (request, status, error) {
                        abp.notify.error("Bank details have not been able to be retrieved and that the bank details should be entered manually");
                    }
                });
            }

        });

        document.getElementById('amtChange').addEventListener('keyup', e => {
            var openBalVal = $("#openBalance").val();
            var postBalVal = parseFloat(openBalVal) - parseFloat(e.target.value);
            $('#postBalance').val(postBalVal);
            $('#newBalance').val(postBalVal);
            onChangeColor();
        });

        function onChangeColor() {
            if ($("#postBalance").val() < 0) {
                $("#postBalance").css('color', 'red');
            }
            else {
                $("#postBalance").css('color', 'black');
            }
            if ($("#openBalance").val() < 0) {
                $("#openBalance").css('color', 'red');
            }
            else {
                $("#openBalance").css('color', 'black');
            }
            if ($("#freeCCYBalance").val() < 0) {
                $("#freeCCYBalance").css('color', 'red');
            }
            else {
                $("#freeCCYBalance").css('color', 'black');
            }
            if ($("#newBalance").val() < 0) {
                $("#newBalance").css('color', 'red');
            }
            else {
                $("#newBalance").css('color', 'black');
            }
        }

        function UpdateBalance() {
            var postBalVal = $("#openBalance").val();
            var amtChangeVal = $("#amtChange").val();
            var freeCCYBalval = parseFloat(postBalVal) - parseFloat(amtChangeVal) - parseFloat($('#feeChange').val());
            $('#freeCCYBalance').val(freeCCYBalval);
            $('#newBalance').val(freeCCYBalval);
        }

        $('#ViewModel_StdFree').on('change', function () {
            if (this.value == '2') { $("#feeChange").val("0"); UpdateBalance(); }
            else {
                indo.payments.payment.fetchTransferFees(portfolioCode)
                    .then(function (data) {
                        if (data != null) {
                            $("#feeChange").val(data[0].tF_Fee);
                            UpdateBalance();
                        }
                    });
            }
        });
                
        document.getElementById('feeChange').addEventListener('keyup', e => {
            UpdateBalance();
            onChangeColor();
        });

    });
    $("#ViewModel_MgtFeesMaxCharge").on('blur', function () { onMinMaxChargeChange(); });

    function onMinMaxChargeChange() {
        $("#tblBodyDynamicTableRowsForFees").empty();
        var minCharge = $("#ViewModel_MgtFeesMinCharge").val();
        var maxCharge = $("#ViewModel_MgtFeesMaxCharge").val();
        if (minCharge && !isNaN(minCharge) && !isNaN(parseFloat(minCharge)) && maxCharge && !isNaN(maxCharge) && !isNaN(parseFloat(maxCharge)) && (minCharge < maxCharge)) {
            maxLimitArray = [];
            if (maxLimitArray.indexOf(maxCharge) === -1) {
                maxLimitArray.push(maxCharge);
            }
            generateDynamicRowsForFees(minCharge, maxCharge);
        }
        else {
            $("#tblBodyDynamicTableRowsForFees").html('<tr><td colspan="3">Min and max charge is not valid</td></tr>');
        }
    }

    /* File manager Second Tab Start*/

    var fileManagerFirstOpen = true;
    setTimeout(function () {
        refreshFile();
    }, 1000)
    loadFileManager();
    function loadFileManager() {
        var hostUrl = '/';
        var fileObject = new ej.filemanager.FileManager({
            ajaxSettings: {
                url: hostUrl + 'api/FileManagerFees/FileOperations',
                getImageUrl: hostUrl + 'api/FileManagerFees/GetImage',
                uploadUrl: hostUrl + 'api/FileManagerFees/Upload',
                downloadUrl: hostUrl + 'api/FileManagerFees/Download?folder=' + globalFolderName
            },
            toolbarSettings: { items: ['Upload', 'Delete', 'Refresh'], visible: true },
            beforeSend: (beforeSendEvent) => {
                var request = JSON.parse(beforeSendEvent.ajaxSettings.data);
                if (request.action == "read") {
                    request.path = globalPath
                    request.data = []
                    var updatedRequest = JSON.stringify(request);
                    beforeSendEvent.ajaxSettings.data = updatedRequest
                }
                if (beforeSendEvent.action == "Upload") {
                    request[0].path = globalPath
                    var updatedRequest = JSON.stringify(request);
                    beforeSendEvent.ajaxSettings.data = updatedRequest
                }
                if (request.action == "delete") {
                    request.path = globalPath
                    var updatedRequest = JSON.stringify(request);
                    beforeSendEvent.ajaxSettings.data = updatedRequest
                }
                beforeSendEvent.ajaxSettings.beforeSend = function (beforeSendEvent) {
                    beforeSendEvent.httpRequest.setRequestHeader('folder', globalFolderName);
                }
            },
            beforeImageLoad: (beforeLoadImageEvent) => {
                beforeLoadImageEvent.imageUrl = beforeLoadImageEvent.imageUrl + '&folder=' + globalFolderName;
            },
            created: () => {
                var fileManagerHeight = ($(".pcoded-main-container").height()) - 5;
                fileObject.height = fileManagerHeight;
            },
            view: 'Details',
            failure: (event) => {
                if (event.action == 'create' && event.action == 'delete' && event.error.code == '400') {
                    globalFile.refresh();
                }
            }
        });
        globalFile = fileObject;
        fileObject.appendTo('#filemanager');
    }
    function refreshFile() {
        try {
            var folderNames = [];
            globalFile.selectAll();
            var allDir = globalFile.getSelectedFiles();
            for (var dir = 0; dir < allDir.length; dir++) {
                folderNames.push(allDir[dir].name);
            }
            globalFile.deleteFiles(folderNames);
            globalFile.refresh();
        } catch (e) {
        }
    }
    $("#ViewModel_SubBankName").on("change", function () {
        var subBankNameId = $("#ViewModel_SubBankName").val()
        indo.payments.payment.fetchSwiftCodes(subBankNameId)
            .then(function (data) {
                $("#ViewModel_SubBankSwift").val(data[0].sC_SwiftCode);
                $("#ViewModel_SubBankCtry").val(data[0].sC_RARangeCountryID.toString());
            });
    })
});
