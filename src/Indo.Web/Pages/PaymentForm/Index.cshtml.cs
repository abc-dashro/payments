using Indo.Payments;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;

namespace Indo.Web.Pages.PaymentForm
{
    public class IndexModel : IndoPageModel
    {
        private readonly IPaymentAppService _paymentAppService;
        public IndexModel(IPaymentAppService paymentAppService)
        {
            _paymentAppService = paymentAppService;
        }
        [BindProperty]
        public PaymentViewModel ViewModel { get; set; }

        public List<SelectListItem> PortfolioList { get; set; } 
        public List<SelectListItem> PayTypeList { get; set; } 
        public List<SelectListItem> CCYList { get; set; } 
        public List<SelectListItem> StdFreeList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "1", Text = "Std Fee"},
            new SelectListItem { Value = "2", Text = "Rate"}
        };
        public List<SelectListItem> BenPortfolioList { get; set; } 
        public List<SelectListItem> TemplateNameList { get; set; } 
        public List<SelectListItem> BenTypeList { get; set; } 
        public List<SelectListItem> RelationshipList { get; set; } 
        public List<SelectListItem> CountryList { get; set; } 
        public List<SelectListItem> SwiftCodeList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "1", Text = "BUKBGB22"},
            new SelectListItem { Value = "2", Text = "SWIFTBIC2"},
        };
        public List<SelectListItem> SubBankNameList { get; set; }
        public List<SelectListItem> BankNameList { get; set; } 
        public List<SelectListItem> BankCtryLookupList { get; set; }


        public async Task OnGetAsync()
        {
            var portfolioType = await _paymentAppService.FetchSourcePortfolios("");
            PortfolioList = portfolioType.Select(x => new SelectListItem() { Text = x.SP_Name, Value = x.SP_ID.ToString() }).OrderBy(x => x.Text).ToList();
            PortfolioList.Add(new SelectListItem { Value = "-1", Text = "Select" });

            BenPortfolioList = portfolioType.Select(x => new SelectListItem() { Text = x.SP_Name, Value = x.SP_ID.ToString() }).OrderBy(x => x.Text).ToList();
            BenPortfolioList.Add(new SelectListItem { Value = "-1", Text = "Select" });

            var payType = await _paymentAppService.FetchPaymentTypes("");
            PayTypeList = payType.Select(x => new SelectListItem() { Text = x.PT_PaymentType, Value = x.PT_ID.ToString() }).OrderBy(x => x.Text).ToList();
            PayTypeList.Add(new SelectListItem { Value = "-1", Text = "Select" });

            var ccy = await _paymentAppService.GetCurrencyListAsync();
            CCYList = ccy.Select(x => new SelectListItem() { Text = x.Fullname, Value = x.Id.ToString() }).OrderBy(x => x.Text).ToList();
            CCYList.Add(new SelectListItem { Value = "-1", Text = "Select" });

            StdFreeList.Add(new SelectListItem { Value = "-1", Text = "Select" });

            var templateList = await _paymentAppService.FetchTemplates();
            TemplateNameList = templateList.Select(x => new SelectListItem() { Text = x.pa_TemplateName, Value = x.pa_ID.ToString() }).OrderBy(x => x.Text).ToList();
            TemplateNameList.Add(new SelectListItem { Value = "-1", Text = "Select" });

            var benTypeList = await _paymentAppService.FetchBenficiaries();
            BenTypeList = benTypeList.Select(x => new SelectListItem() { Text = x.BeneficiaryType, Value = x.BeneficiaryTypeID.ToString() }).OrderBy(x => x.Text).ToList();
            BenTypeList.Add(new SelectListItem { Value = "-1", Text = "Select" });

            var relationshipList = await _paymentAppService.FetchSourceClientRelationships();
            RelationshipList = relationshipList.Select(x => new SelectListItem() { Text = x.U_FullName, Value = x.U_ID.ToString() }).OrderBy(x => x.Text).ToList();
            RelationshipList.Add(new SelectListItem { Value = "-1", Text = "Select" });

            SwiftCodeList.Add(new SelectListItem { Value = "-1", Text = "Select" });         

            var fetchCodes = await _paymentAppService.FetchSwiftCodes("-1", "");
            SubBankNameList = fetchCodes.Select(x => new SelectListItem() { Text = x.SC_BankName, Value = x.SC_ID.ToString() }).OrderBy(x => x.Text).ToList();
            SubBankNameList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });

            BankNameList = fetchCodes.Select(x => new SelectListItem() { Text = x.SC_BankName, Value = x.SC_ID.ToString() }).OrderBy(x => x.Text).ToList();
            BankNameList.Add(new SelectListItem { Value = "-1", Text = "Select" });

            var countries = await _paymentAppService.FetchPaymentCountries("");
            BankCtryLookupList = countries.Select(x => new SelectListItem() { Text = x.Ctry_Name, Value = x.Ctry_ID.ToString() }).OrderBy(x => x.Text).ToList();
            BankCtryLookupList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });

            CountryList = countries.Select(x => new SelectListItem() { Text = x.Ctry_Name, Value = x.Ctry_ID.ToString() }).OrderBy(x => x.Text).ToList();
            CountryList.Insert(0, new SelectListItem { Value = "-1", Text = "Select" });
        }

        public IActionResult OnPost()
        {
            try
            {
                Payment Payment = new Payment()
                {
                    NewPay = ViewModel.PayType,
                    P_PortfolioID = ViewModel.Portfolio,
                    P_Amount = ViewModel.Amt,
                    P_CCYID = ViewModel.CCY,
                    P_OrderRef = ViewModel.StdFree,
                    P_PortfolioFee = ViewModel.Fee,
                    P_BenCCYID = ViewModel.CCY2,
                    P_OrderSwIFt = ViewModel.SwiftRef,
                    P_BenPortfolioID = ViewModel.BenPortfolio,
                    P_BenTemplateName = ViewModel.TemplateName,
                    P_BenTypeID = ViewModel.BenType,
                    P_BenRelationshipID = ViewModel.Relationship,
                    P_BenName = ViewModel.BusinessName,
                    P_BenFirstname = ViewModel.FirstName,
                    P_BenMiddlename = ViewModel.MiddleName,
                    P_BenSurname = ViewModel.Surname,
                    P_BenAddress1 = ViewModel.Address,
                    P_BenAddress2 = ViewModel.City,
                    P_BenCounty = ViewModel.County,
                    P_BenZip = ViewModel.PostCode,
                    P_BenBankCountryID = ViewModel.Country,
                    P_BenSubBankName = ViewModel.SubBankName,
                    P_BenBankName = ViewModel.BankName,
                    P_BenSubBankSwIFt = ViewModel.SubBankSwift,
                    P_BenSwIFt = ViewModel.BankSwift,
                    P_BenSubBankIBAN = ViewModel.SubBankIBAN,
                    P_BenIBAN = ViewModel.BankIBAN,
                    P_BenSubBankCountryID = ViewModel.SubBankCtry,
                    P_BenCountryID = ViewModel.BankCtry,
                    PR_CheckedBankName = ViewModel.BankNameLookup,
                    PR_CheckedBankCountryID = ViewModel.BankCtryLookup,
                    PR_CheckedSantions = ViewModel.Checkbox1,
                    PR_CheckedPEP = ViewModel.Checkbox2,
                    PR_CheckedCDD = ViewModel.Checkbox3,
                    PR_IssuesDisclosed = ViewModel.TextArea,
                    PR_PaymentRequestedEmail = ViewModel.EmailTextArea,
                    P_BenBankSwIFt = ViewModel.UKShortCode,
                    P_BenBankIBAN = ViewModel.UKAccountNumber,
                    P_PaymentFilePath = ViewModel.PaymentFilePath
                };

                var data = _paymentAppService.InsertPayment(Payment).Result;
                return NoContent();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException($"{ex.Message}");
            }
        }

        public class PaymentViewModel
        {
            public int Id { get; set; }

            [Required]
            [SelectItems(nameof(PayType))]
            [Display(Name = "Pay Type: ")]
            public int PayType { get; set; }

            [Required]
            [SelectItems(nameof(Portfolio))]
            [Display(Name = "Portfolio: ")]
            public int Portfolio { get; set; }

            [Required]
            [Display(Name = "Amt: ")]
            public int Amt { get; set; }

            [Required]
            [SelectItems(nameof(CCY))]
            [Display(Name = "CCY: ")]
            public int CCY { get; set; }

            [Required]
            [Display(Name = "CCY: ")]
            public int CCY2 { get; set; }

            [SelectItems(nameof(StdFree))]
            [Display(Name = "Rate: ")]
            public string StdFree { get; set; }

            [Display(Name = "Fee: ")]
            public int Fee { get; set; }

            [Display(Name = "Swift Ref: ")]
            public string SwiftRef { get; set; }

            [SelectItems(nameof(BenPortfolio))]
            [Display(Name = "Ben Portfolio: ")]
            public int BenPortfolio { get; set; }

            [Required]
            [SelectItems(nameof(TemplateName))]
            [Display(Name = "Template Name: ")]
            public string TemplateName { get; set; }

            [Required]
            [SelectItems(nameof(BenType))]
            [Display(Name = "Ben Type: ")]
            public int BenType { get; set; }

            [Required]
            [SelectItems(nameof(Relationship))]
            [Display(Name = "Relationship: ")]
            public int Relationship { get; set; }


            [Display(Name = "Business Name: ")]
            public string BusinessName { get; set; }

            [Display(Name = "First Name: ")]
            public string FirstName { get; set; }

            [Display(Name = "Middle Name: ")]
            public string MiddleName { get; set; }


            [Display(Name = "Surname: ")]
            public string Surname { get; set; }

            [Display(Name = "Address: ")]
            public string Address { get; set; }

            [Display(Name = "City: ")]
            public string City { get; set; }

            [Display(Name = "County: ")]
            public string County { get; set; }

            [Display(Name = "Post Code: ")]
            public string PostCode { get; set; }

            [Required]
            [SelectItems(nameof(Country))]
            [Display(Name = "Country: ")]
            public int Country { get; set; }

            [SelectItems(nameof(SubBankName))]
            [Display(Name = "SubBank Name: ")]
            public string SubBankName { get; set; }

            [SelectItems(nameof(BankName))]
            [Display(Name = "Bank Name: ")]
            public string BankName { get; set; }

            [Display(Name = "SubBank Swift: ")]
            public string SubBankSwift { get; set; }

            [Display(Name = "Bank Swift: ")]
            public string BankSwift { get; set; }


            [Display(Name = "SubBank IBAN: ")]
            public string SubBankIBAN { get; set; }

            [Display(Name = "Bank IBAN: ")]
            public string BankIBAN { get; set; }

            [Display(Name = "SubBank Ctry: ")]
            public int SubBankCtry { get; set; }

            [Display(Name = "Bank Ctry: ")]
            public int BankCtry { get; set; }

            [SelectItems(nameof(SwiftCode))]
            [Display(Name = "Swift Code: ")]
            public int SwiftCode { get; set; }

            [Display(Name = "UK Sort Code: ")]
            public string UKShortCode { get; set; }

            [Display(Name = "UK Account Number: ")]
            public string UKAccountNumber { get; set; }

            [Display(Name = "Bank Name: ")]
            public string BankNameLookup { get; set; }

            [Display(Name = "Bank Ctry: ")]
            public int? BankCtryLookup { get; set; }

            [Display(Name = "Confirmed payee and country of payee does not have sanctions against them")]
            public bool Checkbox1 { get; set; }

            [Display(Name = "Confirmed PEP (Politically Exposed Person check) has been completed")]
            public bool Checkbox2 { get; set; }

            [Display(Name = "Confirmed CDD Customer Due Diligence checks has been completed")]
            public bool Checkbox3 { get; set; }

            public string TextArea { get; set; }

            public string EmailTextArea { get; set; }

            public string PaymentFilePath { get; set; }
        }
    }
}
