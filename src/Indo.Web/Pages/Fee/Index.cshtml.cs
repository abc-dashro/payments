using System;
using System.ComponentModel.DataAnnotations;

namespace Indo.Web.Pages.Fee
{
    public class IndexModel : IndoPageModel
    {
        public FeesViewModel ViewModel { get; set; }

        public IndexModel()
        {
            ViewModel = new FeesViewModel()
            {
                MgtFeesMinCharge = 0,
                MgtFeesMaxCharge = 999999999,
                FeeScheduleStartDate = DateTime.UtcNow,
                FeeScheduleEndDate = DateTime.UtcNow.AddYears(1),
                TransactionFeesMinCharge = 0,
                TransactionFeesMaxCharge = 999999999
            };
        }
        public void OnGet()
        {
        }

        public class FeesViewModel
        {
            public int Id { get; set; }

            public string Portfolio { get; set; }
            public string CCY { get; set; }
            [Display(Name = "Is Template Name Edit Allowed?")]
            public bool IsEditTemplateName { get; set; }
            [Display(Name = "Template Name")]
            public string TemplateName { get; set; }
            [Display(Name = "Is Redirect Fee Edit Allowed?")]
            public bool IsEditRedirectFee { get; set; }
            [Display(Name = "Redirect Fee")]
            public string RedirectFee { get; set; }

            [Display(Name = "Start Date")]
            public DateTime FeeScheduleStartDate { get; set; }
            [Display(Name = "End Date")]
            public DateTime FeeScheduleEndDate { get; set; }

            [Display(Name = "Destination")]
            public string FeeScheduleDestination { get; set; }
            [Display(Name = "Pay Frequency")]
            public string FeeSchedulePayFrequency { get; set; }
            [Display(Name = "Is EAM?")]
            public string FeeScheduleIsEAM { get; set; }

            [Display(Name = "Management Fees ?")]
            public bool IsManagementFees { get; set; }
            [Display(Name = "Chargable CCY")]
            public string MgtFeesChargableCCY { get; set; }
            [Display(Name = "Fees Type")]
            public string MgtFeesType { get; set; }
            [Display(Name = "Min Charge")]
            public double MgtFeesMinCharge { get; set; }
            [Display(Name = "Max Charge")]
            public double MgtFeesMaxCharge { get; set; }
            [Display(Name = "Min Maint. Charge")]
            public double MgtFeesMinMaintCharge { get; set; }
            [Display(Name = "Range CCY Equivalent")]
            public string MgtFeesRangeCCYEquivalent { get; set; }

            /* Tab 3 */
            [Display(Name = "Transaction Fees ?")]
            public bool IsTransactionFees { get; set; }
            [Display(Name = "Type")]
            public string TransactionFeesType { get; set; }
            [Display(Name = "Min Charge")]
            public double TransactionFeesMinCharge { get; set; }
            [Display(Name = "Max Charge")]
            public double TransactionFeesMaxCharge { get; set; }
            [Display(Name = "Ticket Fee (Basis Pt)")]
            public double TransactionFeesTicketFee { get; set; }
            [Display(Name = "Fixed Ticket Fee")]
            public double TransactionFeesFixedTicketFee { get; set; }
            [Display(Name = "FOP Fee")]
            public double TransactionFeesFOPFee { get; set; }
            [Display(Name = "Ticket Fee CCY")]
            public TransactionFeeCCY TransactionFeesTicketFeeCCY { get; set; }
            [Display(Name = "Fixed Fee CCY")]
            public TransactionFeeCCY TransactionFeesFixedFeeCCY { get; set; }
            [Display(Name = "FOP Fee CCY")]
            public TransactionFeeCCY TransactionFeesFOPFeeCCY { get; set; }
            [Display(Name = "Cover brokerage fees")]
            public bool IsCoverBrokerageFees { get; set; }
            [Display(Name = "Range CCY Equivalent")]
            public string TransactionFeesRangeCCYEquivalent { get; set; }

        }

        public enum TransactionFeeCCY
        {
            GBP
        }
    }
}
