﻿$(function () {
    /* Localization */
    var l = abp.localization.getResource('Indo');

    var nameFeesViewModel_MgtFeesMinCharge = "ViewModel_MgtFeesMinCharge";
    var nameFeesViewModel_MgtFeesMaxCharge = "ViewModel_MgtFeesMaxCharge";
    var nameSubMaxCharges = "subMaxCharges";
    var nameSubMaxChargesForTransactionFees = "subMaxChargesForTransactionFees";
    var nameFeesViewModel_TransactionFeesMinCharge = "ViewModel_TransactionFeesMinCharge";
    var nameFeesViewModel_TransactionFeesMaxCharge = "ViewModel_TransactionFeesMaxCharge";

    var mgtFeesMinChargeTextBox = $("#" + nameFeesViewModel_MgtFeesMinCharge);
    var mgtFeesMaxChargeTextBox = $("#" + nameFeesViewModel_MgtFeesMaxCharge);
    var tblBodyDynamicTableRowsForFees = $("#tblBodyDynamicTableRowsForFees");
    var transactionFeesMinCharge = $("#" + nameFeesViewModel_TransactionFeesMinCharge);
    var transactionFeesMaxCharge = $("#" + nameFeesViewModel_TransactionFeesMaxCharge);
    var tblBodyDynamicTableRowsForTransactionFees = $("#tblBodyDynamicTableRowsForTransactionFees");

    var maxLimitArray = [];
    var maxLimitArrayTransactionFees = [];
    var assetTypeList = ["Fixed Income Govt", "FX"];

    var minChargeFee = 0;
    var maxChargeFee = 0;
    var minTransactionChargeFee = 0;
    var maxTransactionChargeFee = 0;

    onMinMaxChargeChange();
    setValidationsForMgtFees();
    onMinMaxChargeChangeForTransactionFees();
    setValidationsForTransactionFees();

    /* Fees Management first Tab Start*/
    mgtFeesMinChargeTextBox.on('blur', function () { onMinMaxChargeChange(); });
    mgtFeesMaxChargeTextBox.on('blur', function () { onMinMaxChargeChange(); });

    function setValidationsForMgtFees() {
        setInputFilter(document.getElementById(nameFeesViewModel_MgtFeesMinCharge), function (value) {
            return /^\d*$/.test(value) && (value === "" || parseInt(value) <= mgtFeesMaxChargeTextBox.val());
        });
        setInputFilter(document.getElementById(nameFeesViewModel_MgtFeesMaxCharge), function (value) {
            return /^\d*$/.test(value);
        });
        setInputFilter(document.getElementById(nameSubMaxCharges), function (value) {
            return /^\d*$/.test(value) && (value === "" || parseInt(value) <= mgtFeesMaxChargeTextBox.val());
        });
    }

    function onMinMaxChargeChange() {
        var minCharge = mgtFeesMinChargeTextBox.val();
        var maxCharge = mgtFeesMaxChargeTextBox.val();
        if (minCharge && !isNaN(minCharge) && !isNaN(parseFloat(minCharge)) && maxCharge && !isNaN(maxCharge) && !isNaN(parseFloat(maxCharge)) && (minCharge < maxCharge)) {
            if (minCharge != minChargeFee || maxCharge != maxChargeFee) {
                minChargeFee = minCharge = Number(minCharge);
                maxChargeFee = maxCharge = Number(maxCharge);

                tblBodyDynamicTableRowsForFees.empty();
                maxLimitArray = [];
                if (maxLimitArray.filter(x => x.charge == maxCharge).length == 0) {
                    maxLimitArray.push({ charge: maxCharge, commission: 0 });
                }
                generateDynamicRowsForFees(minCharge, maxCharge);
            }
        }
        else {
            tblBodyDynamicTableRowsForFees.html('<tr><td colspan="4">Min and max charge is not valid</td></tr>');
        }
    }

    function onSubMaxChargeBlur() {
        var subMaxCharge = $("#" + nameSubMaxCharges).val();
        var minCharge = mgtFeesMinChargeTextBox.val();
        var maxCharge = mgtFeesMaxChargeTextBox.val();
        if (!(subMaxCharge != null && subMaxCharge != '' && subMaxCharge.length > 0)) {
            subMaxCharge = maxCharge;
        }

        for (var i = 0; i < maxLimitArray.length; i++) {
            var obj = maxLimitArray.filter(x => x.charge == maxLimitArray[i].charge);
            if (obj) {
                maxLimitArray[i].commission = $('#commission_' + maxLimitArray[i].charge).val();
            }
        }

        tblBodyDynamicTableRowsForFees.empty();
        if (subMaxCharge && !isNaN(subMaxCharge) && !isNaN(parseFloat(subMaxCharge)) && minCharge && !isNaN(minCharge) && !isNaN(parseFloat(minCharge)) && maxCharge && !isNaN(maxCharge) && !isNaN(parseFloat(maxCharge)) && (Number(minCharge) < Number(maxCharge)) && (Number(minCharge) < Number(subMaxCharge) && Number(subMaxCharge) <= Number(maxCharge))) {
            if (maxLimitArray.filter(x => x.charge == Number(subMaxCharge)).length == 0) {
                maxLimitArray.push({ charge: Number(subMaxCharge), commission: 0 });
            }
            generateDynamicRowsForFees(minCharge, maxCharge);
        }
        else {
            tblBodyDynamicTableRowsForFees.html('<tr><td colspan="4">Min and max charge is not valid</td></tr>');
        }

    }

    function generateDynamicRowsForFees(minCharge, maxCharge) {
        maxLimitArray.sort(function (a, b) { return a.charge - b.charge; });
        for (var i = 0; i < maxLimitArray.length; i++) {
            var strRowHtml = "<tr class='text-right'>";
            strRowHtml += (i == maxLimitArray.length - 1) ? '<td></td>' : '<td><a href="#!" class="mgtFees" data-delValue="' + maxLimitArray[i].charge + '"><i class="far fa-trash-alt"></i></a></td>';
            // first column
            strRowHtml += (i == 0) ? '<td>' + minCharge + '</td>' : '<td>' + (Number(maxLimitArray[i - 1].charge) + 1) + '</td>';

            // second column
            if (i == 0) {
                if (maxLimitArray[i].charge == maxCharge) {
                    strRowHtml += '<td><input class="form-control text-right" id="' + nameSubMaxCharges + '" type="number" min="' + minCharge + '" max="' + maxCharge + '" value="' + maxCharge + '">' + '</td>';
                } else {
                    strRowHtml += '<td>' + maxLimitArray[i].charge + '</td>';
                }
            }
            else if (i == maxLimitArray.length - 1) {
                strRowHtml += '<td><input id="' + nameSubMaxCharges + '" class="form-control text-right" type="number" min="' + minCharge + '" max="' + maxLimitArray[i].charge + '" value="' + maxLimitArray[i].charge + '">' + '</td>';
            }
            else {
                strRowHtml += '<td>' + maxLimitArray[i].charge + '</td>';
            }

            // third column
            strRowHtml += '<td><input class="form-control text-right" id="commission_' + maxLimitArray[i].charge + '" type="number" value="' + maxLimitArray[i].commission + '" /></td>';
            strRowHtml += "</tr>";
            tblBodyDynamicTableRowsForFees.append(strRowHtml);
        }
        $("#" + nameSubMaxCharges).on('blur', function () { onSubMaxChargeBlur(); });
        setValidationsForMgtFees();
        $('.mgtFees').on('click', function () {
            var delValue = $(this).attr('data-delValue');
            maxLimitArray = maxLimitArray.filter(x => x.charge != delValue);
            tblBodyDynamicTableRowsForFees.empty();
            generateDynamicRowsForFees(minCharge, maxCharge);
        });
    }
    /* Fees Management first Tab End */

    /* File manager Second Tab Start*/

    var fileManagerFirstOpen = true;
    loadFileManager();
    function loadFileManager() {
        var hostUrl = '/';
        var fileObject = new ej.filemanager.FileManager({
            ajaxSettings: {
                url: hostUrl + 'api/FileManagerFees/FileOperations',
                getImageUrl: hostUrl + 'api/FileManagerFees/GetImage',
                uploadUrl: hostUrl + 'api/FileManagerFees/Upload',
                downloadUrl: hostUrl + 'api/FileManagerFees/Download?folder=' + 'Fees Files'
            },
            beforeSend: (args) => {
                args.ajaxSettings.beforeSend = function (args) {
                    args.httpRequest.setRequestHeader('folder', 'Fees Files');
                };
            },
            beforeImageLoad: (args) => {
                args.imageUrl = args.imageUrl + '&folder=' + 'Fees Files';
            },
            created: () => {
                var fileManagerHeight = ($(".pcoded-main-container").height()) - 5;
                fileObject.height = fileManagerHeight;
            },
            view: 'Details',
        });
        fileObject.appendTo('#filemanager');
    }

    $('#tabs-167221 a').on('click', function (e) {
        e.preventDefault();

        //File Manager
        if (e.target.id === 'file-tab' && fileManagerFirstOpen) {
            fileManagerFirstOpen = false;
            setTimeout(() => {
                var fileObject = document.getElementById("filemanager").ej2_instances[0];
                fileObject.refresh();
            }, 500);
        }
    });

    /* File manager Second Tab End */

    /* File manager Third Tab Start */
    transactionFeesMinCharge.on('blur', function () { onMinMaxChargeChangeForTransactionFees(); });
    transactionFeesMaxCharge.on('blur', function () { onMinMaxChargeChangeForTransactionFees(); });

    function setValidationsForTransactionFees() {
        setInputFilter(document.getElementById(nameFeesViewModel_TransactionFeesMinCharge), function (value) {
            return /^\d*$/.test(value) && (value === "" || parseInt(value) <= transactionFeesMinCharge.val());
        });
        setInputFilter(document.getElementById(nameFeesViewModel_TransactionFeesMaxCharge), function (value) {
            return /^\d*$/.test(value);
        });
        if (document.getElementsByClassName(nameSubMaxChargesForTransactionFees).length > 0) {
            var elements = document.getElementsByClassName(nameSubMaxChargesForTransactionFees);
            for (var i = 0; i < elements.length; i++) {
                setInputFilter(elements[i], function (value) {
                    return /^\d*$/.test(value) && (value === "" || parseInt(value) <= transactionFeesMaxCharge.val());
                });
            }
        }
    }

    function onMinMaxChargeChangeForTransactionFees() {
        var minCharge = transactionFeesMinCharge.val();
        var maxCharge = transactionFeesMaxCharge.val();
        if (minCharge && !isNaN(minCharge) && !isNaN(parseFloat(minCharge)) && maxCharge && !isNaN(maxCharge) && !isNaN(parseFloat(maxCharge)) && (minCharge < maxCharge)) {

            if (minCharge != minTransactionChargeFee || maxCharge != maxTransactionChargeFee) {
                minTransactionChargeFee = minCharge = Number(minCharge);
                maxTransactionChargeFee = maxCharge = Number(maxCharge);
                tblBodyDynamicTableRowsForTransactionFees.empty();
                maxLimitArrayTransactionFees = [];
                if (maxLimitArrayTransactionFees.filter(x => x.charge == maxCharge).length == 0) {
                    assetTypeList.forEach(function (item, index) {
                        maxLimitArrayTransactionFees.push({ assetType: item, charge: maxCharge, commission: 0 });
                    });
                }
                generateDynamicRowsForTransactionFees(minCharge, maxCharge);
            }
        } else {
            tblBodyDynamicTableRowsForTransactionFees.html('<tr><td colspan="5">Min and max charge is not valid</td></tr>');
        }
    }

    function onSubMaxChargeBlurForTransactionFees(subMaxChargesForTransactionFeesId) {
        var subMaxCharge = $("#" + subMaxChargesForTransactionFeesId).val();
        var minCharge = transactionFeesMinCharge.val();
        var maxCharge = transactionFeesMaxCharge.val();
        if (!(subMaxCharge != null && subMaxCharge != '' && subMaxCharge.length > 0)) {
            subMaxCharge = maxCharge;
        }

        for (var i = 0; i < maxLimitArrayTransactionFees.length; i++) {
            maxLimitArrayTransactionFees[i].commission = $('#commission_' + maxLimitArrayTransactionFees[i].assetType.replace(/\s/g, "_") + '_' + maxLimitArrayTransactionFees[i].charge).val();
        }

        tblBodyDynamicTableRowsForTransactionFees.empty();

        if (subMaxCharge && !isNaN(subMaxCharge) && !isNaN(parseFloat(subMaxCharge)) && minCharge && !isNaN(minCharge) && !isNaN(parseFloat(minCharge)) && maxCharge && !isNaN(maxCharge) && !isNaN(parseFloat(maxCharge)) && (Number(minCharge) < Number(maxCharge)) && (Number(minCharge) < Number(subMaxCharge) && Number(subMaxCharge) <= Number(maxCharge))) {
            if (maxLimitArrayTransactionFees.filter(x => x.charge == subMaxCharge).length == 0) {
                assetTypeList.forEach(function (item, index) {
                    maxLimitArrayTransactionFees.push({ assetType: item, charge: subMaxCharge, commission: 0 });
                });
            }
            generateDynamicRowsForTransactionFees(minCharge, maxCharge);
        }
        else {
            tblBodyDynamicTableRowsForTransactionFees.html('<tr><td colspan="5">Min and max charge is not valid</td></tr>');
        }
    }

    function generateDynamicRowsForTransactionFees(minCharge, maxCharge) {
        maxLimitArrayTransactionFees.sort(function (a, b) { return a.assetType - b.assetType || a.charge - b.charge; });
        if (assetTypeList != null && assetTypeList.length > 0) {
            assetTypeList.forEach(function (item, index) {
                var subMaxChargesForTransactionFeesId = nameSubMaxChargesForTransactionFees + index;
                var transactionFeesMaxLimitArrayForAssetType = maxLimitArrayTransactionFees.filter(x => x.assetType == item);
                for (var i = 0; i < transactionFeesMaxLimitArrayForAssetType.length; i++) {

                    var strRowHtml = '<tr class="text-right">';
                    strRowHtml += (i == transactionFeesMaxLimitArrayForAssetType.length - 1) ? '<td></td>' : '<td><a href="#!" class="transactionFees" data-delValue="' + transactionFeesMaxLimitArrayForAssetType[i].charge + '"><i class="far fa-trash-alt"></i></a></td>';
                    //first column 
                    strRowHtml += '<td>' + bindAssetTypes(item) + '</td>';

                    //second column
                    strRowHtml += (i == 0) ? '<td>' + minCharge + '</td>' : '<td>' + (Number(transactionFeesMaxLimitArrayForAssetType[i - 1].charge) + 1) + '</td>';

                    // third column
                    if (i == 0) {
                        if (transactionFeesMaxLimitArrayForAssetType[i].charge == maxCharge) {
                            strRowHtml += '<td><input class="form-control text-right ' + nameSubMaxChargesForTransactionFees + '" id="' + subMaxChargesForTransactionFeesId + '" type="number" min="' + minCharge + '" max="' + maxCharge + '" value="' + maxCharge + '">' + '</td>';
                        } else {
                            strRowHtml += '<td>' + transactionFeesMaxLimitArrayForAssetType[i].charge + '</td>';
                        }
                    }
                    else if (i == transactionFeesMaxLimitArrayForAssetType.length - 1) {
                        strRowHtml += '<td><input id= ' + subMaxChargesForTransactionFeesId + ' class="form-control text-right ' + nameSubMaxChargesForTransactionFees + '" type="number" min="' + minCharge + '" max="' + transactionFeesMaxLimitArrayForAssetType[i].charge + '" value="' + transactionFeesMaxLimitArrayForAssetType[i].charge + '">' + '</td>';
                    }
                    else {
                        strRowHtml += '<td>' + transactionFeesMaxLimitArrayForAssetType[i].charge + '</td>';
                    }

                    // fourth column
                    strRowHtml += '<td><input class="form-control text-right" id="commission_' + item.replace(/\s/g, "_") + '_' + transactionFeesMaxLimitArrayForAssetType[i].charge + '" type="number" value="' + transactionFeesMaxLimitArrayForAssetType[i].commission + '"></td>';

                    strRowHtml += '</tr>';

                    tblBodyDynamicTableRowsForTransactionFees.append(strRowHtml);
                }
                $("#" + subMaxChargesForTransactionFeesId).on('blur', function () { onSubMaxChargeBlurForTransactionFees(subMaxChargesForTransactionFeesId); });

            });
            setValidationsForTransactionFees();
            $('.transactionFees').on('click', function () {
                var delValue = $(this).attr('data-delValue');
                maxLimitArrayTransactionFees = maxLimitArrayTransactionFees.filter(x => x.charge != delValue);
                tblBodyDynamicTableRowsForTransactionFees.empty();
                generateDynamicRowsForTransactionFees(minCharge, maxCharge);
            });
        }
    }

    function bindAssetTypes(assetType) {
        var tag = "<select class='form-control'>";
        assetTypeList.forEach(function (item) {
            if (item === assetType) {
                tag += "<option selected>" + item + "</option>";
            }
            else {
                tag += "<option>" + item + "</option>";
            }
        });
        tag += "</select>";
        return tag;
    }

    /* File manager Third Tab End */

    /* validate inputs */
    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
            textbox.addEventListener(event, function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });
    }

});
