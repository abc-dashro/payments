﻿$(function () {
    /* Localization */
    var l = abp.localization.getResource('Indo');
    var maxLimitArray = [];

    onMinMaxChargeChange();

    $("#FeesViewModel_MgtFeesMaxCharge").on('blur', function () { onMinMaxChargeChange(); });

    /* Fees Management first Tab Start*/
    function onMinMaxChargeChange() {
        $("#tblBodyDynamicTableRowsForFees").empty();
        var minCharge = $("#FeesViewModel_MgtFeesMinCharge").val();
        var maxCharge = $("#FeesViewModel_MgtFeesMaxCharge").val();
        if (minCharge && !isNaN(minCharge) && !isNaN(parseFloat(minCharge)) && maxCharge && !isNaN(maxCharge) && !isNaN(parseFloat(maxCharge)) && (minCharge < maxCharge)) {
            maxLimitArray = [];
            if (maxLimitArray.indexOf(maxCharge) === -1) {
                maxLimitArray.push(maxCharge);
            }
            generateDynamicRowsForFees(minCharge, maxCharge);
        }
        else {
            $("#tblBodyDynamicTableRowsForFees").html('<tr><td colspan="3">Min and max charge is not valid</td></tr>');
        }
    }

    function onSubMaxChargeBlur() {

        var subMaxCharge = $("#subMaxCharges").val();

        $("#tblBodyDynamicTableRowsForFees").empty();
        var minCharge = $("#FeesViewModel_MgtFeesMinCharge").val();
        var maxCharge = $("#FeesViewModel_MgtFeesMaxCharge").val();
        if (subMaxCharge && !isNaN(subMaxCharge) && !isNaN(parseFloat(subMaxCharge)) && minCharge && !isNaN(minCharge) && !isNaN(parseFloat(minCharge)) && maxCharge && !isNaN(maxCharge) && !isNaN(parseFloat(maxCharge)) && (Number(minCharge) < Number(maxCharge)) && (Number(minCharge) < Number(subMaxCharge) && Number(subMaxCharge) <= Number(maxCharge))) {
            if (maxLimitArray.indexOf(subMaxCharge) === -1) {
                maxLimitArray.push(Number(subMaxCharge));
            }
            generateDynamicRowsForFees(minCharge, maxCharge);
        }
        else {
            $("#tblBodyDynamicTableRowsForFees").html('<tr><td colspan="3">Min and max charge is not valid</td></tr>');
        }
    }

    function generateDynamicRowsForFees(minCharge, maxCharge) { 
        maxLimitArray.sort();
        for (var i = 0; i < maxLimitArray.length; i++) {
            if (i == 0) {
                if (maxLimitArray[i] == maxCharge) {
                    $("#tblBodyDynamicTableRowsForFees").append('<tr class="text-right"><td>' + minCharge + '</td><td><input class="form-control text-right" id="subMaxCharges" type="number" min="' + minCharge + '" max="' + maxCharge + '" value="' + maxCharge + '">' + '</td><td><input class="form-control text-right" id="commission_' + i + '" type="number" /></td></tr>');
                }
                else {
                    $("#tblBodyDynamicTableRowsForFees").append('<tr class="text-right"><td>' + minCharge + '</td><td>' + maxLimitArray[i] + '</td><td><input class="form-control text-right" name="commission_' + i + '" type="number" /></td></tr>');
                }
            }
            else if (i == maxLimitArray.length - 1) {
                $("#tblBodyDynamicTableRowsForFees").append('<tr class="text-right"><td>' + (Number(maxLimitArray[i - 1]) + 1) + '</td><td><input id="subMaxCharges" class="form-control text-right" type="number" min="' + minCharge + '" max="' + maxLimitArray[i] + '" value="' + maxLimitArray[i] + '">' + '</td><td><input class="form-control text-right" id="commission_' + i + '" type="number"></td></tr>');
            }
            else {
                $("#tblBodyDynamicTableRowsForFees").append('<tr class="text-right"><td>' + (Number(maxLimitArray[i - 1]) + 1) + '</td><td>' + maxLimitArray[i] + '</td><td><input class="form-control text-right" id="commission_' + i + '" type="number"></td></tr>');
            }
        }
        $("#subMaxCharges").on('blur', function () { onSubMaxChargeBlur(); });
        /*$("#subMaxCharges").focus();*/
    }

    /* Fees Management first Tab End */

    /* File manager Second Tab Start*/
    var fileManagerFirstOpen = true;
    loadFileManager();
    function loadFileManager() {
        var hostUrl = '/';
        var fileObject = new ej.filemanager.FileManager({
            ajaxSettings: {
                url: hostUrl + 'api/FileManage/FileOperations',
                getImageUrl: hostUrl + 'api/FileManage/GetImage',
                uploadUrl: hostUrl + 'api/FileManage/Upload',
                downloadUrl: hostUrl + 'api/FileManage/Download?folder=' + 'DefaultFolder'
            },
            beforeSend: (args) => {
                args.ajaxSettings.beforeSend = function (args) {
                    // args.httpRequest.setRequestHeader('folder', $('#hfRootFolder').val());
                    args.httpRequest.setRequestHeader('folder', 'DefaultFolder');
                }
            },
            beforeImageLoad: (args) => {
                args.imageUrl = args.imageUrl + '&folder=' + 'DefaultFolder';
            },
            created: () => {
                var fileManagerHeight = ($(".pcoded-main-container").height()) - 5;
                fileObject.height = fileManagerHeight;
            },
            view: 'Details',
        });
        fileObject.appendTo('#fileuploadmanager');
    }

    /* File manager Second Tab End */

    /* Tab Click */
    $('#tabs-167221 a').on('click', function (e) {
        e.preventDefault();

        //File Manager
        if (e.target.id === 'file-tab' && fileManagerFirstOpen) {
            fileManagerFirstOpen = false;
            setTimeout(() => {
                var fileObject = document.getElementById("fileuploadmanager").ej2_instances[0];
                fileObject.refresh();
            }, 500);
        }
    })
});
