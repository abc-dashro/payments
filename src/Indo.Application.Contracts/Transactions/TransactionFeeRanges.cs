﻿namespace Indo.Transactions
{
    public class TransactionFeeRanges
    {
        public int TF_ID { get; set; }
        public int TF_TType { get; set; } //Asset Type
        public double TF_RangeFrom { get; set; } //From Amount
        public double TF_RangeTo { get; set; } //To Amount
        public double TF_Commission { get; set; } //Commission %
    }
}