﻿namespace Indo.Transactions
{
    public class TransactionSafekeeping
    {
        public int TF_ID { get; set; }
        public int TF_ClearerCode { get; set; } //Safekeeping
        public double TF_Commission { get; set; } //Commission
        public double TF_TicketFee { get; set; } //TicketFee
        public int TF_TicketFeeCCYCode { get; set; } //TicketFeeCCY
    }
}