﻿namespace Indo.FilePaths
{
    public class FilePathReadDto
    {
        public int Path_ID { get; set; }
        public string Path_Name { get; set; }
        public string Path_FilePath { get; set; }
        public string Path_UserModifiedBy { get; set; }
        public string Path_UserModifiedTime { get; set; }
    }
}