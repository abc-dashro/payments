﻿namespace Indo.Currencies
{
    public class FlowCurrency
    {
        public int Id { get; set; } //CR_Code
        public string Name { get; set; } //CR_Name
        public string Fullname { get; set; } //CR_FullName
        public string Shortcut { get; set; } //CR_Shortcut
        public string Reverse { get; set; } //CR_Reverse
    }
}