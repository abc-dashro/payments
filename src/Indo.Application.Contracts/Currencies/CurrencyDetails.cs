﻿namespace Indo.Currencies
{
    public class CurrencyDetails
    {
        public int CR_Code { get; set; }
        public string CR_Name { get; set; }
        public string CR_FullName { get; set; }
        public string CR_Shortcut { get; set; }
        public string CR_Reverse { get; set; }
    }
}