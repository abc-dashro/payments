﻿namespace Indo.FileExports
{
    public class FileExportReadDto
    {
        public int FE_ID { get; set; }
        public string FE_Key { get; set; }
        public string FE_Name { get; set; }
        public bool FE_ExportBuffer { get; set; }
        public string FE_UserModifiedBy { get; set; }
        public string FE_UserModifiedTime { get; set; }
    }
}