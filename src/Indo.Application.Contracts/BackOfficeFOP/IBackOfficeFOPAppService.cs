﻿using Indo.Currencies;
using Indo.FreeOfPayments;
using Indo.PaymentReceiveDeliveries;
using Indo.PaymentReceiveDeliverType;
using Indo.PaymentSelectAccounts;
using Indo.PaymentTypes;
using Indo.PortfolioHomeAddresses;
using Indo.SourceInstruments;
using Indo.SourcePortfolios;
using Indo.SourceSettlementPlaces;
using Indo.SwiftCodes;
using Indo.Swifts;
using Indo.SwiftsMT542;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;



namespace Indo.BackOfficeFOPs
{
    public interface IBackOfficeFOPAppService : IApplicationService
    {
        Task<List<PortfoliosReadDto>> FetchSourcePortfolios(string? SpId);
        Task<List<PaymentTypesReadDto>> FetchPaymentTypes(string? ctyId);
        Task<List<FlowCurrency>> FetchCurrencies();
        Task<List<InstrumentsReadDto>> FetchSourceInstruments(string? ctyId);
        Task<List<SettlementPlacesReadDto>> FetchSourceSettlementPlaces(string? ctyId);
        Task<List<SwiftCodeReadDto>> FetchSwiftCodes(string? scId, string? swiftCode);
        Task<string> InsertFOPPayment(FOP fop);
        Task<string> UpdateFOPPayment(FOP fop);        
        Task<string> FetchSwift(SwiftDto? swiftDto);
        Task<string> FetchSwiftMT542(SwiftMT542Dto? swiftMT542Dto);
        Task<PaymentHomeAddressReadDto> FetchPaymentHomeAddress();
        Task<List<PaymentReceiveDeliverTypeReadDto>> FetchPaymentReceiveDeliverType();
        Task<List<PaymentSelectAccountReadDto>> FetchPaymentSelectAccount(string? spId);
        Task<List<PaymentRecieveDeliveriesReadDto>> FetchPaymentReceiveDeliveries(string prdId);
    }
}
