﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.Portfolios
{
    public interface IPortfolioAppService
    {
        Task<List<PortfolioBalancesReadDto>> GetListAsync();
    }
}