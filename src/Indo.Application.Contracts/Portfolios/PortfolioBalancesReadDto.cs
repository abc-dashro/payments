﻿namespace Indo.Portfolios
{
    public class PortfolioBalancesReadDto
    {
        public string Portfolio { get; set; }
        public string PortfolioType { get; set; }
        public string CCY { get; set; }
        public string Broker { get; set; }
        public string Account { get; set; }
        public string BrokerClass { get; set; }
        public double Balance { get; set; }
        public string T_Account { get; set; }
        public string T_IBAN { get; set; }
        public int BranchCode { get; set; }
        public string BranchLocation { get; set; }
        public int PortfolioCode { get; set; }
        public string PFShortcut { get; set; }
        public int InstrumentCode { get; set; }
        public int BrokerCode { get; set; }
        public int CCYCode { get; set; }
        public string Name { get; set; }
    }
}