﻿namespace Indo.PaymentReceiveDeliverType
{
    public class PaymentReceiveDeliverTypeReadDto
    {
        public int rD_ID { get; set; }
        public string rD_Type { get; set; }
    }
}
