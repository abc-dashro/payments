﻿using System;

namespace Indo.Benficiaries
{
    public class BeneficiaryReadDto
    {
        public int BeneficiaryTypeID { get; set; }
        public string BeneficiaryType { get; set; }
        public int? BeneficiaryTypeThreshold { get; set; }
        public string? Ben_ModifiedBy { get; set; }
        public DateTime? Ben_ModifiedDate { get; set; }
    }
}