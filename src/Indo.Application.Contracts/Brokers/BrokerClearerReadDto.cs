﻿namespace Indo.Brokers
{
    public class BrokerClearerReadDto
    {
        public int BrokerID { get; set; }
        public string BrokerSwiftCode { get; set; }
        public string BrokerBankName { get; set; }
        public string BrokerAccountNo { get; set; }
        public int ClearerID { get; set; }
        public string ClearerSwiftCode { get; set; }
        public string ClearerShortCode { get; set; }
        public string ClearerExchangeCode { get; set; }
    }
}