﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.Brokers
{
    public interface IBrokerAppService : IApplicationService
    {
        Task<List<BrokerBalancesReadDto>> GetListAsync();
    }
}