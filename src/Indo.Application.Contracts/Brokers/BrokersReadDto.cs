﻿namespace Indo.Brokers
{
    public class BrokersReadDto
    {
        public int B_Code { get; set; }
        public string B_ShortCut1 { get; set; }
        public string B_ShortCut2 { get; set; }
        public string B_Name1 { get; set; }
        public string B_Name2 { get; set; }
        public string B_Address { get; set; }
        public string B_Zip { get; set; }
        public string B_Area { get; set; }
        public string B_Class { get; set; }
        public string B_Account { get; set; }
        public string B_SwiftAddress { get; set; }
        public int B_Active { get; set; }
        public string BrokerClass { get; set; }
        public string BP_Object { get; set; }
        public string SC_SwiftCode { get; set; }
        public string? SC_BIK { get; set; }
        public string? SC_INN { get; set; }
        public string SC_FailMessage { get; set; }
        public int SC_ID { get; set; }
        public int SC_SwiftTransferMessTypeID { get; set; }
        public string? SwiftTransferMessType { get; set; }
        public int SC_Swift3rdPartyMessTypeID { get; set; }
        public string? Swift3rdPartyMessType { get; set; }
        public int SC_SwiftOtherMessTypeID { get; set; }
        public string? SwiftOtherMessType { get; set; }
        public bool SC_SwiftPaidReturned { get; set; }
        public int B_BlockBroker { get; set; }
        public int SC_CountryID { get; set; }
        public string SC_Country { get; set; }
    }
}
