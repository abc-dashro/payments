﻿using System;

namespace Indo.Brokers
{
    public class BrokerBalancesReadDto
    {
        public string Broker { get; set; }
        public string CCY { get; set; }
        public string BrokerClass { get; set; }
        public double Balance { get; set; }
        public int BranchCode { get; set; }
        public string BranchLocation { get; set; }
    }
}