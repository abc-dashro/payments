﻿namespace Indo.PaymentRARangeType
{
    public class PaymentRARangeTypeReadDto
    {
        public int PTy_ID { get; set; }
        public string PTy_Description { get; set; }
        public string PTy_Score { get; set; }
        public string PTy_ModifiedBy { get; set; }        
        public string PTy_ModifiedDate { get; set; }
    }
}