﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Indo.Payments
{
    public class Payment
    {
        public int NewPay { get; set; }
        public int? P_ID { get; set; }
        public string P_PaymentType { get; set; }
        public int P_MessageTypeID { get; set; }
        public int P_PortfolioID { get; set; }
        public string P_Portfolio { get; set; }
        public DateTime P_TransDate { get; set; }
        public DateTime P_SettleDate { get; set; }
        public double P_Amount { get; set; }
        public int P_CCYID { get; set; }
        [MaxLength(10)]
        public string P_CCY { get; set; }
        public string P_OrderRef { get; set; }
        public int P_OrderBrokerID { get; set; }
        public int P_OrderAccountID { get; set; }
        public string P_OrderAccount { get; set; }
        public string P_OrderName { get; set; }
        public string P_OrderAddress1 { get; set; }
        public string P_OrderAddress2 { get; set; }
        [MaxLength(20)]
        public string P_OrderZip { get; set; }
        public int P_OrderCountryID { get; set; }
        public string P_OrderSwIFt { get; set; }
        public string P_OrderIBAN { get; set; }
        public string P_OrderOther { get; set; }
        public int P_OrderVO_ID { get; set; }
        public string P_BenRef { get; set; }
        public int P_BenBrokerID { get; set; }
        public int P_BenAccountID { get; set; }
        public string P_BenAccount { get; set; }
        public int P_BenAddressID { get; set; }
        public int P_BenTypeID { get; set; }
        public string P_BenName { get; set; }
        public string P_BenFirstname { get; set; }
        public string P_BenMiddlename { get; set; }
        public string P_BenSurname { get; set; }
        public string P_BenAddress1 { get; set; }
        public string P_BenAddress2 { get; set; }
        public string P_BenCounty { get; set; }
        [MaxLength(20)]
        public string P_BenZip { get; set; }
        public int P_BenCountryID { get; set; }
        public string P_BenSwIFt { get; set; }
        public string P_BenIBAN { get; set; }
        public string P_BenOther { get; set; }
        public string P_BenBIK { get; set; }
        public string P_BenINN { get; set; }
        public int P_BenRelationshipID { get; set; }
        public int P_BenPortfolioID { get; set; }
        public string P_BenPortfolio { get; set; }
        public int P_BenCCYID { get; set; }
        [MaxLength(10)]
        public string P_BenCCY { get; set; }
        public double? P_ExchangeRate { get; set; }
        public string P_BenTemplateName { get; set; }
        public int P_BenSubBankAddressID { get; set; }
        public string P_BenSubBankName { get; set; }
        public string P_BenSubBankAddress1 { get; set; }
        public string P_BenSubBankAddress2 { get; set; }
        [MaxLength(20)]
        public string P_BenSubBankZip { get; set; }
        public int P_BenSubBankCountryID { get; set; }
        public string P_BenSubBankSwIFt { get; set; }
        public string P_BenSubBankIBAN { get; set; }
        public string P_BenSubBankOther { get; set; }
        public string P_BenSubBankBIK { get; set; }
        public string P_BenSubBankINN { get; set; }
        public int? P_BenBankAddressID { get; set; }
        public string P_BenBankName { get; set; }
        public string P_BenBankAddress1 { get; set; }
        public string P_BenBankAddress2 { get; set; }
        [MaxLength(20)]
        public string P_BenBankZip { get; set; }
        public int P_BenBankCountryID { get; set; }
        public string P_BenBankSwIFt { get; set; }
        public string P_BenBankIBAN { get; set; }
        public string P_BenBankOther { get; set; }
        public string P_BenBankBIK { get; set; }
        public string P_BenBankINN { get; set; }
        public string P_Comments { get; set; }
        public bool P_SendSwIFt { get; set; }
        public bool P_SendIMS { get; set; }
        public bool P_SendSwIFt2 { get; set; }
        public bool P_SendIMS2 { get; set; }
        public bool P_SwIFtUrgent { get; set; }
        public bool P_SwIFtSendRef { get; set; }
        public int? P_Other_ID { get; set; }
        public double? P_PortfolioFee { get; set; }
        [MaxLength(10)]
        public string P_PortfolioFeeCCY { get; set; }
        public double? P_PortfolioFeeINT { get; set; }
        [MaxLength(10)]
        public string P_PortfolioFeeCCYINT { get; set; }
        public int? P_PortfolioFeeAccountID { get; set; }
        public int? P_PortfolioFeeRMSAccountID { get; set; }
        public bool P_PortfolioIsFee { get; set; }
        public bool P_PortfolioIsCommission { get; set; }
        public int P_PortfolioFeeId { get; set; }
        public string? P_PaymentFilePath { get; set; }
        public string P_Username { get; set; }
        public string P_AuthorisedUsername { get; set; }
        public string P_SourceSystem { get; set; }
        public bool P_AboveThreshold { get; set; }
        [MaxLength(50)]
        public string P_IMSRef { get; set; }
        public int P_SentApproveEmailcompliance { get; set; }
        public bool PR_PaymentRequested { get; set; }
        public int PR_PaymentRequestedType { get; set; }
        public double? PR_PaymentRequestedRate { get; set; }
        public string? PR_PaymentRequestedFrom { get; set; }
        public string? PR_PaymentRequestedEmail { get; set; }
        public string PR_PaymentRequestedBeneficiaryName { get; set; }
        public bool PR_CheckedSantions { get; set; }
        public bool PR_CheckedPEP { get; set; }
        public bool PR_CheckedCDD { get; set; }
        public string? PR_IssuesDisclosed { get; set; }
        public string? PR_CheckedBankName { get; set; }
        public int? PR_CheckedBankCountryID { get; set; }
        public string PR_CheckedBankCountry { get; set; }
        public int? PR_MM_BS { get; set; }
        public int? PR_MM_InstrCode { get; set; }
        public int? PR_MM_DurationType { get; set; }
        public DateTime? PR_MM_StartDate { get; set; }
        public DateTime? PR_MM_EndDate { get; set; }
        public bool PR_MM_FirmMove { get; set; }
        public int P_PaymentTypeID { get; set; }
        public int P_BenTemplateSelected { get; set; }


    }
}