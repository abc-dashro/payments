﻿namespace Indo.Payments
{
    public class PaymentOthersDto
    {
        public int OTH_ID { get; set; }
        public int M_ID { get; set; }
        public string OtH_Type { get; set; }
        public string OtH_ShortCode { get; set; }
        public string OtH_IMSPayINCode { get; set; }
        public string OtH_IMSPayOUTCode { get; set; }
        public bool OtH_DefaultSendIMS { get; set; }
        public bool OtH_DefaultSendSwift { get; set; }
        public bool OtH_DefaultCheckSwift { get; set; }
        public int? S_ID1 { get; set; }
        public int? S_ID2 { get; set; }
        public int? S_ID3 { get; set; }
        public int? S_ID4 { get; set; }
        public int? S_ID5 { get; set; }
        public bool OtH_ShowOrder {get; set;}
        public bool OtH_ShowBeneficiary { get; set;}
        public bool OtH_ShowPortfolioAccount { get; set;}
        public bool OtH_ShowMessageDetails { get; set;}
        public string OtH_UserModifiedBy { get; set; }
        public string OtH_UserModifiedTime { get; set; }
        public string M_MessageType { get; set; }
        
    }
}
