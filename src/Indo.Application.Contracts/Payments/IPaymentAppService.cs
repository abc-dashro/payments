﻿using Indo.Benficiaries;
using Indo.Currencies;
using Indo.PaymentCountries;
using Indo.PaymentTypes;
using Indo.SourceClients;
using Indo.SourcePortfolios;
using Indo.SourcePortfolioTypes;
using Indo.SwiftCodes;
using Indo.Templates;
using Indo.TransferFees;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.Payments
{
    public interface IPaymentAppService : IApplicationService
    {        
        Task<string> InsertPayment(Payment payment);
        Task<List<SwiftCodeReadDto>> FetchSwiftCodes(string? scId, string? swiftCode);
        Task<List<PaymentCountriesReadDto>> FetchPaymentCountries(string? ctyId);
        Task<List<PaymentTypesReadDto>> FetchPaymentTypes(string? ctyId);
        Task<List<PortfoliosReadDto>> FetchSourcePortfolios(string? SpId);
        Task<List<FlowCurrency>> GetCurrencyListAsync();
        Task<List<TransferFeesReadDto>> FetchTransferFees(string? scId);
        Task<List<ClientRelationships>> FetchSourceClientRelationships();
        Task<List<BeneficiaryReadDto>> FetchBenficiaries();
        Task<List<TemplateReadDto>> FetchTemplates();
    }
}
