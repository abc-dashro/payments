﻿namespace Indo.Payments
{
    public class PaymentAddressReadDto
    {
        public int Pa_ID { get; set; }
        public int Pa_PortfolioID { get; set; }
        public string Portfolio { get; set; }
        public int Pa_CCYID { get; set; }
        public string? CR_Name1 { get; set; }
        public string? Pa_Name { get; set; }
        public string? Pa_Address1 { get; set; }
        public string? Pa_Address2 { get; set; }
        public string? Pa_Zip { get; set; }
        public string? Pa_Swift { get; set; }
        public string? Pa_IBAN { get; set; }
        public string? Pa_Other { get; set; }
        public string? Pa_BIK { get; set; }
        public string? Pa_INN { get; set; }
        public string? Pa_TemplateName { get; set; }
        public int Pa_TemplateSelected { get; set; }
        public string? Pa_LastModifiedTime { get; set; }
        public int Pa_BeneficiaryTypeID { get; set; }
        public string? Pa_Firstname { get; set; }
        public string? Pa_Middlename { get; set; }
        public string? Pa_Surname { get; set; }
        public string? Pa_County { get; set; }
        public int? Pa_RelationshipID { get; set; }
        public int? Pa_CountryID { get; set; }
        public string? Pa_Country { get; set; }
    }
}