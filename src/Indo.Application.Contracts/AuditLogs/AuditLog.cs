﻿namespace Indo.AuditLogs
{
    public class AuditLog
    {
        public int A_AuditID { get; set; }
        public string A_AuditStatus { get; set; }
        public string A_AuditGroupName { get; set; }
        public int A_AuditTransactionID { get; set; }
        public string A_AuditDescription { get; set; }
        public string A_AuditLoggedUser { get; set; }
        public string A_AuditLoggedUserMachine { get; set; }
        public string A_AuditDateTime { get; set; }
    }
}