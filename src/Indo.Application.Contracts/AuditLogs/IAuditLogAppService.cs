﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.AuditLogs
{
    public interface IAuditLogAppService : IApplicationService
    {
        Task<List<AuditLog>> GetListAsync();
    }
}