﻿namespace Indo.SourceAccounts
{
    public class AccountCreateDto
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public int SC_ID { get; set; }
        public string T_Code { get; set; }
        public string T_ShortCut1 { get; set; }
        public string T_Name1 { get; set; }
        public string T_Account { get; set; }
        public string T_IBAN { get; set; }
        public int SA_CurrencyID { get; set; }
    }
}