﻿using Indo.Currencies;
using System.Collections.Generic;

namespace Indo.SourceAccounts
{
    public class AccountsReadDto
    {
        public int SA_ID { get; set; }
        public int? SC_ID { get; set; }
        public string T_Code { get; set; }
        public string T_ShortCut1 { get; set; }
        public string T_Account { get; set; }
        public string T_Name1 { get; set; }
        public string T_IBAN { get; set; }
        public string CR_Name1 { get; set; }
        public int SA_CurrencyID { get; set; }
        public int T_type { get; set; }
        public List<CurrencyDetails> Currencies { get; set; }
    }
}