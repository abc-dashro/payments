﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.SourceAccounts
{
    public interface ISourceAccountAppService : IApplicationService
    {
        Task<List<AccountsReadDto>> GetListAsync();
        Task<object> CreateAsync(AccountCreateDto input);
        Task DeleteAsync(int id);
    }
}