﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.SourcePortfolioTypes
{
    public interface ISourcePortfolioTypeAppService : IApplicationService
    {
        Task<List<PortfolioTypesReadDto>> GetListAsync();
        Task<object> CreateAsync(PortfolioTypeCreateDto input);
        Task DeleteAsync(int id);
    }
}