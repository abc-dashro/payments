﻿namespace Indo.SourcePortfolioTypes
{
    public class PortfolioTypesReadDto
    {
        public int SPT_ID { get; set; }
        public string SPT_Type { get; set; }
    }
}