﻿namespace Indo.SourcePortfolioTypes
{
    public class PortfolioTypeCreateDto
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public int? SPT_ID { get; set; }
        public string SPT_Type { get; set; }
    }
}