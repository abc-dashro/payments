﻿using Indo.Currencies;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.TreasuryTools
{
    public interface ITreasuryToolAppService : IApplicationService
    {
        List<MovementReadDto> GetMovementsList();
        Task<List<FlowCurrency>> GetCurrencyListAsync();
    }
}
