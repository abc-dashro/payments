﻿using System;
using Volo.Abp.Application.Dtos;

namespace Indo.TreasuryTools
{
    public class MovementReadDto : AuditedEntityDto<Guid>
    {
        public string Portfolio { get; set; }
        public float CurrentBalanceA { get; set; }
        public float? MovementA { get; set; }
        public float NewBalanceA => CurrentBalanceA;
        public float CurrentBalanceB { get; set; }
        public float? MovementB { get; set; }
        public float NewBalanceB => CurrentBalanceB;
    }
}
