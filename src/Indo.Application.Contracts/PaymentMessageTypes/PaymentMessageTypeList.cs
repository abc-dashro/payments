﻿namespace Indo.PaymentMessageTypes
{
    public class PaymentMessageTypeList
    {
        public int M_ID { get; set; }
        public string M_MessageType { get; set; }
    }
}