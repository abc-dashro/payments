﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.PaymentMessageTypes
{
    public interface IPaymentMessageTypeAppService : IApplicationService
    {
        Task<List<PaymentMessageTypesReadDto>> GetListAsync();
        Task<object> CreateAsync(PaymentMessageTypeCreateDto input);
        Task DeleteAsync(int m_Id);
    }
}