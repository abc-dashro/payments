﻿namespace Indo.PaymentMessageTypes
{
    public class PaymentMessageTypeCreateDto
    {
        public string M_MessageType { get; set; }
        public string M_MessageTypeShortcut { get; set; }
        public bool M_Incoming { get; set; }
        public bool M_RD { get; set; }
        public bool M_Auto { get; set; }
        public string M_Description { get; set; }
        public string M_UserModifiedBy { get; set; }
    }
}