﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.ReadSwifts
{
    public interface IReadSwiftAppService
    {
        Task<List<SwiftReads>> GetListAsync();
    }
}