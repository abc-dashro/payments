﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.SourcePortfolios
{
    public interface ISourcePortfolioAppService : IApplicationService
    {
        Task<List<PortfoliosReadDto>> GetListAsync();
        Task<object> CreateAsync(PortfolioCreateDto input);
        Task DeleteAsync(int id);
    }
}