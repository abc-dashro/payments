﻿namespace Indo.SourcePortfolios
{
    public class PortfolioCreateDto
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public int SP_ID { get; set; }
        public int? SCL_ID { get; set; }
        public int? SP_Code { get; set; }
        public string SP_ShortCode { get; set; }
        public string SP_ShortName { get; set; }
        public string SP_Name { get; set; }
        public int SP_ManagementTypeID { get; set; }
        public int SP_CurrencyID { get; set; }
        public int SP_BranchID { get; set; }
        public string SP_UpdatedDate { get; set; }
    }
}