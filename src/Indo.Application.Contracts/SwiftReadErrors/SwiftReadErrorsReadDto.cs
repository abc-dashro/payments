﻿namespace Indo.SwiftReadErrors
{
    public class SwiftReadErrorsReadDto
    {
        public int SRE_ID { get; set; }
        public string SRE_RunDate { get; set; }
        public string SRE_FileName { get; set; }
        public string SRE_FileNameDate { get; set; }
        public string SRE_FileDateTime { get; set; }
        public string? SRE_ErrorSwiftType { get; set; }
        public string? SRE_ErrorReference { get; set; }
        public string SRE_ErrorMessage { get; set; }
        public string SRE_SwiftMessage { get; set; }
        public string SRE_EntryDate { get; set; }
    }
}