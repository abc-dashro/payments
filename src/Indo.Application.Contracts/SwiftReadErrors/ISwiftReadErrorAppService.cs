﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.SwiftReadErrors
{
    public interface ISwiftReadErrorAppService 
    {
        Task<List<SwiftReadErrorsReadDto>> GetListAsync();
    }
}