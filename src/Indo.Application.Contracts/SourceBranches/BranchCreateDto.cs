﻿namespace Indo.SourceBranches
{
    public class BranchCreateDto
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public int SBR_ID { get; set; }
        public string BR_Code { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string BranchLocation { get; set; }
    }
}