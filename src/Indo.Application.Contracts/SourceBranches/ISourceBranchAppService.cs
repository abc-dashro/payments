﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.SourceBranches
{
    public interface ISourceBranchAppService : IApplicationService
    {
        Task<List<BranchesReadDto>> GetListAsync();
        Task<object> CreateAsync(BranchCreateDto input);
        Task DeleteAsync(int id);
    }
}