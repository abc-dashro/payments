﻿namespace Indo.PaymentCountries
{
    public class PaymentCountriesReadDto
    {
        public int Ctry_ID { get; set; }
        public string Ctry_Name { get; set; }
        public string Ctry_Code { get; set; }
        public int Ctry_Score { get; set; }
    }
}