﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.PaymentCountries
{
    public interface IPaymentCountryAppService : IApplicationService
    {
        Task<List<PaymentCountriesReadDto>> GetListAsync();
        Task<object> CreateAsync(PaymentCountryCreateDto input);
        Task DeleteAsync(int ctryId);
    }
}