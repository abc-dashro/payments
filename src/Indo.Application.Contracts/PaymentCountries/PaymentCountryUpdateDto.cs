﻿namespace Indo.PaymentCountries
{
    public class PaymentCountryUpdateDto
    {
        public int Ctry_ID { get; set; }
        public string Ctry_Name { get; set; }
        public string Ctry_Code { get; set; }
        public int Ctry_Score { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
    }
}