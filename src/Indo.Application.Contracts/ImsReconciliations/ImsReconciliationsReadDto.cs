﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Indo.ImsReconciliations
{
    public class ImsReconciliationsReadDto
    {
        public int ID { get; set; }
        public string MatchStatusDesc { get; set; }
        public string BankCCY { get; set; }
        public string IMSCCY { get; set; }
        public decimal BankAmount { get; set; }
        public decimal IMSAmount { get; set; }
        public decimal AmountDiff { get; set; }
        public string BankRef1 { get; set; }
        public string IMSRef1 { get; set; }
        public string IMSRef2 { get; set; }
        public string BankType { get; set; }
        public string IMSType { get; set; }
        public string BankInfo2 { get; set; }
        public string BankTd { get; set; }
        public string IMSTd { get; set; }
        public string BankSd { get; set; }
        public string IMSSd { get; set; }
        public string BankISIN { get; set; }
        public string IMSISIN { get; set; }
        public string BankAccountNo { get; set; }
        public string IMSAccountNo { get; set; }
        public string BankAccountName { get; set; }
        public string IMSAccountName { get; set; }
        public decimal BankQuantity { get; set; }
        public decimal IMSQuantity { get; set; }
        public decimal QuantityDiff { get; set; }
        public string BankBroker { get; set; }
        public string IMSBroker { get; set; }
        public string Notes { get; set; }
        public string BankRef2 { get; set; }
        public string MatchStatus { get; set; }
        public bool Match_Reference { get; set; }
        public bool Match_TradeDate { get; set; }
        public bool Match_SettleDate { get; set; }
        public bool Match_Broker { get; set; }
        public bool AccountNo { get; set; }
        public bool Match_ISIN { get; set; }
        public bool MatchQuantity { get; set; }
        public bool Match_Amount { get; set; }
        public bool Match_CCY { get; set; }
        public int SwiftReadID { get; set; }
        public string BankInfo1 { get; set; }
        public string IMSPortfolio { get; set; }
        public string IMSPortfolioOther { get; set; }
        public int IMSIDCode { get; set; }
        public string BankLinkedIMSRef { get; set; }
        public string Rec_IMSLinkedIMSRef { get; set; }
        public string RunDateTime { get; set; }
        public string RunBy { get; set; }
        public string Rec_Rundate { get; set; }
        public int IDLink { get; set; }
        public int OrderColumn { get; set; }
    }
}
