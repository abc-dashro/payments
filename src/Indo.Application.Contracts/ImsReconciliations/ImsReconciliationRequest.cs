﻿namespace Indo.ImsReconciliations
{
    public class ImsReconciliationRequest
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public bool ReCalc { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
    }
}
