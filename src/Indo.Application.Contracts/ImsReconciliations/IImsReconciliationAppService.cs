﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.ImsReconciliations
{
    public interface IImsReconciliationAppService
    {
        Task<List<ImsReconciliationsReadDto>> GetListAsync();
    }
}