﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.Statuses
{
    public interface IStatusAppService : IApplicationService
    {
        Task<List<StatusReadDto>> GetListAsync();
        Task<object> CreateAsync(StatusCreateDto input);
        Task DeleteAsync(int id);
    }
}