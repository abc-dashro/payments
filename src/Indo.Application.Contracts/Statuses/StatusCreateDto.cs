﻿namespace Indo.Statuses
{
    public class StatusCreateDto
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public int S_ID { get; set; }
        public string S_Status { get; set; }
    }
}