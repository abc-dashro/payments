﻿namespace Indo.Statuses
{
    public class StatusReadDto
    {
        public int? S_ID { get; set; }
        public string S_Status { get; set; }
        public string S_UserModifiedBy { get; set; }
        public string S_UserModifiedTime { get; set; }
    }
}