﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.CorporateVoluntaryActions
{
    public interface ICorporateVoluntaryActionAppService : IApplicationService
    {
        Task<List<CorporateActionsVoluntaryReadDto>> GetListAsync();
    }
}