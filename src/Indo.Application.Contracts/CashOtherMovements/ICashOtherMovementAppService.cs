﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.CashOtherMovements
{
    public interface ICashOtherMovementAppService : IApplicationService
    {
        Task<List<CashOtherMovementsReadDto>> GetListAsync();
    }
}