﻿namespace Indo.SourceSettlementAccounts
{
    public class SettlementAccountsReadDto
    {
        public int SSA_ID { get; set; }
        public string Portfolio { get; set; }
        public int SP_ID { get; set; }
        public string T_Name1 { get; set; }
        public int SA_ID { get; set; }
        public double SSA_Balance1 { get; set; }
        public double SSA_Balance2 { get; set; }
        public double SSA_Balance3 { get; set; }
        public string SSA_Updated_DTTM { get; set; }
    }
}