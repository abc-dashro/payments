﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.SourceSettlementAccounts
{
    public interface ISourceSettlementAccountAppService : IApplicationService
    {
        Task<List<SettlementAccountsReadDto>> GetListAsync();
        Task<object> CreateAsync(SettlementAccountCreateDto input);
        Task DeleteAsync(int id);
    }
}