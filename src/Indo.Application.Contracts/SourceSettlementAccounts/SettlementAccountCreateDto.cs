﻿namespace Indo.SourceSettlementAccounts
{
    public class SettlementAccountCreateDto
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public int? SSA_ID { get; set; }
        public int SP_ID { get; set; }
        public int SA_ID { get; set; }
        public decimal? SSA_Balance1 { get; set; }
        public decimal? SSA_Balance2 { get; set; }
        public decimal? SSA_Balance3 { get; set; }
    }
}