﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.PaymentReceiveDeliveries
{
    public interface IPaymentReceiveDeliveryAppService : IApplicationService
    {
        Task<List<PaymentRecieveDeliveriesReadDto>> GetListAsync();
    }
}