﻿using System;

namespace Indo.PaymentReceiveDeliveries
{
    public class PaymentRecieveDeliveriesReadDto
    {
        public int PRD_ID { get; set; }
        public int PRD_StatusID { get; set; }
        public int PRD_MessageTypeID { get; set; }
        public int PRD_RecType { get; set; }
        public string PRD_OrderRef { get; set; }
        public int PRD_PortfolioCode { get; set; }
        public int PRD_CCYCode { get; set; }
        public int PRD_InstCode { get; set; }
        public string PRD_BrokerCode { get; set; }
        public int? PRD_CashAccountCode { get; set; }
        public string PRD_ClearerCode { get; set; }
        public DateTime? PRD_TransDate { get; set; }
        public DateTime? PRD_SettleDate { get; set; }
        public decimal PRD_Amount { get; set; }
        public string PRD_Agent { get; set; }
        public string PRD_AgentAccountNo { get; set; }
        public string? PRD_PlaceofSettlement { get; set; }
        public string? PRD_Instructions { get; set; }
        public bool PRD_SwiftReturned { get; set; }
        public bool PRD_SwiftAcknowledged { get; set; }
        public string? PRD_SwiftFile { get; set; }
        public string PRD_SwiftStatus { get; set; }
        public string PRD_Username { get; set; }
        public string? PRD_AuthorisedUsername { get; set; }
        public DateTime? PRD_AuthorisedTime { get; set; }
        public DateTime? PRD_CreatedTime { get; set; }
        public DateTime? PRD_ModifiedTime { get; set; }
        public DateTime? PRD_SwiftSentTime { get; set; }
        public DateTime? PRD_SwiftReceivedTime { get; set; }
        public bool PRD_ConfirmAcknowledged { get; set; }
        public DateTime? PRD_ConfirmReceivedTime { get; set; }
        public string? PRD_ConfirmSwiftFile { get; set; }
        public bool PRD_StatusAcknowledged { get; set; }
        public DateTime? PRD_StatusReceivedTime { get; set; }
        public string? PRD_StatusSwiftFile { get; set; }
        public DateTime? PRD_SwiftCancelSentTime { get; set; }
        public DateTime? PRD_SwiftCancelReceivedTime { get; set; }
        public bool PRD_SendIMS { get; set; }
        public bool PRD_IMSAcknowledged { get; set; }
        public DateTime? PRD_IMSSentTime { get; set; }
        public string? PRD_IMSFile { get; set; }
        public string PRD_IMSStatus { get; set; }
        public bool PRD_SendMiniOMS { get; set; }
        public DateTime? PRD_MiniOMSSentTime { get; set; }
        public string? PRD_Email { get; set; }
        public string? PRD_FilePath { get; set; }
        public int? PRD_LinkedID { get; set; }
    }
}