﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.SourceSettlementPlaces
{
    public interface ISourceSettlementPlaceAppService : IApplicationService
    {
        Task<List<SettlementPlacesReadDto>> GetListAsync();
        Task<object> CreateAsync(SettlementPlaceCreateDto input);
        Task DeleteAsync(int id);
    }
}