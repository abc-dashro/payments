﻿namespace Indo.SourceSettlementPlaces
{
    public class SettlementPlaceCreateDto
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public int SSP_ID { get; set; }
        public string SSP_Code { get; set; }
        public string SSP_ShortCode { get; set; }
        public string SSP_Name { get; set; }
        public string SSP_BIC { get; set; }
        public string? SSP_ExchangeCode { get; set; }
        public string SSP_Updated_DTTM { get; set; }
    }
}