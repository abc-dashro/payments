﻿namespace Indo.SwiftBackOfficePaymentMt210
{
    public class SwiftBackOfficePaymentMt210Dto
    {
        public string clientSwiftCode { get; set; }
        public string benSwiftCode { get; set; }
        public string orderRef { get; set; }
        public string brokerAccountNo { get; set; }
        public string relatedOrderRef { get; set; }            
        public string settleDate { get; set; }
        public double amount { get; set; }
        public string ccy { get; set; }
        public string orderSwiftCode { get; set; }
        public string benSubBankSwift { get; set; }    
        public bool swiftUrgent { get; set; }
        public string email { get; set; }
        public bool ftp { get; set; }
    }
}
