﻿namespace Indo.SwiftBackOfficePaymentmt200
{
    public class SwiftBackOfficePaymentmt200Dto
    {
        public string clientSwiftCode { get; set; }
        public string orderSwiftCode { get; set; }
        public string orderRef { get; set; }
        public string settleDate { get; set; }
        public double amount { get; set; }
        public string ccy { get; set; }
        public string benIBAN { get; set; }
        public string orderIBAN { get; set; }
        public string orderOther { get; set; }
        public bool swiftUrgent { get; set; }
        public string email { get; set; }
        public bool ftp { get; set; }       

    }
}
