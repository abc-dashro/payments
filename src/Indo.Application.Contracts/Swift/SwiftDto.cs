﻿namespace Indo.Swifts
{
    public class SwiftDto
    {
        public string clientSwiftCode { get; set; }
        public string brokerSwiftCode { get; set; }
        public string orderRef { get; set; }
        public string brokerAccountNumber { get; set; }
        public string tradeType { get; set; }
        public string cancelOrderRef { get; set; }
        public bool isEquity { get; set; }
        public string tradeDate { get; set; }
        public string settleDate { get; set; }
        public double amount { get; set; }
        public string instrumentISIN { get; set; }
        public string clearerSwiftCode { get; set; }
        public string deliveringAgentSwiftCode { get; set; }
        public string deliveringAgent { get; set; }
        public string settlementSwiftCode { get; set; }
        public string instructions { get; set; }
        public string email { get; set; }
        public bool ftp { get; set; }

    }
}
