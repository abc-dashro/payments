﻿namespace Indo.SwiftBackOfficePayment
{
    public class SwiftBackOfficePaymentDto
    {
        public string clientSwiftCode { get; set; }
        public string settleDate { get; set; }
        public double amount { get; set; }
        public string ccy { get; set; }
        public string orderRef { get; set; }
        public string orderName { get; set; }
        public string orderAddress { get; set; }
        public string orderCity { get; set; }
        public string orderPostCode { get; set; }
        public string orderSwift { get; set; }
        public string orderIbanAccountNumber { get; set; }
        public string orderOther { get; set; }
        public string orderVOCode { get; set; }
        public string beneficiaryName { get; set; }
        public string beneficiaryAddress1 { get; set; }
        public string beneficiaryAddress2 { get; set; }
        public string beneficiaryPostCode { get; set; }
        public string beneficiarySwift { get; set; }
        public string beneficiaryIBAN { get; set; }
        public string beneficiaryCcy { get; set; }
        public string beneficiarySubBankName { get; set; }
        public string beneficiarySubBankAddress1 { get; set; }
        public string beneficiarySubBankAddress2 { get; set; }
        public string beneficiarySubBankPostCode { get; set; }
        public string beneficiarySubBankSwift { get; set; }
        public string beneficiarySubBankIBAN { get; set; }
        public string beneficiarySubBankBik { get; set; }
        public string beneficiarySubBankINN { get; set; }
        public string beneficiaryBankName { get; set; }
        public string beneficiaryBankAddress1 { get; set; }
        public string beneficiaryBankAddress2 { get; set; }
        public string beneficiaryBankPostCode { get; set; }
        public string beneficiaryBankSwift { get; set; }
        public string beneficiaryBankIBAN { get; set; }
        public bool swiftUrgent { get; set; }
        public bool swiftSendRef { get; set; }
        public double exchangeRate { get; set; }
        public string email { get; set; }
        public bool ftp { get; set; }

    }
}
