﻿namespace Indo.SwiftsMT542
{
    public class SwiftMT542Dto
    {
        public string clientSwiftCode { get; set; }
        public string brokerSwiftCode { get; set; }
        public string orderRef { get; set; }
        public string brokerAccountNo { get; set; }
        public string tradeType { get; set; }
        public string cancelOrderRef { get; set; }
        public bool isEquity { get; set; }
        public string tradeDate { get; set; }
        public string settleDate { get; set; }
        public double amount { get; set; }
        public string instrumentISIN { get; set; }
        public string clearerSwiftCode { get; set; }
        public string agentAccountNo { get; set; }
        public string settlementSwiftCode { get; set; }
        public string instructions { get; set; }
        public string email { get; set; }
        public bool ftp { get; set; }

    }
}
