﻿namespace Indo.SwiftBackOfficePaymentMt299
{
    public class SwiftBackOfficePaymentMt299Dto
    {
        public string clientSwiftCode { get; set; }
        public string orderSwiftCode { get; set; }
        public string orderRef { get; set; }
        public string orderRelatedRef { get; set; }        
        public string message { get; set; }
        public bool swiftUrgent { get; set; }
        public string email { get; set; }
        public bool ftp { get; set; }
    }
}
