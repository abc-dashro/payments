﻿namespace Indo.CashMainMovements
{
    public class CashMainMovementsReadDto
    {
        public int P_PaymentTypeID { get; set; }
        public int P_ID { get; set; } //ID
        public string P_Status { get; set; } //Status
        public bool P_SentApproveEmail { get; set; } //EmailSent
        public bool P_SendSwift { get; set; } //SendSwift
        public bool P_SwiftAcknowledged { get; set; } //SwiftAck
        public bool P_SendIMS { get; set; } //SendIMS
        public bool P_IMSAcknowledged { get; set; } //IMSAck
        public string PaymentType { get; set; }
        public string BranchLocation { get; set; } //Branch
        public string P_Portfolio { get; set; } //Portfolio
        public decimal P_Amount { get; set; } //Amount
        public string P_CCY { get; set; } //P_CCY
        public string P_TransDate { get; set; } //TransDate
        public string P_SettleDate { get; set; } //SettleDate
        public string P_OrderRef { get; set; } //OrderRef
        public string P_OrderAccount { get; set; } //OrderAccount
        public decimal OrderBalanceUnblockedIncPending { get; set; } //OrderBalance
        public string P_BenPortfolio { get; set; } //BenPortfolio
        public string BenAccount { get; set; }
        public string P_BenRef { get; set; } //BenRef
        public string? P_SwiftFile { get; set; } //SwiftFile
        public bool P_SwiftReturned { get; set; } //SwiftReturned
        public string P_SwiftRef { get; set; } //SwiftSentRef
        public string P_IMSFile { get; set; } //IMSFile
        public bool P_IMSReturned { get; set; } //IMSReturned
        public string P_IMSDocNoIn { get; set; } //DocNoIn
        public string P_IMSDocNoOut { get; set; } //DocNoOut
        public bool P_PaidAcknowledged { get; set; } //PaidAck
        public string P_Comments { get; set; } //Comments
        public string P_OrderOther { get; set; } //SwiftRef
        public string P_CreatedTime { get; set; } //CreatedTime
        public string P_ModifiedTime { get; set; } //ModifiedTime
        public string? P_SwiftSentTime { get; set; } //SwiftSentTime
        public string? P_SwiftReceivedTime { get; set; }  //SwiftReceivedTime
        public string? P_IMSSentTime { get; set; }  //IMSSentTime
        public string? P_IMSReceivedTime { get; set; } //P_IMSReceivedTime
        public string? P_AuthorisedTime { get; set; } //AuthorisedTime
        public int orderval { get; set; }
        public string? P_PaidReceivedTime { get; set; } //PaidReceivedTime
        public string P_PaidReceivedInPayFile { get; set; } //PaidReceivedInPayFile
        public string P_PaidReceivedOutPayFile { get; set; } //PaidReceivedOutPayFile
        public string P_IMSRef { get; set; } //IMSRef
        public int? PaymentRequested { get; set; } //PayRequest
        public int? P_PaymentIDOther { get; set; } //PayIDOther
        public int BranchCode { get; set; }
        public int P_PortfolioCode { get; set; } //PortfolioCode
        public int P_CCYCode { get; set; } //CCYCode
        public string P_MessageType { get; set; } //MessageType
    }
}