﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.CashMainMovements
{
    public interface ICashMainMovementAppService : IApplicationService
    {
        Task<List<CashMainMovementsReadDto>> GetListAsync();
    }
}