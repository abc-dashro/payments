﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.SourceClients
{
    public interface ISourceClientAppService : IApplicationService
    {
        Task<List<ClientsReadDto>> GetListAsync();
        Task<object> CreateAsync(ClientCreateDto input);
        Task DeleteAsync(int id);
    }
}