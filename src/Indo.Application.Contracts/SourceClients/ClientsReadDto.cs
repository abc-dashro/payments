﻿using System.Collections.Generic;

namespace Indo.SourceClients
{
    public class ClientsReadDto
    {
        public int SCL_ID { get; set; }
        public int SCL_Code { get; set; }
        public string SCL_ShortCode { get; set; }
        public string SCL_Firstname { get; set; }
        public string SCL_Surname { get; set; }
        public int SCL_InvestmentTypeID { get; set; }
        public int SCL_RelationshipManagerID1 { get; set; }
        public string RelationshipManager1 { get; set; }
        public int SCL_RelationshipManagerID2 { get; set; }
        public string RelationshipManager2 { get; set; }
        public string SCL_UpdatedDate { get; set; }
        public List<ClientRelationships> ClientRelationships { get; set; }
    }
}