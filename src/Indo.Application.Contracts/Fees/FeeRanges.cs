﻿namespace Indo.Fees
{
    public class FeeRanges
    {
        public int Mgt_ID { get; set; }
        public double MGT_RangeFrom { get; set; } //From Amount
        public double MGT_RangeTo { get; set; } //To Amount
        public double MGT_Commission { get; set; } //Commission %
    }
}