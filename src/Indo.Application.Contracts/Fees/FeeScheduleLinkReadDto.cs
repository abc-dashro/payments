﻿using Indo.Assets;
using Indo.Templates;
using Indo.Transactions;
using System.Collections.Generic;

namespace Indo.Fees
{
    public class FeeScheduleLinkReadDto
    {
        public string Name { get; set; }
        public string Cust_Code { get; set; }
        public int PF_Code { get; set; }
        public string Portfolio { get; set; }
        public string? PF_ShortCut1 { get; set; }
        public string? RM1 { get; set; }
        public string? RM2 { get; set; }
        public string? PF_SName1 { get; set; } //PortfolioType
        public int FS_ID { get; set; }
        public int FS_StatusID { get; set; }
        public string S_Status { get; set; }
        public string? CR_Name1 { get; set; }
        public string FS_StartDate { get; set; }
        public string FS_EndDate { get; set; }
        public int FS_DestinationID { get; set; }
        public string FS_Destination { get; set; }
        public int FS_PayFreqID { get; set; }
        public string FS_PayFreq { get; set; }
        public int FS_MGTID { get; set; }
        public int FS_CFID { get; set; }
        public int FS_TFID { get; set; }
        public int TS_ID { get; set; }
        public string FS_FilePath { get; set; }
        public string FS_UserModifiedBy { get; set; }
        public string FS_UserModifiedTime { get; set; }
        public int? CR_Code { get; set; }
        public bool FS_Template { get; set; }
        public int Activity { get; set; }
        public int InvVisa { get; set; }
        public int? FS_Template_ID { get; set; } //Linked template id
        public string? TemplateLinkedName { get; set; }
        public string FS_TemplateName { get; set; }
        public int? BranchCode { get; set; }
        public string? BranchLocation { get; set; }
        public bool FS_IsEAM { get; set; }
        public int? PortfolioRedirectCode { get; set; }
        public string? PortfolioRedirect { get; set; }
        public List<FeeRanges> FeeRanges { get; set; }
        public List<InstrumentGroups> InstrumentGroups { get; set; }
        public List<Instruments> Instruments { get; set; }
        public List<CustodyFeeRanges> CustodyFeeRanges { get; set; }
        public List<AssetTypes> AssetTypes { get; set; }
        public List<CustodySafekeeping> CustodySafeKeeping { get; set; }
        public List<Safekeeping> Safekeeping { get; set; }
        public List<TransactionFeeRanges> TransactionFeeRanges { get; set; }
        public List<TransactionFeeTypes> TransactionFeeTypes { get; set; }
        public List<TransactionSafekeeping> TransactionSafekeeping { get; set; }
        public List<TransactionSafekeeping> TransactionSafekeepingFixed { get; set; }
        public List<TemplateTransferFee> TransferFees { get; set; }
        public List<ManagementTypes> ManagementTypes { get; set; }
        public List<EAMClients> EAMClients { get; set; }
    }
}
