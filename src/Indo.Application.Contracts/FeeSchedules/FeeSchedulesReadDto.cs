﻿namespace Indo.FeeSchedules
{
    public class FeeSchedulesReadDto
    {
        public int FS_ID { get; set; }
        public string S_Status { get; set; }
        public string Portfolio { get; set; }
        public string? CR_Name1 { get; set; }
        public string ClientName { get; set; }
        public string? PF_Shortcut1 { get; set; }
        public string? RM1 { get; set; }
        public string? RM2 { get; set; }
        public string? TemplateLinkedName { get; set; }
        public string FS_Destination { get; set; }
        public bool MGTID { get; set; }
        public bool CFID { get; set; }
        public bool TFID { get; set; }
        public bool TSID { get; set; }
        public bool FS_Template { get; set; }
        public bool Activity { get; set; }
        public bool InvVisa { get; set; }
        public bool FS_IsEAM { get; set; }
        public string FS_FilePath { get; set; }
        public int PF_Code { get; set; }
    }
}