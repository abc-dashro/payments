﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.FeeSchedules
{
    public interface IFeeScheduleAppService : IApplicationService
    {
        Task<List<FeeSchedulesReadDto>> GetListAsync();
    }
}