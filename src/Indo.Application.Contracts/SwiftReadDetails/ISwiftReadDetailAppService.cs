﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.SwiftReadDetails
{
    public interface ISwiftReadDetailAppService
    {
        Task<List<SwiftReadDetailsReadDto>> GetListAsync();
    }
}