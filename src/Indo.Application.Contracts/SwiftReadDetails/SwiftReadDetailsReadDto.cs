﻿namespace Indo.SwiftReadDetails
{
    public class SwiftReadDetailsReadDto
    {
        public int SRA_ID { get; set; }
        public string SRA_RunDate { get; set; }
        public string SRA_FileName { get; set; }
        public string SRA_FileNameDate { get; set; }
        public string SRA_SwiftType { get; set; }
        public string SRA_SwiftCode { get; set; }
        public string SRA_Reference { get; set; }
        public string? SRA_CustomerReference { get; set; }
        public string? SRA_BankReference { get; set; }
        public string SRA_AccountNo { get; set; }
        public string? SRA_TradeDate { get; set; }
        public string? SRA_SettleDate { get; set; }
        public double? SRA_Amount { get; set; }
        public string? SRA_CCY { get; set; }
        public string? SRA_Type { get; set; }
        public string? SRA_Type2 { get; set; }
        public string? SRA_AdditionalInfo1 { get; set; }
        public string? SRA_AdditionalInfo2 { get; set; }
        public string? SRA_OrderRef { get; set; }
        public int? M_ID { get; set; }
        public string? M_MessageType { get; set; }
        public string? M_MessageTypeShortcut { get; set; }
        public bool M_Incoming { get; set; }
        public bool M_RD { get; set; }
        public bool M_Auto { get; set; }
        public string? M_Description { get; set; }
        public string? M_UserModifiedBy { get; set; }
        public string? M_UserModifiedTime { get; set; }
    }
}