﻿namespace Indo.PortfolioHomeAddresses
{
    public class  PaymentHomeAddressReadDto
    {
        public string SwiftCode { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string CityName { get; set; }
        public string CountryCode { get; set; }
    }
}