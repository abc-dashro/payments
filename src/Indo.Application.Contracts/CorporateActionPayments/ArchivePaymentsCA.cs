﻿namespace Indo.CorporateActionPayments
{
    public class ArchivePaymentsCA
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; }
    }
}