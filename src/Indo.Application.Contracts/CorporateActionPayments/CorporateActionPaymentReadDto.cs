﻿namespace Indo.CorporateActionPayments
{
    public class CorporateActionPaymentReadDto
    {
        public string Status { get; set; }
        public int? CA_ID { get; set; }
        public int? PAYID { get; set; }
        public string Name { get; set; }
        public string ISIN { get; set; }
        public string CA_Event { get; set; }
        public string MV { get; set; }
        public string Custodian { get; set; }
        public string DeadlineDate { get; set; }
        public string DolfinDeadlineDate { get; set; }
        public string EarliestPayDate { get; set; }
        public string ExDate { get; set; }
        public string RecDate { get; set; }
        public string Paid_ISIN { get; set; }
        public string Option { get; set; }
        public string Cash_Stock { get; set; }
        public string Term_CCY { get; set; }
        public string Credit_Debit { get; set; }
        public string ValueDate { get; set; }
        public string PayDate { get; set; }
        public string PostingDate { get; set; }
        public string PostingDate1 { get; set; }
        public string CurrentDate { get; set; }
        public double? Gross_Amount { get; set; }
        public string Gross_CCY { get; set; }
        public double? Net_Amount { get; set; }
        public string Net_CCY { get; set; }
        public double? Posted_Amount { get; set; }
        public string Posted_Amount_CCY { get; set; }
        public double? Taxed_Amount { get; set; }
        public string Tax_CCY { get; set; }
        public double? Rate { get; set; }
        public string Rate_Type { get; set; }
        public string SRCA_FileDateTime { get; set; }
        public string SRCA_FileName { get; set; }
        public int? SRCA_ID { get; set; }
        public int? S_ID { get; set; }
        public int? CAC_ID { get; set; }
        public double? Settled_Position { get; set; }
    }
}