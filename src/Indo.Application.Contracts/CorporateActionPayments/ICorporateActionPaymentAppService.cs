﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.CorporateActionPayments
{
    public interface ICorporateActionPaymentAppService : IApplicationService
    {
        Task<List<CorporateActionPaymentReadDto>> GetListAsync();
    }
}