﻿using System;
using Volo.Abp.Application.Dtos;

namespace Indo.Employees
{
    public class EmployeeReadDto : AuditedEntityDto<Guid>
    {
        public string Name { get; set; }
        public string Position { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public Guid DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string EmployeeNumber { get; set; }
        public EmployeeGroup EmployeeGroup { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
