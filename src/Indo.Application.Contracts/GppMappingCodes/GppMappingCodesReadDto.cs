﻿using Indo.Brokers;
using Indo.Clearers;
using Indo.Currencies;
using System.Collections.Generic;

namespace Indo.GppMappingCodes
{
    public class GppMappingCodesReadDto
    {
        public int GPP_ID { get; set; }
        public string BrokerName { get; set; }
        public int GPP_BrCode { get; set; }
        public string ClearerName { get; set; }
        public int GPP_BrClrCode { get; set; }
        public string CCY { get; set; }
        public int GPP_CCYCode { get; set; }
        public string GPP_ClrCode { get; set; }
        public string GPP_ShortCode { get; set; }
        public string GPP_AccountNo { get; set; }
        public List<BrokerNames> Brokers { get; set; }
        public List<ClearerNames> Clearers { get; set; }
        public List<CurrencyDetails> Currencies { get; set; }
    }
}
