﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.GppMappingCodes
{
    public interface IGppMappingCodeAppService
    {
        Task<List<GppMappingCodesReadDto>> GetListAsync();
        Task<object> CreateAsync(GppMappingCodeCreateDto input);
        Task DeleteAsync(int id);
    }
}