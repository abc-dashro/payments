﻿namespace Indo.GppMappingCodes
{
    public class GppMappingCodeCreateDto
    {
        public int GPP_BrCode { get; set; }
        public int GPP_BrClrCode { get; set; }
        public int GPP_CCYCode { get; set; }
        public string GPP_ClrCode { get; set; }
        public string GPP_ShortCode { get; set; }
        public string GPP_AccountNo { get; set; }
    }
}