﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.PaymentTypes
{
    public interface IPaymentTypeAppService : IApplicationService
    {
        Task<List<PaymentTypesReadDto>> GetListAsync();
        Task<object> CreateAsync(PaymentTypeCreateDto input);
        Task DeleteAsync(int pt_Id);
    }
}