﻿namespace Indo.PaymentTypes
{
    public class PaymentTypeCreateDto
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public int PT_ID { get; set; }
        public string PT_PaymentType { get; set; }
        public string PT_ShortCode { get; set; }
        public bool PT_Movement { get; set; }
        public bool PT_DefaultSendIMS { get; set; }
        public bool PT_DefaultSendSwift { get; set; }
        public string? PT_IMSPayINCode { get; set; }
        public string? PT_IMSPayOUTCode { get; set; }
        public string? PT_AutoCommentHeader { get; set; }
        public string? PT_UserModifiedBy { get; set; }
        public string? PT_UserModifiedTime { get; set; }
        public int? M_ID { get; set; }
        public string? PT_ShortCodeCancel { get; set; }
        public int? S_ID1 { get; set; }
        public int? S_ID2 { get; set; }
        public int? S_ID3 { get; set; }
        public int? S_ID4 { get; set; }
        public int? S_ID5 { get; set; }
    }
}