﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.SwiftCodes
{
    public interface ISwiftCodeAppService : IApplicationService
    {
        Task<List<SwiftCodeReadDto>> GetListAsync();
        Task<object> CreateAsync(SwiftCodeCreateDto input);
        Task DeleteAsync(int id);
    }
}