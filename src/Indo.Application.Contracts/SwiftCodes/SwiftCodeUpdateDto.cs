﻿namespace Indo.SwiftCodes
{
    public class SwiftCodeUpdateDto
    {
        public int SC_ID { get; set; }
        public string SC_SwiftCode { set; get; }
        public string SC_BankName { get; set; }
        public string SC_Branch { get; set; }
        public string SC_Address1 { get; set; }
        public string SC_City { get; set; }
        public string SC_PostCode { get; set; }
        public string SC_BIK { get; set; }
        public string SC_INN { get; set; }
        public string SC_XMLSwift { get; set; }
        public string SC_FailMessage { get; set; }
        public string SC_LastModifiedDate { get; set; }
        public int? SC_SwiftTransferMessTypeID { get; set; }
        public string? SwiftTransferMessType { get; set; }
        public int? SC_Swift3rdPartyMessTypeID { get; set; }
        public string? Swift3rdPartyMessType { get; set; }
        public int? SC_SwiftOtherMessTypeID { get; set; }
        public string SwiftOtherMessType { get; set; }
        public bool? SC_SwiftPaidReturned { get; set; }
        public string SC_Source { get; set; }
        public int SC_CountryID { get; set; }
        public string SC_Country { get; set; }
        public string SC_CountryCode { get; set; }
    }
}