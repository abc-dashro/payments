﻿namespace Indo.SwiftCodes
{
    public class SwiftTransferMsgTypes
    {
        public int SC_SwiftTransferMessTypeID { get; set; }
        public string M_MessageType { get; set; }
    }
}