﻿namespace Indo.SwiftCodes
{
    public class SwiftOtherMsgTypes
    {
        public int SC_SwiftOtherMessTypeID { get; set; }
        public string M_MessageType { get; set; }
    }
}