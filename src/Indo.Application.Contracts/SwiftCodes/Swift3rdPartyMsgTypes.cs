﻿namespace Indo.SwiftCodes
{
    public class Swift3rdPartyMsgTypes
    {
        public int SC_Swift3rdPartyMessTypeID { get; set; }
        public string M_MessageType { get; set; }
    }
}