﻿namespace Indo.SwiftCodes
{
    public class SwiftCodeCreateDto
    {
        public string SC_SwiftCode { set; get; }
        public string SC_BankName { get; set; }
        public string SC_Branch { get; set; }
        public string SC_Address1 { get; set; }
        public string SC_City { get; set; }
        public string SC_PostCode { get; set; }
        public string SC_Country { get; set; }
        public int SC_RARangeCountryID { get; set; }
        public int SC_SwiftTransferMessTypeID { get; set; }
        public int SC_Swift3rdPartyMessTypeID { get; set; }
        public int SC_SwiftOtherMessTypeID { get; set; }
        public string SC_BIK { get; set; }
        public string SC_INN { get; set; }
        public string SC_LastModifiedDate { get; set; }
        public string SC_Source { get; set; }
    }
}