﻿using Indo.Benficiaries;
using Indo.Currencies;
using Indo.CutOffs;
using Indo.IntermediaryCodes;
using Indo.PaymentCountries;
using Indo.PaymentGridViews;
using Indo.PaymentRARangeType;
using Indo.Payments;
using Indo.PaymentSelectAccounts;
using Indo.PaymentTypes;
using Indo.PortfolioHomeAddresses;
using Indo.SourceClients;
using Indo.SourcePortfolios;
using Indo.SwiftBackOfficePayment;
using Indo.SwiftBackOfficePaymentmt200;
using Indo.SwiftBackOfficePaymentMt210;
using Indo.SwiftBackOfficePaymentMt299;
using Indo.SwiftCodes;
using Indo.Templates;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.BackOfficePayment
{
    public interface IBackOfficePaymentAppService: IApplicationService
    {
        Task<List<PaymentTypesReadDto>> FetchMoveTypes(string? ptId);

        Task<List<PortfoliosReadDto>> FetchPortfolio(string? SpId);

        Task<List<PaymentOthersDto>> FetchPaymentOthers(string? othId);
        Task<List<FlowCurrency>> FetchCurrencies(string currValue);
        Task<List<PaymentCountriesReadDto>> FetchPaymentCountries(string? ctyId);
        Task<List<TemplateReadDto>> FetchTemplates();
        Task<List<BeneficiaryReadDto>> FetchBenficiaries();
        Task<List<ClientRelationships>> FetchSourceClientRelationships();
        Task<List<IntermediaryCodesReadDto>> FetchIntermediaryCodes(string? ibank_Id);
        Task<List<PaymentSelectAccountReadDto>> FetchPaymentSelectAccount(string? spId);
        Task<string> InsertPayment(Payment payment);
        Task<string> UpdatePayment(Payment payment);
        Task<string> FetchSwiftBackOfficePayment(SwiftBackOfficePaymentDto? swiftBackOfficePaymentDto);
        Task<string> FetchSwiftBackOfficePaymentmt200(SwiftBackOfficePaymentmt200Dto? swiftBackOfficePaymentmt200Dto);
        Task<string> FetchSwiftBackOfficePaymentMt210(SwiftBackOfficePaymentMt210Dto? swiftBackOfficePaymentMt210Dto);
        Task<string> FetchSwiftBackOfficePaymentMt299(SwiftBackOfficePaymentMt299Dto? swiftBackOfficePaymentMt299Dto);
        Task<PaymentHomeAddressReadDto> FetchPaymentHomeAddress();
        Task<List<SwiftCodeReadDto>> FetchSwiftCodes(string? scId, string? swiftCode);
        Task<List<CutOffPayments>> FetchCutOffPayments(string? brokerCode, string? currCode);
        Task<List<PaymentAddressReadDto>> FetchPaymentAddress(string? paId, string? tempName);
        Task<List<PaymentGridViewReadDto>> FetchPaymentGridView(string? pId);
        Task<List<PaymentRARangeTypeReadDto>> FetchPaymentRARangeTypes(string? ptId);
    }
}
