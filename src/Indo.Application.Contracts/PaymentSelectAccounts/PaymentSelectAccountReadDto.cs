﻿namespace Indo.PaymentSelectAccounts
{
    public class PaymentSelectAccountReadDto
    {
        public int SP_ID { get; set; }
        public int SA_ID { get; set; }
        public int SCU_ID { get; set; }
        public int Cust_ID { get; set; }
        public string Cust_Code { get; set; }
        public string Name { get; set; }
        public int PF_Code { get; set; }
        public string Portfolio { get; set; }
        public string PF_ShortCut1 { get; set; }
        public string PortfolioType { get; set; }
        public int CR_Code { get; set; }
        public string CR_Name1 { get; set; }
        public int B_Code { get; set; }
        public string B_ShortCut1 { get; set; }
        public string B_Name1 { get; set; }
        public string B_Address { get; set; }
        public string B_Zip { get; set; }
        public string B_Area { get; set; }
        public int SC_CountryID { get; set; }
        public string SC_Country { get; set; }
        public string T_Account { get; set; }
        public string T_IBAN { get; set; }
        public string B_SwiftAddress { get; set; }
        public double Balance { get; set; }
        public double BalanceIncPending { get; set; }
        public double BalanceUnblocked { get; set; }
        public double BalanceUnblockedIncPending { get; set; }
        public decimal Pending { get; set; }
        public string T_Code { get; set; }
        public string T_ShortCut1 { get; set; }
        public string T_Name1 { get; set; }
        public string? BrokerClass { get; set; }
        public string? B_Class { get; set; }
        public string? BIK { get; set; }
        public string? INN { get; set; }
        public bool SC_SwiftPaidReturned { get; set; }
        public int SC_SwiftTransferMessTypeID { get; set; }
        public string? SwiftTransferMessType { get; set; }
        public int SC_Swift3rdPartyMessTypeID { get; set; }
        public string? Swift3rdPartyMessType { get; set; }
        public int SC_SwiftOtherMessTypeID { get; set; }
        public string? SwiftOtherMessType { get; set; }
        public string? IBankSwiftAddress { get; set; }
        public string? IBankName { get; set; }
        public string? IBankAddress { get; set; }
        public string? IBankCity { get; set; }
        public string? IBankPostCode { get; set; }
        public int? IBankCountryID { get; set; }
        public string? IBankCountry { get; set; }
        public string? IBankIBAN { get; set; }
        public string? IBankBIK { get; set; }
        public string? IBankINN { get; set; }
        public int B_BlockBroker { get; set; }
        public string BranchCode { get; set; }
        public string BranchLocation { get; set; }
        public bool PF_Active { get; set; }
        public bool SA_IsSecurityAccount { get; set; }        
    }
}