﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.SourceInstruments
{
    public interface ISourceInstrumentAppService : IApplicationService
    {
        Task<List<InstrumentsReadDto>> GetListAsync();
        Task<object> CreateAsync(InstrumentCreateDto input);
        Task DeleteAsync(int id);
    }
}