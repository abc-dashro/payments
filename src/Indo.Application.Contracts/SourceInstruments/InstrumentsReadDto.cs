﻿namespace Indo.SourceInstruments
{
    public class InstrumentsReadDto
    {
        public int SI_ID { get; set; }
        public string SI_Code { get; set; }
        public string SI_ShortCode { get; set; }
        public string SI_Name { get; set; }
        public string SI_Ticker { get; set; }
        public int SI_Type { get; set; }
        public string CR_Name1 { get; set; }
        public int SI_CurrencyID { get; set; }
        public string SI_ISIN { get; set; }
    }
}