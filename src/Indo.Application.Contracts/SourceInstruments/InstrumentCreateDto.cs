﻿namespace Indo.SourceInstruments
{
    public class InstrumentCreateDto
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public int SI_ID { get; set; }
        public string SI_Code { get; set; }
        public string SI_ShortCode { get; set; }
        public string SI_Name { get; set; }
        public string SI_Ticker { get; set; }
        public int? SI_Type { get; set; }
        public int SI_CurrencyID { get; set; }
        public string SI_ISIN { get; set; }
    }
}