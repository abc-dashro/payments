﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.CorporateMandatoryActions
{
    public interface ICorporateMandatoryActionAppService : IApplicationService
    {
        Task<List<CorporateActionsMandatoryReadDto>> GetListAsync();
    }
}