﻿namespace Indo.CorporateMandatoryActions
{
    public class CorporateActionsMandatoryReadDto
    {
        public string Status { get; set; }
        public int? CA_ID { get; set; }
        public string Name { get; set; }
        public string ISIN { get; set; }
        public string CA_Event { get; set; }
        public string MV { get; set; }
        public string Custodian { get; set; }
        public string DeadlineDate { get; set; }
        public string DolfinDeadlineDate { get; set; }
        public string EarliestPayDate { get; set; }
        public string ExDate { get; set; }
        public string RecDate { get; set; }
        public double? Settled_Position { get; set; }
        public string Comments1 { get; set; }
        public string Comments2 { get; set; }
        public string SR_FileName { get; set; }
        public string SR_FileDateTime { get; set; }
        public int? CAC_ID { get; set; }
        public int? S_ID { get; set; }
        public string SR_Reference { get; set; }
        public int? CheckDeadline { get; set; }
        public int? CheckExDate { get; set; }
        public int? CheckEvent { get; set; }
        public int? ChangeEvent { get; set; }
        public int? ChangeDeadlineDate { get; set; }
        public int? ChangeDolfinDeadlineDate { get; set; }
        public int? ChangeEarliestPayDate { get; set; }
        public int? ChangeExDate { get; set; }
        public int? ChangeRecDate { get; set; }
        public int? ChangeSettledPosition { get; set; }
        public int? PreviousSRID { get; set; }
        public int? T_Code { get; set; }
        public int? PAY_ID { get; set; }
    }
}
