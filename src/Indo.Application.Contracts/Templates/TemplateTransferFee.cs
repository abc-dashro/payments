﻿namespace Indo.Templates
{
    public class TemplateTransferFee
    {
        public int PF_Code { get; set; }
        public string Portfolio { get; set; }
        public int TF_Fee { get; set; }
        public int TF_CCYCode { get; set; }
        public int TF_Int_Fee { get; set; }
        public int TF_Int_CCYCode { get; set; }
    }
}