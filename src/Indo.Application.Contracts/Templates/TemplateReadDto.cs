﻿namespace Indo.Templates
{
    public class TemplateReadDto
    {
        public int pa_ID { get; set; }
        public string pa_TemplateName { get; set; }
    }
}