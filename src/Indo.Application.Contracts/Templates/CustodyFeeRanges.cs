﻿namespace Indo.Templates
{
    public class CustodyFeeRanges
    {
        public int CF_ID { get; set; }
        public int CF_TType { get; set; } //Asset Type
        public double CF_RangeFrom { get; set; } //From Amount
        public double CF_RangeTo { get; set; }  //To Amount
        public double CF_Commission { get; set; } //Commission %
    }
}