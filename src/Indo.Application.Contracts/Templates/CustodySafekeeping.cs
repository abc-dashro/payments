﻿namespace Indo.Templates
{
    public class CustodySafekeeping
    {
        public int CF_ID { get; set; }
        public int CF_ClearerCode { get; set; } //Safekeeping
        public double CF_Commission { get; set; } //Commission
        public double CF_MovementFee { get; set; } //CF_MovementFee
        public int CF_MovementFeeCCYCode { get; set; } //Movement CCY
    }
}