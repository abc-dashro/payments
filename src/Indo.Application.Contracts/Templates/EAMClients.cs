﻿namespace Indo.Templates
{
    public class EAMClients
    {
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public int ID { get; set; }
        public string ShortCode { get; set; }
        public string PortfolioCCY { get; set; }
        public string Destination { get; set; }
        public string PortfolioType { get; set; }
        public string RM1 { get; set; }
        public string RM2 { get; set; }
    }
}