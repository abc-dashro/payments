﻿namespace Indo.PaymentFirmPortfolios
{
    public class  PaymentFirmPortfolioReadDto
    {
        public int PF_Code { get; set; }
        public string Portfolio { get; set; }
        public string PF_ShortCut1 { get; set; }
        public int CR_Code { get; set; }
        public string CR_Name1 { get; set; }
        public string T_Code { get; set; }
        public string T_Name1 { get; set; }
        public int B_Code { get; set; }
        public string B_Name1 { get; set; }
        public string B_Class { get; set; }
    }
}