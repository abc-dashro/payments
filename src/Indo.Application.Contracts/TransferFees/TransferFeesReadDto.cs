﻿namespace Indo.TransferFees
{
    public class TransferFeesReadDto
    {
        public int PF_Code { get; set; }
        public string Portfolio { get; set; }
        public int? TF_Fee { get; set; }
        public string TF_CCY { get; set; }
        public int? TF_Int_Fee { get; set; }
        public string TF_Int_CCY { get; set; }
        public string TF_UserModifiedBy { get; set; }
        public string TF_UserModifiedTime { get; set; }
        public bool Exists { get; set; }
    }
}