﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.TransferFees
{
    public interface ITransferFeeAppService : IApplicationService
    {
        Task<List<TransferFeesReadDto>> GetListAsync();
        Task<object> CreateAsync(TransferFeeCreateDto input);
        Task DeleteAsync(int id);
    }
}