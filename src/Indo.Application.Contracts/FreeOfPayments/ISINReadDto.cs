﻿namespace Indo.FreeOfPayments
{
    public class ISINReadDto
    {
        public int ISINID { get; set; }
        public string ISINName { get; set; }
    }
}
