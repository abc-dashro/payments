﻿using Indo.PaymentTypes;
using Indo.SourcePortfolios;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.FreeOfPayments
{
    public interface IFreeOfPaymentAppService : IApplicationService
    {
        List<TypeReadDto> GetTypeList();
        List<ISINReadDto> GetISINList();
        List<CCYReadDto> GetCCYList();
        List<FOPRequestReadDto> GetFOPRequestList();
        Task<string> InsertFOPPayment(FOP fop);
        Task<List<PortfoliosReadDto>> FetchSourcePortfolios(string? SpId);
        Task<List<PaymentTypesReadDto>> FetchPaymentTypes(string? ctyId);
    }
}
