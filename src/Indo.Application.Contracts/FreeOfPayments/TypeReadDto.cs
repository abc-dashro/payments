﻿namespace Indo.FreeOfPayments
{
    public class TypeReadDto
    {
        public int TypeID { get; set; }
        public string TypeName { get; set; }
    }
}
