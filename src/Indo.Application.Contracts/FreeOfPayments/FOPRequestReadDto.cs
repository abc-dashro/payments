﻿using System;

namespace Indo.FreeOfPayments
{
    public class FOPRequestReadDto
    {
        public int Type { get; set; }
        public DateTime TransDate { get; set; }
        public DateTime SettleDate { get; set; }
        public int ISIN { get; set; }
        public float Amount { get; set; }
        public int CCY { get; set; }
    }
}
