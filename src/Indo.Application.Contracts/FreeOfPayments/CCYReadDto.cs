﻿namespace Indo.FreeOfPayments
{
    public class CCYReadDto
    {
        public int CCYID { get; set; }
        public string CCYName { get; set; }
    }
}
