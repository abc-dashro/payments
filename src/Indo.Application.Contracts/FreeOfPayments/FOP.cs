﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Indo.FreeOfPayments
{
    public class FOP
    {
        public int? PRD_ID { get; set; }
        public int PRD_RecType { get; set; }
        public int PRD_PortfolioCode { get; set; }
        public int PRD_CCYCode { get; set; }
        public int? PRD_InstCode { get; set; }
        public string PRD_BrokerCode { get; set; }
        public int? PRD_CashAccountCode { get; set; }
        public string PRD_ClearerCode { get; set; }
        public DateTime PRD_TransDate { get; set; }
        public DateTime PRD_SettleDate { get; set; }
        public double PRD_Amount { get; set; }
        [StringLength(12)]
        public string PRD_Agent { get; set; }
        [StringLength(30)]
        public string PRD_AgentAccountNo { get; set; }
        [StringLength(12)]
        public string PRD_PlaceofSettlement { get; set; }
        public string PRD_Instructions { get; set; }
        public bool PRD_SendIMS { get; set; }
        public bool PRD_SendMiniOMS { get; set; }
        public string? PRD_Email { get; set; }
        public string PRD_Username { get; set; }
        public string PRD_FilePath { get; set; }
    }
}