﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Indo.CutOffs
{
    public class CutOffPayment
    {
        [Required]
        public int CO_BrokerCode { get; set; }
        [Required]
        public int CO_CCYCode { get; set; }
        public string CO_CutOffTime { get; set; }
        public string CO_CutOffTimeZone { get; set; }
        public string CO_UserModifiedBy { get; set; }
        public string? CO_UserModifiedTime { get; set; }
    }
}
