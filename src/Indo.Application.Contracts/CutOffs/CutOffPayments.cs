﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Indo.CutOffs
{
    public class CutOffPayments
    {
        public int CO_BrokerCode { get; set; }
        public string B_Name1 { get; set; }
        public int CO_CCYCode { get; set; }
        [StringLength(3)]
        [Required]
        public string CR_Name1 { get; set; }
        public string CO_CutOffTime { get; set; }
        public string CO_CutOffTimeZone { get; set; }
        public string CO_UserModifiedBy { get; set; }
        public string? CO_UserModifiedTime { get; set; }
        public bool Exists { get; set; }
        public List<CutOffTime> CutOffTime { get; set; }
        public List<CutOffTimeZone> CutOffTimeZone { get; set; }
    }
}
