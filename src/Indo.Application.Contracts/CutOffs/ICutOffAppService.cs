﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Indo.CutOffs
{
    public interface ICutOffAppService : IApplicationService
    {
        Task<List<CutOffPayments>> GetListAsync();
        Task<object> CreateAsync(CutOffPayment input);
        Task DeleteAsync(int brokerCode, int currCode); 
    }
}