﻿namespace Indo.IntermediaryCodes
{
    public class IntermediarySwiftCodes
    {
        public int SC_ID { get; set; }
        public string SC_SwiftCode { get; set; }
    }
}