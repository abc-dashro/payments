﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.IntermediaryCodes
{
    public interface IIntermediaryCodeAppService
    {
        Task<List<IntermediaryCodesReadDto>> GetListAsync();
        Task<object> CreateAsync(IntermediaryCodeCreateDto input);
        Task DeleteAsync(int id);
    }
}