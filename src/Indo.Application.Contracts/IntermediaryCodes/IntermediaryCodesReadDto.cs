﻿using Indo.Currencies;
using Indo.SwiftCodes;
using System.Collections.Generic;

namespace Indo.IntermediaryCodes
{
    public class IntermediaryCodesReadDto
    {
        public int IBankID { get; set; }
        public string BankSwiftCode { get; set; }
        public int BankID { get; set; }
        public string BankCCY { get; set; }
        public int BankCCYCode { get; set; }
        public string IBankSwiftCode { get; set; }
        public int IB_SC_ID { get; set; }
        public string IBankCountry { get; set; }
        public int IBankCountryID { get; set; }
        public string IBankINN { get; set; }
        public string IBankIBAN { get; set; }
        public string IBankBIK { get; set; }
        public string BankFullName { get; set; } 
        public string IBankFullName { get; set; }
        public string IB_LastModifiedDate { get; set; }
        public List<IntermediarySwiftCodes> SwiftCodes { get; set; }
        //public List<CurrencyDetails> Currencies { get; set; }  Commented out by GB on 08/03/2022 - not used.
        //public List<SwiftRangeCountries> Countries { get; set; }  Commented out by GB on 08/03/2022 - not used.
    }
}