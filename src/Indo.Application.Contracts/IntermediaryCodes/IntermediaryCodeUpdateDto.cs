﻿namespace Indo.IntermediaryCodes
{
    public class IntermediaryCodeUpdateDto
    {
        public int IBankID { get; set; }
        public int BankID { get; set; }
        public int IB_SC_ID { get; set; }
        //public int IBankCountryID { get; set; }
        //public string IBankCountry { get; set; }
        public int BankCCYCode { get; set; }
        //public string IBankINN { get; set; }
        public string IBankIBAN { get; set; }
        public string IBankBIK { get; set; }
        //public string BankFullName { get; set; }
        //public string IBankFullName { get; set; }
    }
}