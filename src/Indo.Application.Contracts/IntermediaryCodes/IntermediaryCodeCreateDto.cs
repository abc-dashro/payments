﻿namespace Indo.IntermediaryCodes
{
    public class IntermediaryCodeCreateDto
    {
        public int BankID { get; set; }
        public int IBankCountryID { get; set; }
        public int IB_SC_ID { get; set; }
        public string IBankCountry { get; set; }
        public int BankCCYCode { get; set; }
        public string IBankINN { get; set; }
        public string IBankIBAN { get; set; }
        public string IBankBIK { get; set; }
        public string BankName { get; set; }
        public string BankCity { get; set; }
        public string IBankName { get; set; }
        public string IBankCity { get; set; }
    }
}