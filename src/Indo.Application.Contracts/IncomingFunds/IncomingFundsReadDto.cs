﻿namespace Indo.IncomingFunds
{
    public class IncomingFundsReadDto
    {
        public int ID { get; set; }
        public string Status { get; set; }
        public string IncomingFundsType { get; set; }
        public string? Portfolio { get; set; }
        public string Sender { get; set; }
        public double Amount { get; set; }
        public string AmountCCY { get; set; }
        public string TradeDate { get; set; }
        public string SettleDate { get; set; }
        public string? SwiftCode { get; set; }
        public string? SenderBankInstitution { get; set; }
        public string? AccountNo { get; set; }
        public string? ClientID { get; set; }
        public int? PF_Code { get; set; }
        public int SR_ID { get; set; }
        public int PortfolioID { get; set; }
        public string? Ref { get; set; }
        public string Filename { get; set; }
        public string? SR_IMSDocNoIn { get; set; }
        public string? IF_IMSDocNoOut { get; set; }
        public string? SR_Reference { get; set; }
        public int CCYCode { get; set; }
        public string Notes { get; set; }
        public string ReceivedDate { get; set; }
        public string ModifiedTime { get; set; }
        public string IF_AuthorisedDate { get; set; }
    }
}