﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indo.IncomingFunds
{
    public interface IIncomingFundAppService
    {
        Task<List<IncomingFundsReadDto>> GetListAsync();
    }
}